function maxCostRatioError = cr_err(threshNull, threshAlt, cr, models)
%UNTITLED6 Find squared error in theta coordinates for finding cost ratio
%corresponding to max threshNull and max threshAlt
%   Detailed explanation goes here
theta_ini = [1, 1];
A = []; b = []; Aeq = []; beq = []; lb = [0, 0]; ub = [];
[x, fval] = fmincon(@(theta) bayesrisk(theta, cr, models), theta_ini, A,b,Aeq,beq,lb,ub);

maxCostRatioError = (x(1) - max(threshNull))^2 + (x(2) - max(threshAlt))^2;
end

