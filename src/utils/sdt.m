function out = sdt(Hypotheses, nThreshold, nSamples, varargin)
%SDT Signal detection theory analysis of two distributions.
%   SDT(HYPOTHESES, NTHRESHOLD, NSAMP) returns a structure with values
%   necessary for plotting exact and MC ROC curves (NSAMP) at points
%   NTHRESHOLD. HYPOTHESES should be a structure.
%
%   SDT(HYPOTHESES, NTHRESHOLD, NSAMP, 'SamplePerThreshold', true) does the same
%   except MC sampling takes place at each threshold value in NTHRESHOLD.
%
%   SDT(HYPOTHESES, NTHRESHOLD, NSAMP, 'SequentialAnalysis', true,
%   'AvTimes', T) does the same except averaging over timesteps in array T.
%
%   thom.griffith@bristol.ac.uk, July 2018
%
%   Example
%       s = sdt(Hypotheses, nThreshold, nSamples, 'SamplePerThreshold',
%       true);
%       
%       sAv = sdt(Hypotheses, nThreshold, nSamples, 'SequentialAnalysis',
%       true, 'AvTimes', T);


%# define defaults at the beginning of the code so that you do not need to
%# scroll way down in case you want to change something or if the help is
%# incomplete. Use lower case for names.
options = struct('sampleperthreshold', 1, 'sequentialanalysis', 0, 'avtimes', [1 5 10 15 20]);

%# read the acceptable names
optionNames = fieldnames(options);

%# count arguments
nArgs = length(varargin);
if round(nArgs/2)~=nArgs/2
    error('SDT needs propertyName/propertyValue pairs')
end

for pair = reshape(varargin,2,[]) %# pair is {propName;propValue}
    inpName = lower(pair{1}); %# make case insensitive
    
    if any(strcmp(inpName,optionNames))
        %# overwrite options. If you want you can test for the right class here
        %# Also, if you find out that there is an option you keep getting wrong,
        %# you can use "if strcmp(inpName,'problemOption'),testMore,end"-statements
        options.(inpName) = pair{2};
    else
        error('%s is not a recognized parameter name',inpName)
    end
end

% Check order of distributions and assign noise and signal labels
tmpMu = zeros(2, 1);
for i = 1:2
    tmpMu(i) = Hypotheses(i).mu;
end
[~, idxMu] = max(tmpMu);
hSignal = idxMu;
if hSignal == 1
    hNoise = 2;
else
    hNoise = 1;
end

threshArray = linspace(Hypotheses(hNoise).mu - Hypotheses(hNoise).sd*3, Hypotheses(hSignal).mu + Hypotheses(hSignal).sd*3, nThreshold);

if ~(options.sequentialanalysis)
    % H_1
    tprArrayExact = 1 - cdf('Normal', threshArray, Hypotheses(hSignal).mu, Hypotheses(hSignal).sd);
    fnrArrayExact = 1 - tprArrayExact;
    tprVarArrayExact = (tprArrayExact .* fnrArrayExact);
    
    % H_0
    fprArrayExact = 1 - cdf('Normal', threshArray, Hypotheses(hNoise).mu, Hypotheses(hNoise).sd);
    tnrArrayExact = 1 - fprArrayExact;
    fprVarArrayExact = (fprArrayExact .* tnrArrayExact);
    
    % MC sampling
    sNullArray = zeros(nSamples, 1);
    sTrueArray = zeros(nSamples, 1);
    tprArray = zeros(length(threshArray), 1);
    fnrArray = zeros(length(threshArray), 1);
    fprArray = zeros(length(threshArray), 1);
    tnrArray = zeros(length(threshArray), 1);
    tprVarArray = zeros(length(threshArray), 1);
    fprVarArray = zeros(length(threshArray), 1);
    if options.sampleperthreshold
        sNullArrayPerThresholdCount = zeros(length(threshArray), nSamples);
        sTrueArrayPerThresholdCount = zeros(length(threshArray), nSamples);
    end
    
    if ~(options.sampleperthreshold)
        for k = 1:nSamples
            sNullArray(k) = icdf(Hypotheses(hNoise).name, rand(1), Hypotheses(hNoise).mu, Hypotheses(hNoise).sd);
            sTrueArray(k) = icdf(Hypotheses(hSignal).name, rand(1), Hypotheses(hSignal).mu, Hypotheses(hSignal).sd);
        end
    end
    
    for i = 1:length(threshArray)
        if options.sampleperthreshold
            for k = 1:nSamples
                sNullArray(k) = icdf(Hypotheses(hNoise).name, rand(1), Hypotheses(hNoise).mu, Hypotheses(hNoise).sd);
                sTrueArray(k) = icdf(Hypotheses(hSignal).name, rand(1), Hypotheses(hSignal).mu, Hypotheses(hSignal).sd);
                sNullArrayPerThresholdCount(i, k) = sNullArray(k);
                sTrueArrayPerThresholdCount(i, k) = sTrueArray(k);
            end
        end
        tprArray(i) = sum(sTrueArray >= threshArray(i)) / nSamples;
        fnrArray(i) = 1 - tprArray(i);
        tprVarArray(i) = tprArray(i) * fnrArray(i);
        fprArray(i) = sum(sNullArray >= threshArray(i)) / nSamples;
        tnrArray(i) = 1 - fprArray(i);
        fprVarArray(i) = fprArray(i) * tnrArray(i);
    end
    
    out.tprArray = tprArray;
    out.tprVarArray = tprVarArray;
    out.tprArrayExact = tprArrayExact;
    out.tprVarArrayExact = tprVarArrayExact;
    out.fprArray = fprArray;
    out.fprVarArray = fprVarArray;
    out.fprArrayExact = fprArrayExact;
    out.fprVarArrayExact = fprVarArrayExact;
    out.NoiseArrayPerThresholdCount = sNullArrayPerThresholdCount;
    out.SignalArrayPerThresholdCount = sTrueArrayPerThresholdCount;
    out.NoiseArray = sNullArray;
    out.SignalArray = sTrueArray;
    
    out.threshArray = threshArray;
    
    out.hNoise = hNoise;
    out.hSignal = hSignal;
    
else
    fprPlotArray = zeros(length(threshArray), length(options.avtimes));
    tprPlotArray = zeros(length(threshArray), length(options.avtimes));
    
    % Exact ROC curves
    nXvals = 500;
    xValArray = linspace(Hypotheses(hNoise).mu - Hypotheses(hNoise).sd*3, Hypotheses(hSignal).mu + Hypotheses(hSignal).sd*3, nXvals);
    fprArrayExact = zeros(length(threshArray), length(options.avtimes));
    tprArrayExact = zeros(length(threshArray), length(options.avtimes));
    fprVarArrayExact = zeros(length(threshArray), length(options.avtimes));
    tprVarArrayExact = zeros(length(threshArray), length(options.avtimes));
    
    iPlt = 1;
    for t = options.avtimes
        
        % MC sampling
        tprArray = zeros(length(threshArray), 1);
        fnrArray = zeros(length(threshArray), 1);
        fprArray = zeros(length(threshArray), 1);
        tnrArray = zeros(length(threshArray), 1);
        tprVarArray = zeros(length(threshArray), 1);
        fprVarArray = zeros(length(threshArray), 1);
        
        
        
        % H_1 - distribution of the sample mean - variance goes as 1/t
        tprArrayExact(:,iPlt) = 1 - cdf('Normal', threshArray, Hypotheses(hSignal).mu, Hypotheses(hSignal).sd/sqrt(t));
        fnrArrayExact = 1 - tprArrayExact(:,iPlt);
        tprVarArrayExact(:,iPlt) = (tprArrayExact(:,iPlt) .* fnrArrayExact);
        
        % H_0 - distribution of the sample mean - variance goes as 1/t
        fprArrayExact(:,iPlt) = 1 - cdf('Normal', threshArray, Hypotheses(hNoise).mu, Hypotheses(hNoise).sd/sqrt(t));
        tnrArrayExact = 1 - fprArrayExact(:,iPlt);
        fprVarArrayExact(:,iPlt) = (fprArrayExact(:,iPlt) .* tnrArrayExact);
        
        sNullArray = zeros(nSamples, t);
        sTrueArray = zeros(nSamples, t);
        if ~options.sampleperthreshold
            for k = 1:nSamples
                sNullArray(k, :) = icdf(Hypotheses(hNoise).name, rand(1, t), Hypotheses(hNoise).mu, Hypotheses(hNoise).sd);
                sTrueArray(k, :) = icdf(Hypotheses(hSignal).name, rand(1, t), Hypotheses(hSignal).mu, Hypotheses(hSignal).sd);
            end
        end
        
        for i = 1:length(threshArray)
            if options.sampleperthreshold
                for k = 1:nSamples
                    sNullArray(k, :) = icdf(Hypotheses(hNoise).name, rand(1, t), Hypotheses(hNoise).mu, Hypotheses(hNoise).sd);
                    sTrueArray(k, :) = icdf(Hypotheses(hSignal).name, rand(1, t), Hypotheses(hSignal).mu, Hypotheses(hSignal).sd);
                end
            end
            tprArray(i) = sum(mean(sTrueArray, 2) >= threshArray(i)) / nSamples;
            fnrArray(i) = 1 - tprArray(i);
            tprVarArray(i) = tprArray(i) * fnrArray(i);
            fprArray(i) = sum(mean(sNullArray, 2) >= threshArray(i)) / nSamples;
            tnrArray(i) = 1 - fprArray(i);
            fprVarArray(i) = fprArray(i) * tnrArray(i);
        end
        
        fprPlotArray(:,iPlt) = fprArray;
        tprPlotArray(:,iPlt) = tprArray;
        iPlt = iPlt + 1;
    end
    
    out.tprArray = tprArray;
    out.tprVarArray = tprVarArray;
    out.tprPlotArray = tprPlotArray;
    out.tprArrayExact = tprArrayExact;
    out.tprVarArrayExact = tprVarArrayExact;
    out.fprArray = fprArray;
    out.fprVarArray = fprVarArray;
    out.fprPlotArray = fprPlotArray;
    out.fprArrayExact = fprArrayExact;
    out.fprVarArrayExact = fprVarArrayExact;
    out.NoiseArray = sNullArray;
    out.SignalArray = sTrueArray;
    out.threshArray = threshArray;
    out.hNoise = hNoise;
    out.hSignal = hSignal;
    out.avTimes = options.avtimes;
end


end

