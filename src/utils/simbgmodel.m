function [ HTrue, H, delta, decTime, sArray, OArray ] = simbgmodel( numHyp, numChannel, probThresh, simTime )
%BGMODEL Summary of this function goes here
%   Detailed explanation goes here

priorArray = (1/numHyp) * ones(numHyp, 1);
probThreshArray = probThresh * ones(numHyp, 1);
thetaArr = -log(probThreshArray);

[H, HTrue] = make_hyp(numHyp, numChannel);

% Preallocate storage arrays
sArray = zeros(numChannel, simTime);
OArray = zeros(numHyp, simTime);
YArray = zeros(numHyp, simTime);

xT = zeros(numHyp, numChannel);

% Initialise array values at time t = 0
YArray(:,1) = log(priorArray);
OArray(:,1) = -YArray(:,1);

% Step through time array
for t = 2:simTime
    
    % Sample from true hypothesis channel distributions
    for k = 1:numChannel
        sArray(k,t) = icdf(H(HTrue).name, rand(1), H(HTrue).mu(k), H(HTrue).sd(k));
    end
    
    % Calculate likelihoods over hypotheses
    for m = 1:numHyp
        for k = 1:numChannel
            xT(m,k) = pdf(H(m).name, sArray(k,t), H(m).mu(k), H(m).sd(k)); % likelihood of individual observations
        end
    end
    X_t = log(prod(xT,2)); % this is the log combined likelihood for observations (s_1,...,s_K) given H_m
    
    % Iterate evidence, Y_arr(t)
    YArray(:,t) = YArray(:,t-1) + X_t;
    
    % Calculate the negative log posterior, O_vec
    OArray(:,t) = -YArray(:,t) + logsumexp(YArray(:,t));
    
    % Test if threshold is met
    if any(OArray(:,t) < thetaArr)
        break
    end
    
end

% Set decision time and delta
decTime = t-1;
[M, delta] = min(OArray(:,t));

end

