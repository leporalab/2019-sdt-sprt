function [out, model4Grid] = sprt4roc_thresh_isocurves(Hypotheses, threshNullArray, threshAltArray, tArr, varargin)
%SPRT4ROC Returns structure with arrays TPR and FPR for equivalent SPRT ROC curves
%   Detailed explanation goes here

%# define defaults at the beginning of the code so that you do not need to
%# scroll way down in case you want to change something or if the help is
%# incomplete. Use lower case for names.
options = struct('nepisodes', 500, 'fittoav', 0);

%# read the acceptable names
optionNames = fieldnames(options);

%# count arguments
nArgs = length(varargin);
if round(nArgs/2)~=nArgs/2
    error('SPRT4ROC needs propertyName/propertyValue pairs')
end

for pair = reshape(varargin,2,[]) %# pair is {propName;propValue}
    inpName = lower(pair{1}); %# make case insensitive
    
    if any(strcmp(inpName,optionNames))
        %# overwrite options. If you want you can test for the right class here
        %# Also, if you find out that there is an option you keep getting wrong,
        %# you can use "if strcmp(inpName,'problemOption'),testMore,end"-statements
        options.(inpName) = pair{2};
    else
        error('%s is not a recognized parameter name',inpName)
    end
end

%% INITIALISE

ContourLevels = tArr;
maxT = 10000;
nRows = options.nepisodes * length(threshAltArray) * length(threshNullArray);

hSelectedWhenAltTrue = zeros(options.nepisodes, 1);
hSelectedWhenNullTrue = zeros(options.nepisodes, 1);
tprArray = zeros(length(threshNullArray), length(threshAltArray));
fprArray = zeros(length(threshNullArray), length(threshAltArray));
avDecTimeArray = zeros(length(threshNullArray), length(threshAltArray));
threshRatioArray = zeros(length(threshNullArray), length(threshAltArray));
decTimeArray = zeros(length(options.nepisodes), 2);

tabNullThresh = zeros(nRows, 1);
tabAltThresh = zeros(nRows, 1);
tabDecTime = zeros(nRows, 2);
tabError = zeros(nRows, 2);

iRow = 1;

for m = 1:length(threshNullArray)
    for j = 1:length(threshAltArray)
        fprintf('Performing SPRT over %d episodes for thresholds [%.2f %.2f] ...\n', options.nepisodes, threshNullArray(m), threshAltArray(j));
        for k = 1:options.nepisodes
            [hSelectedWhenAltTrue(k), ~, decTimeArray(k, 1)] = sprt(Hypotheses, 2, maxT, threshNullArray(m), threshAltArray(j));
            [hSelectedWhenNullTrue(k), ~, decTimeArray(k, 2)] = sprt(Hypotheses, 1, maxT, threshNullArray(m), threshAltArray(j));
            
            %%%%%%%%%%%%%%%%%%%%
            % Capture column vectors for long table of all data points
            %tabTrueHypothesis(2*k:2*k+1) = [2 1]';
            tabNullThresh(iRow) = threshNullArray(m);
            tabAltThresh(iRow) = threshAltArray(j);
            tabDecTime(iRow, :) = [decTimeArray(k, 1), decTimeArray(k, 2)];
            tabError(iRow, :) = [~(hSelectedWhenAltTrue(k) == 2), ~(hSelectedWhenNullTrue(k) == 1)];
            iRow = iRow + 1;
            %%%%%%%%%%%%%%%%%%
        end
        avDecTimeArray(m, j) = mean(decTimeArray(:));
        threshRatioArray(m, j) = threshAltArray(j) / threshNullArray(m);
        nHitsAlt = sum(hSelectedWhenAltTrue == 2);
        tprArray(m, j) = nHitsAlt / options.nepisodes;
        nHitsNull = sum(hSelectedWhenNullTrue == 2);
        fprArray(m, j) = nHitsNull / options.nepisodes;
        fprintf('TPR = %.2f, FPR = %.2f \n', tprArray(m,j), fprArray(m,j))
    end
end

%% PLOT
% Flatten arrays for plotting
avDecTimeArray = avDecTimeArray(:);
tprArray = tprArray(:);
fprArray = fprArray(:);

if options.fittoav
    % Prepare table for regression analysis
    newThreshNullArray = repmat(threshNullArray, 1, length(threshNullArray));
    newThreshAltArray = repelem(threshAltArray, length(threshAltArray));
    t = table(newThreshNullArray', newThreshAltArray', avDecTimeArray, tprArray, fprArray);
    t.Properties.VariableNames = {'threshNull', 'threshAlt', 'avT', 'tpr', 'fpr'};
    %save('gp_data2','t')
    
    f1 = figure;
    set(f1, 'color','white', 'position', [100 100 1400 350])
    
    %% Fit GPs
    % Plot AvT
    subplot(1,4,1)
    [trainedModelAvT, ~] = trainRegressionModelAvT(t);
    [zfit, zsd, ~] = trainedModelAvT.predictFcn(t);
    scatter3(t.threshNull, t.threshAlt, t.avT, 20, 'MarkerEdgeColor','k',...
        'MarkerFaceColor','r')
    hold on
    plot3d_errorbars(t.threshNull, t.threshAlt, zfit,...
        zeros(length(t.threshNull)), zeros(length(t.threshAlt)), 2.*zsd, '.r')
    [Y,X] = meshgrid(threshNullArray, threshAltArray);
    Z = reshape(zfit, length(threshNullArray), length(threshAltArray));
    surf(X,Y,Z)
    [CavT, c1] = contour3(X,Y,Z,ContourLevels,'ShowText','off');
    c1.Visible = 'off';
    [xAvT, yAvT, zAvT] = C2xyz(CavT);
    xlabel('\theta_0'); ylabel('\theta_1'); zlabel('E(T)');
    title('E(T)')
    
    % Plot TPR
    subplot(1,4,2)
    [trainedModelTpr, ~] = trainRegressionModelTpr(t);
    [zfit, zsd, ~] = trainedModelTpr.predictFcn(t);
    scatter3(t.threshNull, t.threshAlt, t.tpr, 20, 'MarkerEdgeColor','k',...
        'MarkerFaceColor','r')
    hold on
    plot3d_errorbars(t.threshNull, t.threshAlt, zfit,...
        zeros(length(t.threshNull)), zeros(length(t.threshAlt)), 2.*zsd, '.r')
    
    [Y,X] = meshgrid(threshNullArray, threshAltArray);
    Z = reshape(zfit, length(threshNullArray), length(threshAltArray));
    surf(X,Y,Z)
    %     [Ctpr, c2] = contour3(X,Y,Z,'ShowText','off');
    %     c2.Visible = 'off';
    %     CoordTpr = C2xyz(Ctpr);
    xlabel('\theta_0'); ylabel('\theta_1'); zlabel('TPR');
    title('TPR')
    
    % Plot FPR
    subplot(1,4,3)
    [trainedModelFpr, validationRMSE] = trainRegressionModelFpr(t);
    [zfit, zsd, zint] = trainedModelFpr.predictFcn(t);
    scatter3(t.threshNull, t.threshAlt, t.fpr, 20, 'MarkerEdgeColor','k',...
        'MarkerFaceColor','r')
    hold on
    plot3d_errorbars(t.threshNull, t.threshAlt, zfit,...
        zeros(length(t.threshNull)), zeros(length(t.threshAlt)), 2.*zsd, '.r')
    [Y,X] = meshgrid(threshNullArray, threshAltArray);
    Z = reshape(zfit, length(threshNullArray), length(threshAltArray));
    surf(X,Y,Z)
    %     [Cfpr, c3] = contour3(X,Y,Z,'ShowText','off');
    %     c3.Visible = 'off';
    %     CoordFpr = C2xyz(Cfpr);
    xlabel('\theta_0'); ylabel('\theta_1'); zlabel('FPR');
    title('FPR')
    
    % Plot ROC curves
    %subplot(1,4,4)
    f2 = figure;
    set(f2, 'color','white', 'position', [150 150 600 500])
    axis([0, 1, 0, 1])
    hold on
    for i = 1:length(xAvT)
        x = xAvT{i}; y = yAvT{i};
        fitTab = table(x', y');
        fitTab.Properties.VariableNames = {'threshNull', 'threshAlt'};
        tprFit{i} = trainedModelTpr.predictFcn(fitTab);
        fprFit{i} = trainedModelFpr.predictFcn(fitTab);
        plot(fprFit{i}, tprFit{i}, 'linewidth', 1.5)
    end
    titleStr = "SPRT: Iso-nsteps in ROC space";
    title(titleStr)
    
    ylabel('True positive rate'); xlabel('False positive rate')
    for k = 1:length(xAvT)
        legStr{k} = sprintf('T = %d', zAvT(k));
    end
    
out.tpr = tprFit;
out.fpr = fprFit;

%model4Grid = trainedModel4Grid;

end

