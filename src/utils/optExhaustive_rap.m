function Out = optExhaustive_rap(costRatios, threshRange, Hyp, precision)
%EXHAUSTIVE_OPT Finds optimal threshold for sprt model
%using exhaustive search of threshold values and gp regression
%   If costRatios are equal then reduce problem to one dimension. Force
%   symmetric thresholds.

% Uniformly sample threshold from range.
r1 = threshRange(1); r2 = threshRange(2);
if abs(costRatios(1) - (costRatios(2))) < 0.0001
    threshold = r1 + rand(Hyp.nTrials, 1)*(r2 - r1);
    sprtBounds = [-threshold, threshold];
else
    threshold = r1 + rand(2*Hyp.nTrials, 1)*(r2 - r1);
    sprtBounds = reshape(threshold, length(threshold)/2,2);
    sprtBounds = [-1.*sprtBounds(:,1), sprtBounds(:,2)];
    lowerThresh = sprtBounds(:,1);
    upperThresh = sprtBounds(:,2);
end

% Apply SPRT decision rule to evidence.
[~, decTime, errorType] = makeSprtDecision(Hyp, sprtBounds,...
    'decisions', 'all');

% Calculate reward (inner product of errorType and error costs plus
% total sampling cost)
reward = - errorType*costRatios' - decTime;

% Fit Gaussian process model to data
if abs(costRatios(1) - (costRatios(2))) < 0.0001
    dataTable = table(threshold, reward);
else
    dataTable = table(lowerThresh, upperThresh, reward);
end

% Find optimal threshold using GP model
disp('Fitting Gaussian Process regression model...')
Out.gprMdl = fitrgp(dataTable, 'reward');
tmp = threshRange(1):precision:threshRange(2);
if abs(costRatios(1) - (costRatios(2))) < 0.0001
    newThreshPredictors = table(tmp', 'VariableNames', {'threshold'});
    Out.threshold = threshold;
else
    tmptab = combvec(-tmp, tmp)';
    newThreshPredictors = table(tmptab(:,1), tmptab(:,2), 'VariableNames', {'lowerThresh', 'upperThresh'});
    Out.threshold = sprtBounds;
end
disp('Making reward predictions...')
Out.rewardPred = predict(Out.gprMdl, newThreshPredictors);

[Out.optReward, optThreshIdx] = max(Out.rewardPred);
Out.optThresh = newThreshPredictors(optThreshIdx,:);

Out.reward = reward;
if abs(costRatios(1) - (costRatios(2))) < 0.0001
    Out.thresholdPred = newThreshPredictors.threshold;
else
    
    Out.thresholdPred(:,1) = newThreshPredictors.lowerThresh;
    Out.thresholdPred(:,2) = newThreshPredictors.upperThresh;
end
Out.decTime = decTime;

isError = ~isempty(find(errorType, 1));
Out.error = isError;

% Matrix outputs for surf plots
if ~(abs(costRatios(1) - (costRatios(2))) < 0.0001)
    [Out.threshPredMatUpper, Out.threshPredMatLower] = meshgrid(tmp, -tmp);
    Out.rewardPredMat = reshape(Out.rewardPred, size(Out.threshPredMatUpper));
end

end

