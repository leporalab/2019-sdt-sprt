function [ H, HTrue ] = make_hyp( numHyp, numChannel)
%MAKE_HYP Make structure array of numHyp hypotheses with numChan pdf 
% parameters and randomly select true hypothesis, HTrue.
%   Detailed explanation goes here

hypMeanArray = cell(1, numHyp);
hypSdArray = cell(1, numHyp);

for m = 1:numHyp
    hypMeanArray{m} = 10*rand(1, numChannel);
    hypSdArray{m} = 1*ones(1, numChannel);
end

% Fill hypotheses structure with distribution parameters
H = struct('name', 'Normal', 'mu', hypMeanArray, 'sd', hypSdArray);

% Randomly select true hypothesis
HTrue = randi(numHyp);

end

