function y = rocfunc(beta, alpha, C, Hypotheses, option)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
mu0 = Hypotheses(1).mu;
mu1 = Hypotheses(2).mu;
sd = Hypotheses(1).sd;

switch option
    case 1
        y = (2*expEvidenceNorm(mu0, mu1, sd, mu0))^-1 * ((1-alpha)*log(beta/(1-alpha)) + alpha*log((1-beta)/alpha)) + ...
            (2*expEvidenceNorm(mu0, mu1, sd, mu1))^-1 * (beta*log(beta/(1-alpha)) + (1-beta)*log((1-beta)/alpha)) - C;
    case 0
        y = (expEvidenceNorm(mu0, mu1, sd, mu0))^-1 * ((1-alpha)*log(beta/(1-alpha)) + alpha*log((1-beta)/alpha)) - C;
end
end

