function out = logsumexp(x)
%LOGSUMEXP Use log sum exp trick on input to avoid underflow 
a = max(x);
out = a + log(sum(exp(x-a)));
