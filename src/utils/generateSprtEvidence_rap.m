function [evidence, target] = generateSprtEvidence_rap(Hyp)
%GenerateSprtEvidence Generate SPRT evidence trajectories and targets.
%   [evidence, target] = generateSprtEvidence(Dist) returns evidence
%   trajectories (logLR(signal/noise)) for true hypothesis as a matrix and target indicates the
%   true hypothesis (1=noise, 2=signal). Dist is a distribution structure.

disp('generating SPRT evidence trajectories...')

% Apply Distribution defaults if not user-defined
if ~isfield(Hyp,'mean'); Hyp.mean = 1; end
if ~isfield(Hyp,'sd'); Hyp.sd = 1; end
if ~isfield(Hyp,'nHyp'); Hyp.nHyp = length(Hyp.mean); end
if ~isfield(Hyp,'prior'); Hyp.prior = ones( 1, Hyp.nHyp ) / Hyp.nHyp; end
if ~isfield(Hyp,'nTrials'); Hyp.nTrials = 1e5; end
if ~isfield(Hyp,'maxT'); Hyp.maxt = 100; end

% Make symmetrical Distributions for noise and signal
if length(Hyp.mean)==1 
    switch Hyp.nHyp       
        case 2; Hyp.mean = Hyp.mean * [-1, 1];
        otherwise; disp('numHyp not in range'); return
    end
end

% Randomly select targets (true hypotheses)
[~, nTargets] = size(Hyp.mean);
target = randi(nTargets, [Hyp.nTrials, 1]);

% Initialise evidence storage array
evidence = nan(Hyp.maxT, Hyp.nTrials);

% Generate evidence trajectories for true hypothesis for iTrial
for iTrial = 1:Hyp.nTrials
    x = normrnd(Hyp.mean(target(iTrial)), Hyp.sd, [Hyp.maxT, 1]);    
    xLikelihood = normpdf(x, Hyp.mean, Hyp.sd);
    likelihoodRatio = xLikelihood(:,2)./xLikelihood(:,1);
    logLR = log(likelihoodRatio);
    cumLogLR = cumsum(logLR);
    evidence(:, iTrial) = cumLogLR;
end
