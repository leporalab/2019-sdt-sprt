function [costRatioPair] = threshPair2CostPair(threshPair, trainedModelThresh2NullCost, trainedModelThresh2AltCost)
%UNTITLED11 Vector-valued function mapping threshold pair to cost-ratio
%pair in sprt
%   Detailed explanation goes here
t = table(threshPair(1), threshPair(2));
t.Properties.VariableNames = {'threshNull', 'threshAlt'};

costNull = trainedModelThresh2NullCost.predictFcn(t);
costAlt = trainedModelThresh2AltCost.predictFcn(t);

costRatioPair = [costNull, costAlt];
end

