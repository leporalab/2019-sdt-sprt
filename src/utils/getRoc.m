function [alpha, beta] = getRoc(Hypotheses, tArr)
%GETROC Description
%   Detailed explanation goes here

alpha = 0.3:0.1:0.8;
beta = zeros(length(tArr), length(alpha));

i = 1;
for t = tArr
     for j = 1:length(alpha)
        fun = @(x) rocfunc(x, alpha(j), t, Hypotheses, 1);
        x = fzero(fun, 0.5); %beta only allowed to take vales in [0 1]
        beta(i,j) = x;
     end
     i = i + 1;
 end

end

