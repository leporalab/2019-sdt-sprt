function [out, model4Grid] = sprt4roc_thresh_isocurves(Hypotheses, threshNullArray, threshAltArray, tArr, varargin)
%SPRT4ROC Returns structure with arrays TPR and FPR for equivalent SPRT ROC curves
%   Detailed explanation goes here

%# define defaults at the beginning of the code so that you do not need to
%# scroll way down in case you want to change something or if the help is
%# incomplete. Use lower case for names.
options = struct('nepisodes', 500, 'fittoav', 0);

%# read the acceptable names
optionNames = fieldnames(options);

%# count arguments
nArgs = length(varargin);
if round(nArgs/2)~=nArgs/2
    error('SPRT4ROC needs propertyName/propertyValue pairs')
end

for pair = reshape(varargin,2,[]) %# pair is {propName;propValue}
    inpName = lower(pair{1}); %# make case insensitive
    
    if any(strcmp(inpName,optionNames))
        %# overwrite options. If you want you can test for the right class here
        %# Also, if you find out that there is an option you keep getting wrong,
        %# you can use "if strcmp(inpName,'problemOption'),testMore,end"-statements
        options.(inpName) = pair{2};
    else
        error('%s is not a recognized parameter name',inpName)
    end
end

%% INITIALISE

ContourLevels = tArr;
maxT = 10000;
nRows = options.nepisodes * length(threshAltArray) * length(threshNullArray);

hSelectedWhenAltTrue = zeros(options.nepisodes, 1);
hSelectedWhenNullTrue = zeros(options.nepisodes, 1);
tprArray = zeros(length(threshNullArray), length(threshAltArray));
fprArray = zeros(length(threshNullArray), length(threshAltArray));
avDecTimeArray = zeros(length(threshNullArray), length(threshAltArray));
threshRatioArray = zeros(length(threshNullArray), length(threshAltArray));
decTimeArray = zeros(length(options.nepisodes), 2);

tabNullThresh = zeros(nRows, 1);
tabAltThresh = zeros(nRows, 1);
tabDecTime = zeros(nRows, 2);
tabError = zeros(nRows, 2);

iRow = 1;

for m = 1:length(threshNullArray)
    for j = 1:length(threshAltArray)
        fprintf('Performing SPRT over %d episodes for thresholds [%.2f %.2f] ...\n', options.nepisodes, threshNullArray(m), threshAltArray(j));
        for k = 1:options.nepisodes
            [hSelectedWhenAltTrue(k), ~, decTimeArray(k, 1)] = sprt(Hypotheses, 2, maxT, threshNullArray(m), threshAltArray(j));
            [hSelectedWhenNullTrue(k), ~, decTimeArray(k, 2)] = sprt(Hypotheses, 1, maxT, threshNullArray(m), threshAltArray(j));
            
            %%%%%%%%%%%%%%%%%%%%
            % Capture column vectors for long table of all data points
            %tabTrueHypothesis(2*k:2*k+1) = [2 1]';
            tabNullThresh(iRow) = threshNullArray(m);
            tabAltThresh(iRow) = threshAltArray(j);
            tabDecTime(iRow, :) = [decTimeArray(k, 1), decTimeArray(k, 2)];
            tabError(iRow, :) = [~(hSelectedWhenAltTrue(k) == 2), ~(hSelectedWhenNullTrue(k) == 1)];
            iRow = iRow + 1;
            %%%%%%%%%%%%%%%%%%
        end
        avDecTimeArray(m, j) = mean(decTimeArray(:));
        threshRatioArray(m, j) = threshAltArray(j) / threshNullArray(m);
        nHitsAlt = sum(hSelectedWhenAltTrue == 2);
        tprArray(m, j) = nHitsAlt / options.nepisodes;
        nHitsNull = sum(hSelectedWhenNullTrue == 2);
        fprArray(m, j) = nHitsNull / options.nepisodes;
        fprintf('TPR = %.2f, FPR = %.2f \n', tprArray(m,j), fprArray(m,j))
    end
end

%% PLOT
% Flatten arrays for plotting
avDecTimeArray = avDecTimeArray(:);
tprArray = tprArray(:);
fprArray = fprArray(:);


% Prepare table for regression analysis
newThreshNullArray = repmat(threshNullArray, 1, length(threshNullArray));
newThreshAltArray = repelem(threshAltArray, length(threshAltArray));
t = table(newThreshNullArray', newThreshAltArray', avDecTimeArray, tprArray, fprArray);
t.Properties.VariableNames = {'threshNull', 'threshAlt', 'avT', 'tpr', 'fpr'};
%save('gp_data2','t')

f1 = figure;
set(f1, 'color','white', 'position', [100 100 1400 350])



%% Fit GPs
% Plot AvT
subplot(1,4,1)
[trainedModelAvT, ~] = trainRegressionModelAvT(t);
[zfit, zsd, ~] = trainedModelAvT.predictFcn(t);
scatter3(t.threshNull, t.threshAlt, t.avT, 20, 'MarkerEdgeColor','k',...
    'MarkerFaceColor','r')
hold on
plot3d_errorbars(t.threshNull, t.threshAlt, zfit,...
    zeros(length(t.threshNull)), zeros(length(t.threshAlt)), 2.*zsd, '.r')
[Y,X] = meshgrid(threshNullArray, threshAltArray);
Z = reshape(zfit, length(threshNullArray), length(threshAltArray));
surf(X,Y,Z)
[CavT, c1] = contour3(X,Y,Z,ContourLevels,'ShowText','off');
c1.Visible = 'off';
[xAvT, yAvT, zAvT] = C2xyz(CavT);
xlabel('\theta_0'); ylabel('\theta_1'); zlabel('E(T)');
title('E(T)')

% Plot TPR
subplot(1,4,2)
[trainedModelTpr, ~] = trainRegressionModelTpr(t);
[zfit, zsd, ~] = trainedModelTpr.predictFcn(t);
scatter3(t.threshNull, t.threshAlt, t.tpr, 20, 'MarkerEdgeColor','k',...
    'MarkerFaceColor','r')
hold on
plot3d_errorbars(t.threshNull, t.threshAlt, zfit,...
    zeros(length(t.threshNull)), zeros(length(t.threshAlt)), 2.*zsd, '.r')

[Y,X] = meshgrid(threshNullArray, threshAltArray);
Z = reshape(zfit, length(threshNullArray), length(threshAltArray));
surf(X,Y,Z)
%     [Ctpr, c2] = contour3(X,Y,Z,'ShowText','off');
%     c2.Visible = 'off';
%     CoordTpr = C2xyz(Ctpr);
xlabel('\theta_0'); ylabel('\theta_1'); zlabel('TPR');
title('TPR')

% Plot FPR
subplot(1,4,3)
[trainedModelFpr, validationRMSE] = trainRegressionModelFpr(t);
[zfit, zsd, zint] = trainedModelFpr.predictFcn(t);
scatter3(t.threshNull, t.threshAlt, t.fpr, 20, 'MarkerEdgeColor','k',...
    'MarkerFaceColor','r')
hold on
plot3d_errorbars(t.threshNull, t.threshAlt, zfit,...
    zeros(length(t.threshNull)), zeros(length(t.threshAlt)), 2.*zsd, '.r')
[Y,X] = meshgrid(threshNullArray, threshAltArray);
Z = reshape(zfit, length(threshNullArray), length(threshAltArray));
surf(X,Y,Z)
%     [Cfpr, c3] = contour3(X,Y,Z,'ShowText','off');
%     c3.Visible = 'off';
%     CoordFpr = C2xyz(Cfpr);
xlabel('\theta_0'); ylabel('\theta_1'); zlabel('FPR');
title('FPR')

% Plot ROC curves
%subplot(1,4,4)
f2 = figure;
set(f2, 'color','white', 'position', [150 150 600 500])
C = linspecer(10);
axis([0, 1, 0, 1])
hold on

titleStr = "SPRT: ROC curves";
title(titleStr)

ylabel('True positive rate'); xlabel('False positive rate')
for k = 1:length(xAvT)
    legStr{k} = sprintf('T = %d', zAvT(k));
end

%% Build table for [fpr tpr] -> E(n) GP fit
%     X = []; Y = []; Z = [];
%     for i = 1:length(xAvT)
%        xtmp = fprFit{i}; ytmp = tprFit{i};
%        X = [X; xtmp]; Y = [Y; ytmp]; Z = [Z; zAvT(i).*ones(length(xtmp), 1)];
%     end
%     tErr2En = table(X, Y, Z);
%     tErr2En.Properties.VariableNames = {'fpr', 'tpr', 'AvT'};
tErr2En = t(:,{'tpr', 'fpr', 'avT'});
[trainedModel4Grid, validationRMSE4Grid] = trainRegressionModel4Grid(tErr2En);
%[zfit, zsd, zint] = trainedModel4Grid.predictFcn(t);

%% Get error rates for threshold iso-curves
thrPerFix = 10;
isoNull = linspace(min(threshNullArray), max(threshNullArray), thrPerFix);
isoAlt = linspace(min(threshAltArray), max(threshAltArray), thrPerFix);
results0cell = cell(1, length(isoNull));
results1cell = cell(1, length(isoAlt));

for i = 1:length(isoNull)
    tabNull = isoNull(i).*ones(1,thrPerFix)';
    tabAlt = linspace(min(isoAlt), max(isoAlt), thrPerFix)';
    t0 = table(tabNull, tabAlt);
    t0.Properties.VariableNames = {'threshNull', 'threshAlt'};
    [tpr0isoFit, tpr0Sd, ~] = trainedModelTpr.predictFcn(t0);
    [fpr0isoFit, fpr0Sd, ~] = trainedModelFpr.predictFcn(t0);
    t0.tpr = tpr0isoFit;
    t0.fpr = fpr0isoFit;
    results0cell{i} = t0;
    %waitbar(i/(2*length(isoNull)), f);
end

for i = 1:length(isoAlt)
    tabNull = linspace(min(isoNull), max(isoNull), thrPerFix)';
    tabAlt = isoAlt(i).*ones(1,thrPerFix)';
    t1 = table(tabNull, tabAlt);
    t1.Properties.VariableNames = {'threshNull', 'threshAlt'};
    [tpr1isoFit, tpr1Sd, ~] = trainedModelTpr.predictFcn(t1);
    [fpr1isoFit, fpr1Sd, ~] = trainedModelFpr.predictFcn(t1);
    t1.tpr = tpr1isoFit;
    t1.fpr = fpr1isoFit;
    results1cell{i} = t1;
    %waitbar((length(isoNull)+ i) / (2*length(isoNull)), f);
end

%% Plot threshold isocurves
%     f2 = figure;
%     set(f2, 'color','white', 'position', [150 150 600 500])
%     %SDT
%     axis([0, 1, 0, 1])
h8 = plot([0 1], [0 1], 'k:');
set(get(get(h8, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle', 'off');
%     hold on
C2 = linspecer(5);
for i = 1:length(results0cell)
    plt_tab = results0cell{i};
    h1(i) = plot(plt_tab.fpr, plt_tab.tpr);
    set(h1(i), 'linestyle', ':', 'color', C2(1,:),...
        'linewidth', 0.5);
end
for i = 1:length(results1cell)
    plt_tab = results1cell{i};
    h2(i) = plot(plt_tab.fpr, plt_tab.tpr);
    set(h2(i), 'linestyle', ':', 'color', C2(2,:),...
        'linewidth', 0.5);
end
%legend(legStr)


set(gca, 'ColorOrder', C)
colormap(gca, C)
c1 = colorbar;
labels = arrayfun(@(x) num2str(x), tArr, 'UniformOutput', false);
labels_n = cellfun(@(c)['n = ' c],labels,'uni',false);
c1.TickLabels = labels_n;
tmp = c1.Ticks;
jmp = diff(tmp);
jump = jmp(1)/2;
new_ticks = tmp(1:end-1) + jump;
c1.Ticks = new_ticks;
c1.TickLength = 0;

for i = 1:length(xAvT)
    x = xAvT{i}; y = yAvT{i};
    fitTab = table(x', y');
    fitTab.Properties.VariableNames = {'threshNull', 'threshAlt'};
    tprFit{i} = trainedModelTpr.predictFcn(fitTab);
    fprFit{i} = trainedModelFpr.predictFcn(fitTab);
    if i == 1
        tmp = length(tprFit{i});
        if tmp > 1
            tprFit{i} = mean(tprFit{i});
            fprFit{i} = mean(fprFit{i});
        end
    end
    h9 = plot(fprFit{i}, tprFit{i});
    if i == 1
        set(h9, 'linestyle','none', 'Marker', '+', 'color', C(i,:), 'linewidth', 1);
    end
    set(h9, 'linestyle','-', 'color', C(i,:), 'linewidth', 1);
end

t1 = annotation('textbox');
t1.String = '\theta_0';
t1.FontSize = 7;
t2 = annotation('textbox');
t2.String = '\theta_1';
t1.EdgeColor = 'none'; t1.Color = C2(1,:); t1.Position = [0.4800    0.7120    0.0583    0.0660];
t2.EdgeColor = 'none'; t2.Color = C2(2,:); t2.Position = [0.2433    0.4240    0.0583    0.0660];
t2.FontSize = 7;
t3 = annotation('textarrow');
t3.String = '(\theta_0, \theta_1) = (0,0)';
t3.Position = [ 0.5550    0.5060   -0.1517    0.1060];
t3.FontSize = 7;

t4 = annotation('textarrow');
t4.String = '(\theta_0, \theta_1) = (4,0)';
t4.Position = [0.6483    0.8480   -0.0717    0.0540];
t4.FontSize = 7;

t5 = annotation('textarrow');
t5.String = '(\theta_0, \theta_1) = (0,4)';
t5.Position = [ 0.2933    0.2300   -0.1517    0.1060];
t5.FontSize = 7;

out.tpr = tprFit;
out.fpr = fprFit;

model4Grid = trainedModel4Grid;

%%
f2.Units               = 'centimeters';
%f1.Position(3)         = 18.5; % two columns
f2.Position(3)         = 9; % one column
f2.Position(4)         = 6.5;
% set text properties
set(f2.Children, ...
    'FontName',     'Arial', ...
    'FontSize',     8);

set(gcf, 'Renderer', 'painters')

end

