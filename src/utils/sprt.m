function [sel, Y, decTime] = sprt(Hypotheses, hTrue, maxT, threshNull, threshAlt)
%SPRT Run SPRT method on hypotheses
%   Detailed explanation goes here

Y = zeros(maxT, 1);
sTrueArray = zeros(maxT, 1);
likelihoodAltArray = zeros(maxT, 1);
likelihoodNullArray = zeros(maxT, 1);

Y(1) = 0;
for t = 2:maxT
    sTrueArray(t) = icdf(Hypotheses(hTrue).name, rand(1), Hypotheses(hTrue).mu, Hypotheses(hTrue).sd);
    likelihoodAltArray(t) = pdf(Hypotheses(2).name, sTrueArray(t), Hypotheses(2).mu, Hypotheses(2).sd);
    likelihoodNullArray(t) = pdf(Hypotheses(1).name, sTrueArray(t), Hypotheses(1).mu, Hypotheses(1).sd);
    Y(t) = Y(t-1) + log(likelihoodAltArray(t) / likelihoodNullArray(t));
    if Y(t) > threshAlt
        hSelected = 2;
        break
    elseif Y(t) < -threshNull
        hSelected = 1;
        break
    end
end

if t == maxT
    error('Decision threshold not reached in allotted time');
end

sel = hSelected;
decTime = t - 1;

end

