function maxCostRatio = findmaxcost(threshNullArray, threshAltArray, models)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

% Minimize ROC datapoint error
cr_ini = [100, 100];
A = []; b = []; Aeq = []; beq = []; lb = [10, 10]; ub = [];
[x, ~] = fmincon(@(cr) cr_err(threshNullArray, threshAltArray, cr, models), cr_ini, A,b,Aeq,beq,lb,ub);

maxCostRatio = x;
end

