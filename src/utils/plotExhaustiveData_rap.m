function [] = plotExhaustiveData_rap(SamplingOut)
%plotExhaustiveData Summary of this function goes here
%   Detailed explanation goes here

if width(SamplingOut.optThresh) < 1.1
    % Plot sampling optimiser
    f3 = figure;
    set(f3, 'color','white')
    plot(SamplingOut.threshold, SamplingOut.reward, 'k.')
    xlabel('threshold'); ylabel('reward')
    hold on
    [sortedThreshold, indices] = sort(SamplingOut.thresholdPred);
    sortedRewardPred = SamplingOut.rewardPred(indices);
    plot(sortedThreshold, sortedRewardPred, 'r', 'LineWidth', 1.5);
    v1 = vline(table2array(SamplingOut.optThresh),'r:');
    v1.LineWidth = 1.5;
    
else
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Plot sampling optimiser
    f3 = figure;
    set(f3, 'color','white')
    s3 = scatter3(SamplingOut.threshold(:,1), SamplingOut.threshold(:,2), SamplingOut.reward, 'k.');
    s3.MarkerEdgeAlpha = 0.05;
    
    
    xlabel('\theta_0'); ylabel('\theta_1'); zlabel('reward');
    hold on
    s1 = surf(SamplingOut.threshPredMatLower, SamplingOut.threshPredMatUpper, SamplingOut.rewardPredMat);
    s1.EdgeColor = 'none';
    s2 = scatter3(SamplingOut.optThresh.lowerThresh, SamplingOut.optThresh.upperThresh, SamplingOut.optReward, 'ro');
    s2.MarkerEdgeColor = [0 0 0];
    s2.MarkerFaceColor = [1 0 0];
    
    % [sortedThreshold, indices] = sort(SamplingOut.threshold);
    % sortedRewardPred = SamplingOut.rewardPred(indices);
    % plot(sortedThreshold, sortedRewardPred, 'r', 'LineWidth', 1.5);
    % v1 = vline(SamplingOut.optThresh,'r:');
    % v1.LineWidth = 1.5;
end
end


