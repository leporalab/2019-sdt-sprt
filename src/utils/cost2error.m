function [tpr, fpr, x] = cost2error(threshNullArray, threshAltArray, cr, models)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

% Calculate bayes risk for the cost ratio and thresholds used to train the models
BR = zeros(length(threshNullArray), length(threshAltArray));
for j = 1:length(threshNullArray)
    for i = 1:length(threshAltArray)
        BR(i,j) = bayesrisk([threshNullArray(j) threshAltArray(i)], cr, models);
    end
end

% Minimize bayes risk
theta_ini = [1, 1];
A = []; b = []; Aeq = []; beq = []; lb = [0, 0]; ub = [];
[x, ~] = fmincon(@(theta) bayesrisk(theta, cr, models), theta_ini, A,b,Aeq,beq,lb,ub);

% Get threshold pairs into a table as expected by model predict functions
t = table(x(1), x(2));
t.Properties.VariableNames = {'threshNull', 'threshAlt'};

% Predict TPR and FRP for threshold pair
tpr = models.thresh2Tpr.predictFcn(t);
fpr = models.thresh2Fpr.predictFcn(t);

end

