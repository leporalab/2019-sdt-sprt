function br = bayesrisk(theta, cr, models)
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here

% Get threshold pairs into a table as expected by model predict functions
t = table(theta(1), theta(2));
t.Properties.VariableNames = {'threshNull', 'threshAlt'};

% Calculate Bayes Risk for threshold pair
br = cr(1)*models.thresh2Fpr.predictFcn(t) + cr(2)*(1 - models.thresh2Tpr.predictFcn(t)) + ...
    models.thresh2En.predictFcn(t);

end

