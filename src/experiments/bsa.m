clear
close all

% Animate results?
isanim = 1;

% Set number of hypotheses and sensory data channels
num_hyp = 3;
num_channels = 1;

%%
priors_arr = (1/num_hyp) * ones(num_hyp, 1);
probthresh_arr = 0.8 * ones(num_hyp, 1);
Theta_arr = -log(probthresh_arr);

% Setup hypotheses and corresponding sensory channel data distributions
% Set pdf parameters
hyp_means = cell(1, num_hyp);
hyp_sds = cell(1, num_hyp);
for m = 1:num_hyp
    hyp_means{m} = 10*rand(1, num_channels);
    %hyp_sds{m} = 2*rand(1, num_channels);
    hyp_sds{m} = 5*ones(1, num_channels);
end
% Fill hypotheses structure with distribution parameters
H = struct('name', 'Normal', 'mu', hyp_means, 'sd', hyp_sds);

% Randomly select true hypothesis
H_true = randi(num_hyp);

%% Simulation

simtime = 1000;

% Preallocate storage arrays
s_arr = zeros(num_channels, simtime);
O_arr = zeros(num_hyp, simtime);
Y_arr = zeros(num_hyp, simtime);

x_t = zeros(num_hyp, num_channels);

% Initialise array values at time t = 0
Y_arr(:,1) = log(priors_arr);
O_arr(:,1) = -Y_arr(:,1);

% Step through time array
for t = 2:simtime
    
    % Sample from true hypothesis channel distributions
    for k = 1:num_channels
        s_arr(k,t) = icdf(H(H_true).name, rand(1), H(H_true).mu(k), H(H_true).sd(k));
    end
    
    % Calculate likelihoods over hypotheses
    for m = 1:num_hyp
        for k = 1:num_channels
            x_t(m,k) = pdf(H(m).name, s_arr(k,t), H(m).mu(k), H(m).sd(k)); % likelihood of individual observations
        end
    end
    X_t = log(prod(x_t,2)); % this is the log combined likelihood for observations (s_1,...,s_K) given H_m
    
    % Iterate evidence, Y_arr(t)
    Y_arr(:,t) = Y_arr(:,t-1) + X_t;
    
    % Calculate the negative log posterior, O_vec
    O_arr(:,t) = -Y_arr(:,t) + logsumexp(Y_arr(:,t));
    
    % Test if threshold is met
    if any(O_arr(:,t) < Theta_arr)
        break
    end
    
end

% Set decision time and delta according to
dec_time = t-1;
[M, delta] = min(O_arr(:,t));

% Set textbox string
if H_true == delta && t < simtime
    iscorrect_str = 'Correct channel determined on';
elseif ~(H_true == delta) && t < simtime
    iscorrect_str = 'Incorrect channel determined on';
else
    iscorrect_str = 'No decision in simtime';
end

%% Plot results

f1 = figure;
set(f1, 'color','white', 'position', [63 621 1000 350])
C = linspecer(num_hyp); % set attractive colour order

subplot(1,2,1)
% Set axis
axis([0,dec_time, -1, 10]);
hold on
ylabel('O(T)')
xlabel('T')
plot([0 dec_time], [Theta_arr(delta) Theta_arr(delta)], ':')
x = 0:dec_time;
y = O_arr(:,1:t);

if isanim
    % Generate animatedline object array
    for kk = 1:num_hyp
        a1(kk) = animatedline;
        set(a1(kk), 'linestyle',':', 'color', C(kk,:));
    end
    set(a1(H_true), 'linewidth', 1, 'linestyle','-')
    legend(a1(H_true), 'True')
    
    % Plot evolution of -log(posterior) of all hypotheses
    for n = 1:t
        for kk = 1:num_hyp
            addpoints(a1(kk), x(n), y(kk,n));
        end
        drawnow 
     
    end
    tb = annotation('textbox', [0.03 0.2 0.07 0.07]);
    tb.String = iscorrect_str;
end

% Plot s_true counts up to dec_time
isfast = dec_time < 11;
subplot(1,2,2)
if ~isfast
    % Plot all observations of s_true as hist up to dec_time
    tmp = s_arr(:,2:t);
    for p = 1:num_channels
        h1(p) = histogram(tmp(p,:),10);
        h1(p).Normalization = 'probability';
        hold on
    end
    colorList = get(gca,'ColorOrder');
else
    % Plot all observations of s_true as scatter up to dec_time
    data1 = zeros(1, num_channels * (dec_time-1));
    tmp = s_arr(:,2:dec_time);
    data2 = reshape(tmp', 1, num_channels * (dec_time-1));
    group = ones(1, (dec_time-1));
    if num_channels > 1
        for i = 2:num_channels
            group = [group, i*ones(1, (dec_time-1))];
        end
    end
    colorList = get(gca,'ColorOrder');
    g1 = gscatter(data2, data1, group, colorList, '+', 8, 'off');
    xlabel('s^({k)}')
    ylabel('')
    hold on
end

% Plot pdfs of true hypothesis (and delta if not the same)
if H_true == delta
    plot_arr = delta;
else
    plot_arr = [H_true delta];
end
for hh = plot_arr
    for iter_chan = 1:num_channels
        mu = H(hh).mu(iter_chan);
        sd = H(hh).sd(iter_chan);
        xx = linspace(mu - 3*sd, mu + 3*sd, 100);
        pdf_plot = pdf(H(hh).name, xx, mu, sd);
        h2 = plot(xx, pdf_plot);
        if hh == delta
            set(h2, 'color', colorList(iter_chan,:), 'linewidth', 1.5);
        else
            set(h2, 'color', colorList(iter_chan,:), 'linewidth', 1.5, 'linestyle', ':');
        end
    end
end
if isfast
    yrange = get(gca, 'Ylim');
    yrange(1) = -yrange(2)/10; yrange(2) = yrange(2)*1.2;
    ylim(yrange)
end

% Draw hacky legend box
xrange = get(gca, 'Xlim'); yrange = get(gca, 'Ylim');
dumhist = histogram(1000, 'FaceColor','k');
xlim(xrange); ylim(yrange);
dum1 = plot(0,0,'-k');
dum2 = plot(0,0,':k');
xlabel('s^{(k)}')
ylabel('')
if ~(H_true == delta)
    legend([dumhist dum1 dum2], {'Histogram (probability) s^{(k)}','pdf_{true}^{(k)}', 'pdf_{selected}^{(k)}'}, 'Position',[0.85 0.8 0.1 0.1])
else
    legend([dumhist dum1], {'Histogram (probability) s^{(k)}',  'pdf_{true}^{(k)}'},'Position',[0.85 0.8 0.1 0.2])
end
