% Calls simbgmodel() and runs MSPRT over numHyp hypotheses with numChanel
% sensory channels.

clear
close all

% Set number of hypotheses and sensory data channels
numHyp = 3;
numChannel = 2;
probThresh = 0.999;

% Set number of time steps to simulate
simTime = 1000;

% Animate results?
isAnim = 1;

%% Run simulation
[ HTrue, H, delta, decTime, sArray, OArray ] = simbgmodel( numHyp, numChannel, probThresh, simTime );

%% Plot results
f1 = figure;
set(f1, 'color','white', 'position', [100 100 1000 350])
C = linspecer(numHyp); % set attractive colour order

subplot(1,2,1)
% Set axis
axis([0,decTime, -1, 10]);
hold on
ylabel('O_k(t)')
xlabel('t')
theta = -log(probThresh);
plot([0 decTime], [theta theta], ':')
x = 0:decTime;
y = OArray(:,1:decTime+1);

% Set textbox string
if HTrue == delta && decTime < simTime
    iscorrect_str = 'Correct';
elseif ~(HTrue == delta) && decTime < simTime
    iscorrect_str = 'Incorrect';
else
    iscorrect_str = 'No decision in simtime';
end

isVerySlow = decTime > 100;
if isAnim
    % Generate animatedline object array
    for kk = 1:numHyp
        a1(kk) = animatedline;
        set(a1(kk), 'linestyle',':', 'color', C(kk,:));
    end
    set(a1(HTrue), 'linewidth', 2, 'linestyle','-')
    legend(a1(HTrue), 'True')
    
    % Plot evolution of -log(posterior) of all hypotheses
    for n = 1:decTime+1
        for kk = 1:numHyp
            addpoints(a1(kk), x(n), y(kk,n));
        end
        if isVerySlow
            drawnow limitrate
        else
            drawnow
        end
        
    end
    tb = annotation('textbox', [0.03 0.2 0.07 0.07]);
    tb.String = iscorrect_str;
end

% Plot s_true counts up to decTime
isFast = decTime < 11;
subplot(1,2,2)
if ~isFast
    % Plot all observations of s_true as hist up to decTime
    tmp = sArray(:,2:decTime+1);
    for p = 1:numChannel
        h1(p) = histogram(tmp(p,:),10);
        h1(p).Normalization = 'probability';
        hold on
    end
    colorList = get(gca,'ColorOrder');
else
    % Plot all observations of s_true as scatter up to decTime
    data1 = zeros(1, numChannel * (decTime-1));
    tmp = sArray(:,2:decTime);
    data2 = reshape(tmp', 1, numChannel * (decTime-1));
    group = ones(1, (decTime-1));
    if numChannel > 1
        for i = 2:numChannel
            group = [group, i*ones(1, (decTime-1))];
        end
    end
    colorList = get(gca,'ColorOrder');
    g1 = gscatter(data2, data1, group, colorList, '+', 8, 'off');
    xlabel('s^({j)}')
    ylabel('')
    hold on
end

% Plot pdfs of true hypothesis (and delta if not the same)
if HTrue == delta
    plot_arr = delta;
else
    plot_arr = [HTrue delta];
end
for hh = plot_arr
    for iter_chan = 1:numChannel
        mu = H(hh).mu(iter_chan);
        sd = H(hh).sd(iter_chan);
        xx = linspace(mu - 3*sd, mu + 3*sd, 100);
        pdf_plot = pdf(H(hh).name, xx, mu, sd);
        h2 = plot(xx, pdf_plot);
        if hh == delta
            set(h2, 'color', colorList(iter_chan,:), 'linewidth', 1.5);
        else
            set(h2, 'color', colorList(iter_chan,:), 'linewidth', 1.5, 'linestyle', ':');
        end
    end
end
if isFast
    yrange = get(gca, 'Ylim');
    yrange(1) = -yrange(2)/10; yrange(2) = yrange(2)*1.2;
    ylim(yrange)
end

% Draw hacky legend box
xrange = get(gca, 'Xlim'); yrange = get(gca, 'Ylim');
dumhist = histogram(1000, 'FaceColor','k');
xlim(xrange); ylim(yrange);
dum1 = plot(0,0,'-k');
dum2 = plot(0,0,':k');
xlabel('s^{(j)}')
ylabel('')
if ~(HTrue == delta)
    legend([dumhist dum1 dum2], {'Histogram (probability) s^{(j)}','pdf_{true}^{(j)}', 'pdf_{selected}^{(j)}'}, 'Position',[0.85 0.8 0.1 0.1])
else
    legend([dumhist dum1], {'Histogram (probability) s^{(j)}',  'pdf_{true}^{(j)}'},'Position',[0.85 0.8 0.1 0.2])
end