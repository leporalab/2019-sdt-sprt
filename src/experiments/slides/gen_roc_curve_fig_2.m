% Plot ROC curves for average over multiple time steps in signal detection
% theory.

clear
close all

% CHOOSE
nSamples = 500;
nThreshold = 50;
nXvals = 30;
isSamplePerThreshold = 1;
isSequentialAnalysis = 1;
T = [1 5 10 15 20 25];

discrim = 0.5;
sd = 1;
mu1 = 2;

%% INITIALISE
tmpMu = zeros(2, 1);
calcMu2 = discrim * sd + mu1;

Hypotheses(1).mu = mu1;
Hypotheses(1).sd = sd;
Hypotheses(1).name = 'normal';
Hypotheses(2).mu = calcMu2;
Hypotheses(2).sd = sd;
Hypotheses(2).name = 'normal';

%% DO
s = sdt(Hypotheses, nThreshold, nSamples, 'SequentialAnalysis', 1, 'avTimes', T);

%% PLOT
f2 = figure;
set(f2, 'color','white', 'position', [120 120 600 500])
C = linspecer(length(s.avTimes)); % set attractive colour order

% MC ROC curves
%subplot(1,2,1)
axis([0, 1, 0, 1])
h8 = plot([0 1], [0 1], 'k:');
set(get(get(h8, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle', 'off');
hold on
for plt = 1:length(s.avTimes)
    h9 = plot(s.fprPlotArray(:,plt), s.tprPlotArray(:,plt));
    set(h9, 'linestyle','-', 'color', C(plt,:));
end
%plot(s.fprPlotArray', s.tprPlotArray', 'linestyle', '--', 'Color', [150/256, 150/256, 150/256], 'linewidth', 0.5);
titleStr = sprintf("%s ROC curve", 'Monte Carlo');
title(titleStr)
xlabel('False positive rate'); ylabel('True positive rate');
for kk = 1:length(s.avTimes)
    legendCellArr{kk} = sprintf("T = %d", s.avTimes(kk));
end
legend(legendCellArr)
t = annotation('textbox');
t.String = 'Mean of T observations per sample. Dashed lines indicate data points with same decision threshold.';
t.FontSize = 10;
%t.EdgeColor = 'none';
t.Position = [0.45 0.25 0.4 0.12];
x = [0.6 0.4]; y = [0.7 0.5];
ar = annotation('textarrow',x,y);
t = annotation('textbox');
t.String = 'Low threshold';
t.FontSize = 10;
t.EdgeColor = 'none';
t.Position = [0.55 0.65 0.4 0.1];
t = annotation('textbox');
t.String = 'High threshold';
t.FontSize = 10;
t.EdgeColor = 'none';
t.Position = [0.30 0.4 0.4 0.1];

% Exact ROC curves
%subplot(1,2,2)
% axis([0, 1, 0, 1])
% h8 = plot([0 1], [0 1], 'k:');
% set(get(get(h8, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle', 'off');
% hold on
for plt = 1:length(s.avTimes)
    h9 = plot(s.fprArrayExact(:,plt), s.tprArrayExact(:,plt));
    set(h9, 'linestyle','--', 'color', C(plt,:));
end
%plot(s.fprArrayExact', s.tprArrayExact', 'linestyle', '--', 'Color', [150/256, 150/256, 150/256], 'linewidth', 0.5);
% titleStr = sprintf("%s ROC curve", 'Exact');
% title(titleStr)
% xlabel('False positive rate'); ylabel('True positive rate');
% for kk = 1:length(s.avTimes)
%     legendCellArr{kk} = sprintf("T = %d", s.avTimes(kk));
% end
% legend(legendCellArr)
% t = annotation('textbox');
% t.String = 'Mean of T observations.';
% t.FontSize = 10;
% %t.EdgeColor = 'none';
% t.Position = [0.45 0.25 0.4 0.12];
% x = [0.6 0.4]; y = [0.7 0.5];
% ar = annotation('textarrow',x,y);
% t = annotation('textbox');
% t.String = 'Low threshold';
% t.FontSize = 10;
% t.EdgeColor = 'none';
% t.Position = [0.55 0.65 0.4 0.1];
% t = annotation('textbox');
% t.String = 'High threshold';
% t.FontSize = 10;
% t.EdgeColor = 'none';
% t.Position = [0.30 0.4 0.4 0.1];