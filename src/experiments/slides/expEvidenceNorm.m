function [out] = expEvidenceNorm(mu0, mu1, sd, trueMu)
%EXPEVIDENCENORM Returns expected evidence increment for two normal
%hypothesis distributions with equal variace.
%   Detailed explanation goes here
out = 1/(2*sd^2) * (2*(mu1 - mu0)*trueMu + mu0^2 - mu1^2);

end

