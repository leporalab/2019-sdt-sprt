% Plot ROC curves for average over multiple time steps in signal detection
% theory.

clear
close all

% CHOOSE
nSamples = 500;
nThreshold = 1000;
nXvals = 30;
maxT = 100;
nTimeSteps = 20;
T = round(linspace(1,maxT,nTimeSteps));
nCritSteps = 100;

discrim = [1 0.5 0.2];
sd = 1;
mu1 = 2;

%% INITIALISE

f2 = figure;
set(f2, 'color','white', 'position', [120 120 1200 350])

for i = 1:length(discrim)
    tmpMu = zeros(2, 1);
    calcMu2 = discrim(i) * sd + mu1;
    
    Hypotheses(1).mu = mu1;
    Hypotheses(1).sd = sd;
    Hypotheses(1).name = 'normal';
    Hypotheses(2).mu = calcMu2;
    Hypotheses(2).sd = sd;
    Hypotheses(2).name = 'normal';
    
    %% DO
    s = sdt(Hypotheses, nThreshold, nSamples, 'SequentialAnalysis', 1, 'SamplePerThreshold', 0, 'avTimes', T);
    
    %% PLOT
    C = linspecer(length(s.avTimes)); % set attractive colour order
    
    subplot(1,length(discrim), i)
    axis([0, 1, 0, 1])
    h8 = plot([0 1], [0 1], 'k:');
    set(get(get(h8, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle', 'off');
    hold on
    for plt = 1:length(s.avTimes)
        h9 = plot(s.fprArrayExact(:,plt), s.tprArrayExact(:,plt));
        set(h9, 'linestyle','-', 'color', C(plt,:));
    end
    titleStr = sprintf("%s ROC curve: d' = %.1f", 'Exact', discrim(i));
    title(titleStr)
    xlabel('False positive rate'); ylabel('True positive rate');
    for kk = 1:length(s.avTimes)
        legendCellArr{kk} = sprintf("T = %d", s.avTimes(kk));
    end
    % t = annotation('textbox');
    % t.String = 'Mean of T observations per sample. Dashed lines indicate data points with same decision threshold.';
    % t.FontSize = 10;
    % %t.EdgeColor = 'none';
    % t.Position = [0.45 0.25 0.4 0.12];
    % x = [0.6 0.4]; y = [0.7 0.5];
    % ar = annotation('textarrow',x,y);
    % t = annotation('textbox');
    % t.String = 'Low threshold';
    % t.FontSize = 10;
    % t.EdgeColor = 'none';
    % t.Position = [0.55 0.65 0.4 0.1];
    % t = annotation('textbox');
    % t.String = 'High threshold';
    % t.FontSize = 10;
    % t.EdgeColor = 'none';
    % t.Position = [0.30 0.4 0.4 0.1];
    
    %% Draw threshold coords
    idx = round(linspace(1,nThreshold,nCritSteps)); %// get rounded equally spaced indices
    tmp = s.fprArrayExact';
    xplot = tmp(:,idx);
    tmp = s.tprArrayExact';
    yplot = tmp(:,idx);
    h9 = plot(xplot, yplot);
    set(h9, 'linestyle','--', 'linewidth', 0.5, 'Color', [150/256, 150/256, 150/256]);
    if i == length(discrim)
        legend(legendCellArr)
    end
end
