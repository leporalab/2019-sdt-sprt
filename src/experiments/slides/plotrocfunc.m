close all
clear

sd = 1;
mu0 = -0.25;
mu1 = 0.25;
Hypotheses(1).mu = mu0;
Hypotheses(1).sd = sd;
Hypotheses(1).name = 'normal';
Hypotheses(2).mu = mu1;
Hypotheses(2).sd = sd;
Hypotheses(2).name = 'normal';

C = [1 3 5 8 12 15];
alpha = [0.1 0.4];
beta = 0.001:0.01:0.9999;

f2 = figure;
f2.Color = [1 1 1];
for plt = 1:length(C)
    for i = 1:length(alpha)
        y(i,:) = arrayfun(@(x) rocfunc(x, alpha(i), C(plt), Hypotheses, 1), beta);
        legendstr{i} = strcat('alpha = ', num2str(alpha(i)));
    end
    
    subplot(3,2, plt)
    plot(beta, y)
    hold on
    hline(0, 'k:')

    ylabel('y'); xlabel('\beta');
    title(strcat('To find root of beta for n = ', num2str(C(plt))));
end
    legend(legendstr)

f2.Units               = 'centimeters';
f2.Position(1) = 1;
f2.Position(2) = 2;
f2.Position(3)         = 18.5; % two columns
%f2.Position(3)         = 9; % one column
f2.Position(4)         = 18;
% set text properties
set(f2.Children, ...
    'FontName',     'Arial', ...
    'FontSize',     8);

set(gcf, 'Renderer', 'painters')

pos = get(f2,'Position');
set(f2,'PaperPositionMode','Auto','PaperUnits','centimeters','PaperSize',[pos(3), pos(4)])
