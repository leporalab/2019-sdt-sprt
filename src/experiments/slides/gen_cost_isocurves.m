clear
close all

%% CHOOSE

%Distribution options
discrim = 0.5;
sd = 1;
mu1 = 2;

calcMu2 = discrim * sd + mu1;
Hypotheses(1).mu = mu1;
Hypotheses(1).sd = sd;
Hypotheses(1).name = 'normal';
Hypotheses(2).mu = calcMu2;
Hypotheses(2).sd = sd;
Hypotheses(2).name = 'normal';

%SPRT options
nEpisodes = 500;
T = [1, 2:3:50];
threshAltArray = linspace(0, 3, 5);
threshNullArray = linspace(0, 3, 5);

%% Get data and train models

%SPRT
[roc, models] = trainthresh2models(Hypotheses, threshNullArray, threshAltArray, T, 'nEpisodes', 500, 'FitToAv', 1);

%% Get error rates for threshold iso-curves
%maxCostRatio = findmaxcost(threshNullArray, threshAltArray, models);
maxCostRatio = 200;

%ptsPerFix = 20;
% isoNullCost = linspace(1, maxCostRatio, ptsPerFix);
% isoAltCost = linspace(1, maxCostRatio, ptsPerFix);
%%%%%
isoCosts = [1 15 20 25 30 40 80 100 200];
ptsPerFix = length(isoCosts);
isoNullCost = isoCosts;
isoAltCost = isoCosts;
%%%
results0cell = cell(1, length(isoNullCost));
results1cell = cell(1, length(isoAltCost));

f = waitbar(0,'Calculating iso cost curves', 'Name', 'Progress');
for i = 1:length(isoNullCost)
    tabNull = isoNullCost(i).*ones(1,ptsPerFix)';
    tabAlt = isoAltCost';
    t0 = table(tabNull, tabAlt);
    t0.Properties.VariableNames = {'NullCost', 'AltCost'};
    for j = 1:length(tabNull)
        cr = [tabNull(j), tabAlt(j)];
        [tpr(j), fpr(j), ~] = cost2error(threshNullArray, threshAltArray, cr, models);
    end
    t0.tpr = tpr';
    t0.fpr = fpr';
    results0cell{i} = t0;
    waitbar(i/(2*length(isoNullCost)), f);
end

for i = 1:length(isoAltCost)
    tabNull = isoNullCost';
    tabAlt = isoAltCost(i).*ones(1,ptsPerFix)';
    t1 = table(tabNull, tabAlt);
    t1.Properties.VariableNames = {'NullCost', 'AltCost'};
    for j = 1:length(tabNull)
        cr = [tabNull(j), tabAlt(j)];
        [tpr(j), fpr(j), ~] = cost2error(threshNullArray, threshAltArray, cr, models);
    end
    t1.tpr = tpr';
    t1.fpr = fpr';
    results1cell{i} = t1;
    waitbar((length(isoNullCost)+i)/(2*length(isoNullCost)), f);
end
close (f)

%% Plot threshold isocurves
leg = findobj(gcf,'Tag','legend');
legStr = get(leg, 'String');
%     f2 = figure;
%     set(f2, 'color','white', 'position', [150 150 600 500])
%     %SDT
%     axis([0, 1, 0, 1])
h8 = plot([0 1], [0 1], 'k:');

set(get(get(h8, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle', 'off');
     hold on
for i = 1:length(results0cell)
    plt_tab = results0cell{i};
    h1(i) = plot(plt_tab.fpr, plt_tab.tpr);
    %set(h1(i), 'linestyle', '--', 'color', 1/256.*[128 128 128],...
    set(h1(i), 'linestyle', '--', 'color', 'r',...
        'linewidth', 0.5);
end
for i = 1:length(results1cell)
    plt_tab = results1cell{i};
    h2(i) = plot(plt_tab.fpr, plt_tab.tpr);
    %set(h2(i), 'linestyle', '--', 'color', 1/256.*[128 128 128],...
    set(h2(i), 'linestyle', '--', 'color', 'b',...
        'linewidth', 0.5);
end
legend(legStr)
