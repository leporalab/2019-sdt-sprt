clear
close all

n = 5;
mu = 0;
var = 1;

X = -5:0.01:5;

for i = 1:n
    pdfArray = pdf('normal', X, mu, var/i);
    cdfArray = cdf('normal', X, mu, var/i);
    subplot(1,2,1)
    plot(X, pdfArray)
    hold on
    subplot(1,2,2)
    plot(X, cdfArray)
    hold on
end

