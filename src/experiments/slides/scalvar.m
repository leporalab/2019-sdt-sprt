%% CHOOSE

%Distribution options
discrim = 0.5;
sd = 1;
mu1 = 2;

calcMu2 = discrim * sd + mu1;
Hypotheses(1).mu = mu1;
Hypotheses(1).sd = sd;
Hypotheses(1).name = 'normal';
Hypotheses(2).mu = calcMu2;
Hypotheses(2).sd = sd;
Hypotheses(2).name = 'normal';

threshNull = 1;
threshAlt = 1;

hTrue = 1;

maxT = 10000;

%SPRT options
nEpisodes = 5000;

%%
for i = 1:nEpisodes
    [sel(i), Y, decTime(1, i)] = sprt_var(Hypotheses, 1, hTrue, maxT, threshNull, threshAlt);
end

for i = 1:nEpisodes
    [sel(i), Y, decTime(2, i)] = sprt_var(Hypotheses, 2, hTrue, maxT, threshNull, threshAlt);
end
edges = 1:10;
histogram(decTime(1,:), edges)
figure
histogram(decTime(2,:), edges)