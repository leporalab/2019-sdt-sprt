% This script produces a figure comparing exact and MC sampling for signal
% detection theory.

clear
close all

%% CHOOSE
nSamples = 500;
nThreshold = 25;
nXvals = 30;
isSamplePerThreshold = 1;

n = 5;
c = 1/n;

discrim = 0.5;
sd = 1;
mu1 = 2;

%% INITIALISE
tmpMu = zeros(2, 1);
calcMu2 = discrim * sd + mu1;

Hypotheses(1).mu = mu1;
Hypotheses(1).sd = sd;
Hypotheses(1).name = 'normal';
Hypotheses(2).mu = calcMu2;
Hypotheses(2).sd = sd;
Hypotheses(2).name = 'normal';

%% DO
s = sdt(Hypotheses, nThreshold, nSamples, 'SamplePerThreshold', 'true');

%% PLOT
f1 = figure;
set(f1, 'color','white')

C = get(gca,'colororder'); % set attractive colour order

xValArray = linspace(Hypotheses(s.hNoise).mu - Hypotheses(s.hNoise).sd*3, Hypotheses(s.hSignal).mu + Hypotheses(s.hSignal).sd*3, nXvals);

subplot(1,3,1)
for i = [s.hNoise, s.hSignal]
    pdfArray(i,:) = pdf(Hypotheses(i).name, xValArray, Hypotheses(i).mu, Hypotheses(i).sd);
end
h1 = plot(xValArray, pdfArray(1,:), 'linewidth', 1);
hold on
h2 = plot(xValArray, pdfArray(2,:), 'linewidth', 1);
title('Probability distributions')
ylabel('f(x)'); xlabel('x')
%legend([h1 h2], {'Noise', 'Signal + noise'})
probDistYLim = ylim;
box off

% subplot(2,3,4)
% edges = xValArray;
% if ~isSamplePerThreshold
%     histogram(sNullArray, edges)
%     hold on
%     histogram(sTrueArray, edges)
%     titleStr = sprintf("%d samples per distribution", nSamples);
% else
%     histogram(reshape(s.NoiseArrayPerThresholdCount, [], 1), edges)
%     hold on
%     histogram(reshape(s.SignalArrayPerThresholdCount, [], 1), edges);
%     titleStr = sprintf("%d samples per threshold", nSamples);
% end
% xlabel('x'); ylabel('Count');
% legend('Noise', 'Signal + noise')
% title(titleStr)
% histYLim = ylim;

% subplot(2,3,6)
% axis([0, 1, 0, 1])
% hold on
% h3 = plot([0 1], [0 1], 'k:');
% titleStr = sprintf("%s ROC curve", 'Monte Carlo');
% title(titleStr)
% xlabel('False positive rate'); ylabel('True positive rate');
% e1 = errorbar(s.fprArray, s.tprArray, s.tprVarArray, s.tprVarArray,...
%     zeros(1, length(s.fprArray)), zeros(1, length(s.fprArray)), '.', 'capsize', 0);
% set(e1, 'color', C(2,:), 'linewidth', 1);
% e2 = errorbar(s.fprArray, s.tprArray, zeros(1, length(s.fprArray)), ...
%     zeros(1, length(s.fprArray)), s.fprVarArray, s.fprVarArray, '.', 'capsize', 0);
% set(e2, 'color', C(1,:), 'linewidth', 1);
% plot(s.fprArray, s.tprArray, '--ok', 'linewidth', 1);
% ax1 = gca; ax1.YColor = C(2,:); ax1.XColor = C(1,:);

subplot(1,3,3)
axis([0, 1, 0, 1])
hold on
h5 = plot([0 1], [0 1], 'k:');
titleStr = sprintf("%s ROC curve", 'Exact');
title(titleStr)
ylabel('True positive rate'); xlabel('False positive rate')
box off
e1 = errorbar(s.fprArrayExact, s.tprArrayExact, c.*s.tprVarArrayExact, c.*s.tprVarArrayExact,...
    zeros(1, length(s.fprArrayExact)), zeros(1, length(s.fprArrayExact)), '.', 'capsize', 0);
set(e1, 'color', C(2,:), 'linewidth', 1);
e2 = errorbar(s.fprArrayExact, s.tprArrayExact, zeros(1, length(s.fprArrayExact)), ...
    zeros(1, length(s.fprArrayExact)), c.*s.fprVarArrayExact, c.*s.fprVarArrayExact, '.', 'capsize', 0);
set(e2, 'color', C(1,:), 'linewidth', 1);
plot(s.fprArrayExact, s.tprArrayExact, 'k', 'linewidth', 1);
t1 = annotation('textbox');
t1.String = 'Error bars show the variance';
t1.FontSize = 7;
t1.EdgeColor = 'none';
t1.Position = [0.82 0.58 0.07 0.15];
ax1 = gca; ax1.YColor = C(2,:); ax1.XColor = C(1,:);

subplot(1,3,2)
b0 = boundedline(s.threshArray, s.fprArrayExact, c.*s.fprVarArrayExact, 'alpha', 'cmap', C(1,:));
%b0 = plot(s.threshArray, s.fprArrayExact, 'color', C(1,:),'linewidth', 1);
hold on
b1 = boundedline(s.threshArray, s.tprArrayExact, c.*s.tprVarArrayExact , 'alpha', 'cmap', C(2,:));
%b1 = plot(s.threshArray, s.tprArrayExact, 'color', C(2,:), 'linewidth', 1);
title('Exact true and false positive rates')
ylabel('Exact rate'); xlabel('Decision threshold, \theta')
legend([b0 b1], {'FPR', 'TPR'}, 'location', 'southwest')
box off

% subplot(2,3,5)
% b0 = boundedline(s.threshArray, s.fprArray, s.fprVarArray, 'alpha', 'cmap', C(1,:));
% hold on
% b1 = boundedline(s.threshArray, s.tprArray, s.tprVarArray , 'alpha', 'cmap', C(2,:));
% title('MC true and false positive rates')
% xlabel('Decision threshold'); ylabel('Rate')
t = annotation('textbox');
t.String = 'Boundary lines show the variance';
t.FontSize = 7;
t.EdgeColor = 'none';
t.Position = [0.55 0.75 0.07 0.15];
% legend([b0 b1], {'FPR', 'TPR'}, 'location', 'southwest')


%%
f1.Units               = 'centimeters';
f1.Position(3)         = 18.5;
f1.Position(4)         = 5;
% set text properties
set(f1.Children, ...
    'FontName',     'Arial', ...
    'FontSize',     8);

% remove unnecessary white space
set(gca,'LooseInset',max(get(gca,'TightInset'), 0.02))

%% export to png
f1.PaperPositionMode   = 'auto';
print(['fig_1_user_var'], '-dpng', '-r600')
