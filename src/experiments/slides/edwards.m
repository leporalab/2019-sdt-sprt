close all
clear

% Normal distribution parameters
%discrim = 0.5;
sd = 1;
mu0 = -1/3;
mu1 = 1/3;
%mu1 = discrim * sd + mu0;

% Set true hypothesis
mu_true = mu1;

% Define cost ratios
w0 = 10;
w1 = 10;

% Calculate expected evidence for normal case where unknown parameter is true mean
expected_evidence = 1 / (2*sd^2) * (2*(mu1 - mu0) * mu_true + mu0^2 - mu1^2);

% Approximate equations for optimal thresholds from Edwards (1965)
F = w0 * expected_evidence;
G = w1 * expected_evidence;
fun = @(X) [F - log(X(1)/X(2)) + X(2) - X(1); G - (1/X(2) - 1/X(1)) - log(X(1)/X(2))];

% Solve approximate optimal thresholds on the likelihood ratio
x = fsolve(fun, [0.5, 0.2]);

% Print approximate optimal thresholds on log LR
log(x)
% Display table of inputs and outputs here