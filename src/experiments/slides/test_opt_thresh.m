clear
close all

%% Set parameters.
% Define hypotheses distributions.
Hyp.mean = [-1/3, 1/3]; % 1 index is 'noise', 2 index is 'signal'
Hyp.sd = 1;
Hyp.nHyp = 2;
Hyp.nTrials = 1e6;
Hyp.maxT = 200;

% Generate evidence and select true hypotheses (targets).
[Hyp.evidence, Hyp.target] = generateSprtEvidence_rap(Hyp);

sprtBounds = [0, 0];

% Apply SPRT decision rule to evidence.
[~, decTime, errorType] = makeSprtDecision(Hyp, sprtBounds,...
    'decisions', 'all');

% Calculate reward (inner product of errorType and error costs plus
% total sampling cost)
costRatios = [100 100];
reward = - errorType*costRatios' - decTime;

mean(reward)
std(reward)