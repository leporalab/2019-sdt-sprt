clear
close all

sd = 1;
mu0 = -0.25;
mu1 = 0.25;
Hypotheses(1).mu = mu0;
Hypotheses(1).sd = sd;
Hypotheses(1).name = 'normal';
Hypotheses(2).mu = mu1;
Hypotheses(2).sd = sd;
Hypotheses(2).name = 'normal';

maxT = 37;
nTimeSteps = 10;
tArr = round(linspace(5,maxT,nTimeSteps));

[alpha, beta] = getRoc(Hypotheses, tArr);

