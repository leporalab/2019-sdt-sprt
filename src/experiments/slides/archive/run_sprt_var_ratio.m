% This script runs implementations of the simulations suggested by Nathan's
% annotations on basal ganglia slide stack.

clear
close all

%% CHOOSE
nEpisodes = 500;

%% INITIALISE
tmpMu = zeros(2, 1);

maxT = 10000;

mu0Array = [0.5 1.5 2.5];
iterSubPlot = 1;

f1 = figure;
set(f1, 'color','white', 'position', [100 100 600 800])
C = linspecer(5); % set attractive colour order

for iterMu = 1:length(mu0Array)
    
    threshArray = linspace(0.01, 3.5, 10);
    threshRatioArray = [0.1, 1, 5];
    
    %threshAltArray = threshArray;
    threshNullArray = threshArray;
    hSelectedWhenAltTrue = zeros(nEpisodes, 1);
    hSelectedWhenNullTrue = zeros(nEpisodes, 1);
    tprArray = zeros(length(threshArray), length(threshRatioArray));
    fprArray = zeros(length(threshArray), length(threshRatioArray));
    avDecTimeArray = zeros(length(threshArray), length(threshRatioArray));
    threshRatioArrayRecord = zeros(length(threshArray), length(threshRatioArray));
    decTimeArray = zeros(length(nEpisodes), 2);
    
    %% DO
    
    % Hypotheses(1) is the null hypothesis and Hypotheses(2) is the
    % alternative hypothesis
    Hypotheses(1).mu = mu0Array(iterMu);
    Hypotheses(1).sd = 1;
    Hypotheses(1).name = 'normal';
    Hypotheses(2).mu = 3;
    Hypotheses(2).sd = 1;
    Hypotheses(2).name = 'normal';
    
    
    for m = 1:length(threshRatioArray)
        for j = 1:length(threshNullArray)
            threshAlt = threshNullArray(j) * threshRatioArray(m);
            fprintf('Performing SPRT over %d episodes for thresholds [%.3f %.3f] ...\n', nEpisodes, threshNullArray(j), threshAlt);
            for k = 1:nEpisodes
                [hSelectedWhenAltTrue(k), ~, decTimeArray(k, 1)] = sprt(Hypotheses, 2, maxT, threshNullArray(j), threshAlt);
                [hSelectedWhenNullTrue(k), ~, decTimeArray(k, 2)] = sprt(Hypotheses, 1, maxT, threshNullArray(j), threshAlt);
            end
            avDecTimeArray(j, m) = mean(decTimeArray(:));
            threshRatioArrayRecord(j, m) = threshAlt / threshNullArray(j);
            nHitsAlt = sum(hSelectedWhenAltTrue == 2);
            tprArray(j, m) = nHitsAlt / nEpisodes;
            nHitsNull = sum(hSelectedWhenNullTrue == 2);
            fprArray(j, m) = nHitsNull / nEpisodes;
            fprintf('TPR = %.2f, FPR = %.2f \n', tprArray(j,m), fprArray(j,m))
        end
    end
    
    
    %% PLOT
    
    % Flatten arrays for plotting
    avDecTimeArray = avDecTimeArray(:);
    threshRatioArrayRecord = threshRatioArrayRecord(:);
    tprArray = tprArray(:);
    fprArray = fprArray(:);
    
    subplot(3, 3, iterSubPlot)
    plotxArray = linspace(Hypotheses(1).mu - Hypotheses(1).sd*3, Hypotheses(2).mu + Hypotheses(2).sd*3, 30);
    for i = [1 2]
        pdfArray(i,:) = pdf(Hypotheses(i).name, plotxArray, Hypotheses(i).mu, Hypotheses(i).sd);
    end
    h1 = plot(plotxArray, pdfArray(1,:), 'linewidth', 1);
    hold on
    h2 = plot(plotxArray, pdfArray(2,:), 'linewidth', 1);
    title('Probability distributions')
    ylabel('p_i(x)'); xlabel('x')
    legend([h1 h2], {'p_0', 'p_1'})
    probDistYLim = ylim;
    if iterSubPlot == 1
        probDistXLim = xlim;
    else
        axis([probDistXLim probDistYLim]);
    end
    
    subplot(3, 3, iterSubPlot + 3)
    logLrArray = log(pdfArray(2,:) ./ pdfArray(1,:));
    h4 = plot(plotxArray, logLrArray, 'LineWidth', 2.5);
    hold on
    title('logLR vs. observation x')
    ylabel('logLR'); xlabel('x');
    if iterSubPlot == 1
        logYLim = ylim;
        logXLim = xlim;
    else
        axis([logXLim logYLim]);
    end
    plot(logXLim, [0 0], 'k:');
    plot(plotxArray, 10*pdfArray(2,:), 'linewidth', 0.5, 'color', C(2,:) + [0 .25 .25]);
    plot([Hypotheses(2).mu Hypotheses(2).mu], logYLim, 'r:')
    %     for iterP = 1:length(threshAltArray)
    %         ht1 = plot(logXLim, [threshAltArray(iterP), threshAltArray(iterP)]);
    %         ht2 = plot(logXLim, [-threshAltArray(iterP), -threshAltArray(iterP)]);
    %         set(ht1, 'Color', C(iterP, :), 'LineStyle', '-', 'LineWidth', 0.3);
    %         set(ht2, 'Color', C(iterP, :), 'LineStyle', '-', 'LineWidth', 0.3);
    %     end
    
    subplot(3, 3, iterSubPlot + 6)
    axis([0, 1, 0, 1])
    hold on
    sc1 = scatter(fprArray, tprArray, 50.*sqrt(threshRatioArrayRecord), avDecTimeArray, 'filled');
    h3 = plot([0 1], [0 1], 'k:');
    titleStr = sprintf("%s ROC space", 'SPRT');
    title(titleStr)
    ylabel('True positive rate'); xlabel('False positive rate')
    box off
    c1 = colorbar;
    c1.Label.String = 'Average steps to threshold';
        if iterSubPlot == 1
            t = annotation('textbox');
            t.String = sprintf('%d instances per data point. Bubble area \\propto (\\theta_1/\\theta_0)^{1/2}.', nEpisodes);
            t.FontSize = 9;
            t.EdgeColor = 'none';
            t.Position = [0.16 0.15 0.15 0.04];
            t2 = annotation('textbox');
            t2.String = '\theta_0 = linspace(0.01, 3.5, 10)    \theta_1/\theta_0 = [0.1 1 5]';
            t2.FontSize = 9;
            t2.Position = [0.74 0.14 0.12 0.05];
            t2.EdgeColor = 'none';
        end
    iterSubPlot = iterSubPlot + 1;
    
end
