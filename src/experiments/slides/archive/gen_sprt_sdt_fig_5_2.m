% This script runs implementations of the simulations suggested by Nathan's
% annotations on basal ganglia slide stack.

clear
close all

%% CHOOSE
f2 = figure;
set(f2, 'color','white', 'position', [150 150 1200 500])
discrimArr = [1.5 1.0 0.5 0.25];
iD = 1;
for discrim = discrimArr
    %Distribution options
    %discrim =
    var = 1;
    mu1 = 2;
    
    %SPRT options
    nEpisodes = 500;
    ContourLevels = [3 9 20];
    nContourLevels = length(ContourLevels);
    
    %SDT options
    nSamples = 500;
    nThreshold = 50;
    nXvals = 30;
    isSamplePerThreshold = 1;
    isSequentialAnalysis = 1;
    T = ContourLevels;
    
    
    %% INITIALISE
    tmpMu = zeros(2, 1);
    maxT = 10000;
    
    calcMu2 = discrim * sqrt(var) + mu1;
    
    % Hypotheses(1) is the null hypothesis and Hypotheses(2) is the
    % alternative hypothesis
    Hypotheses(1).mu = mu1;
    Hypotheses(1).var = var;
    Hypotheses(1).name = 'normal';
    Hypotheses(2).mu = calcMu2;
    Hypotheses(2).var = var;
    Hypotheses(2).name = 'normal';
    
    iterSubPlot = 1;
    
    threshAltArray = linspace(0.01, 4, 10);
    threshNullArray = linspace(0.01, 4, 10);
    hSelectedWhenAltTrue = zeros(nEpisodes, 1);
    hSelectedWhenNullTrue = zeros(nEpisodes, 1);
    tprArray = zeros(length(threshNullArray), length(threshAltArray));
    fprArray = zeros(length(threshNullArray), length(threshAltArray));
    avDecTimeArray = zeros(length(threshNullArray), length(threshAltArray));
    threshRatioArray = zeros(length(threshNullArray), length(threshAltArray));
    decTimeArray = zeros(length(nEpisodes), 2);
    
    %% DO
    
    %SPRT
    for m = 1:length(threshNullArray)
        for j = 1:length(threshAltArray)
            fprintf('Performing SPRT over %d episodes for thresholds [%.2f %.2f] ...\n', nEpisodes, threshNullArray(m), threshAltArray(j));
            for k = 1:nEpisodes
                [hSelectedWhenAltTrue(k), ~, decTimeArray(k, 1)] = sprt(Hypotheses, 2, maxT, threshNullArray(m), threshAltArray(j));
                [hSelectedWhenNullTrue(k), ~, decTimeArray(k, 2)] = sprt(Hypotheses, 1, maxT, threshNullArray(m), threshAltArray(j));
            end
            avDecTimeArray(m, j) = mean(decTimeArray(:));
            threshRatioArray(m, j) = threshAltArray(j) / threshNullArray(m);
            nHitsAlt = sum(hSelectedWhenAltTrue == 2);
            tprArray(m, j) = nHitsAlt / nEpisodes;
            nHitsNull = sum(hSelectedWhenNullTrue == 2);
            fprArray(m, j) = nHitsNull / nEpisodes;
            fprintf('TPR = %.2f, FPR = %.2f \n', tprArray(m,j), fprArray(m,j))
        end
    end
    
    %SDT
    s = sdt(Hypotheses, nThreshold, nSamples, 'SequentialAnalysis', 1, 'avTimes', T);
    
    %% PLOT
    C = linspecer(nContourLevels); % set attractive colour order
    % Flatten arrays for plotting
    avDecTimeArray = avDecTimeArray(:);
    threshRatioArray = threshRatioArray(:);
    tprArray = tprArray(:);
    fprArray = fprArray(:);
    
    % Prepare table for regression analysis
    newThreshNullArray = repmat(threshNullArray, 1, length(threshNullArray));
    newThreshAltArray = repelem(threshAltArray, length(threshAltArray));
    t = table(newThreshNullArray', newThreshAltArray', avDecTimeArray, tprArray, fprArray);
    t.Properties.VariableNames = {'threshNull', 'threshAlt', 'avT', 'tpr', 'fpr'};
    %save('gp_data2','t')
    
    f1 = figure;
    set(f1, 'color','white', 'position', [100 100 1400 350])
    
    %%%%%%%%%%% Fit GPs %%%%%%%%%%%%%%
    
    % Plot AvT
    subplot(1,4,1)
    [trainedModelAvT, validationRMSE] = trainRegressionModelAvT(t);
    [zfit, zsd, zint] = trainedModelAvT.predictFcn(t);
    scatter3(t.threshNull, t.threshAlt, t.avT, 20, 'MarkerEdgeColor','k',...
        'MarkerFaceColor','r')
    hold on
    plot3d_errorbars(t.threshNull, t.threshAlt, zfit,...
        zeros(length(t.threshNull)), zeros(length(t.threshAlt)), 2.*zsd, '.r')
    [Y,X] = meshgrid(threshNullArray, threshAltArray);
    Z = reshape(zfit, length(threshNullArray), length(threshAltArray));
    surf(X,Y,Z)
    [CavT, c1] = contour3(X,Y,Z,ContourLevels,'ShowText','off');
    c1.Visible = 'off';
    [xAvT, yAvT, zAvT] = C2xyz(CavT);
    xlabel('\theta_0'); ylabel('\theta_1'); zlabel('E(T)');
    title('E(T)')
    
    % Plot TPR
    subplot(1,4,2)
    [trainedModelTpr, validationRMSE] = trainRegressionModelTpr(t);
    [zfit, zsd, zint] = trainedModelTpr.predictFcn(t);
    scatter3(t.threshNull, t.threshAlt, t.tpr, 20, 'MarkerEdgeColor','k',...
        'MarkerFaceColor','r')
    hold on
    plot3d_errorbars(t.threshNull, t.threshAlt, zfit,...
        zeros(length(t.threshNull)), zeros(length(t.threshAlt)), 2.*zsd, '.r')
    
    [Y,X] = meshgrid(threshNullArray, threshAltArray);
    Z = reshape(zfit, length(threshNullArray), length(threshAltArray));
    surf(X,Y,Z)
    [Ctpr, c2] = contour3(X,Y,Z,'ShowText','off');
    c2.Visible = 'off';
    CoordTpr = C2xyz(Ctpr);
    xlabel('\theta_0'); ylabel('\theta_1'); zlabel('TPR');
    title('TPR')
    
    % Plot FPR
    subplot(1,4,3)
    [trainedModelFpr, validationRMSE] = trainRegressionModelFpr(t);
    [zfit, zsd, zint] = trainedModelFpr.predictFcn(t);
    scatter3(t.threshNull, t.threshAlt, t.fpr, 20, 'MarkerEdgeColor','k',...
        'MarkerFaceColor','r')
    hold on
    plot3d_errorbars(t.threshNull, t.threshAlt, zfit,...
        zeros(length(t.threshNull)), zeros(length(t.threshAlt)), 2.*zsd, '.r')
    [Y,X] = meshgrid(threshNullArray, threshAltArray);
    Z = reshape(zfit, length(threshNullArray), length(threshAltArray));
    surf(X,Y,Z)
    [Cfpr, c3] = contour3(X,Y,Z,'ShowText','off');
    c3.Visible = 'off';
    CoordFpr = C2xyz(Cfpr);
    xlabel('\theta_0'); ylabel('\theta_1'); zlabel('FPR');
    title('FPR')
    
    subplot(1,4,4)
    axis([0, 1, 0, 1])
    hold on
    for i = 1:length(xAvT)
        x = xAvT{i}; y = yAvT{i};
        fitTab = table(x', y');
        fitTab.Properties.VariableNames = {'threshNull', 'threshAlt'};
        tprFit = trainedModelTpr.predictFcn(fitTab);
        fprFit = trainedModelFpr.predictFcn(fitTab);
        plot(fprFit, tprFit)
    end
    titleStr = "Iso-nsteps in ROC space";
    title(titleStr)
    ylabel('True positive rate'); xlabel('False positive rate')
    for k = 1:length(xAvT)
        legStr{k} = sprintf('T = %d', zAvT(k));
    end
    legend(legStr)
    
    %%%%%%%%%%%%%%% Comparison figure %%%%%%%%%%%%%%%
    figure(f2)
    subplot(2,length(discrimArr),iD)
    xValArray = linspace(Hypotheses(s.hNoise).mu - Hypotheses(s.hNoise).var*3, Hypotheses(s.hSignal).mu + Hypotheses(s.hSignal).var*3, nXvals);
    for i = [s.hNoise, s.hSignal]
        pdfArray(i,:) = pdf(Hypotheses(i).name, xValArray, Hypotheses(i).mu, Hypotheses(i).var);
    end
    h1 = plot(xValArray, pdfArray(1,:), 'linewidth', 1);
    hold on
    h2 = plot(xValArray, pdfArray(2,:), 'linewidth', 1);
    title('Probability distributions')
    ylabel('f(x)');
    legend([h1 h2], {'Noise', 'Signal + noise'})
    probDistYLim = ylim;
    
    subplot(2,length(discrimArr), iD+length(discrimArr))
    %SDT
    %subplot(1,2,1)
    axis([0, 1, 0, 1])
    h8 = plot([0 1], [0 1], 'k:');
    set(get(get(h8, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle', 'off');
    hold on
    % for plt = 1:length(s.avTimes)
    %     h9 = plot(s.fprPlotArray(:,plt), s.tprPlotArray(:,plt));
    %     set(h9, 'linestyle','-', 'color', C(plt,:));
    % end
    markerList = {'+', 'o', '*', 'x', 's', 'd', '^'};
    for plt = 1:length(s.avTimes)
        xroc = s.fprPlotArray(:,plt); yroc = s.tprPlotArray(:,plt);
        h9(plt) = scatter(s.fprPlotArray(:,plt), s.tprPlotArray(:,plt));
        set(h9(plt), 'marker', markerList{plt},  'markeredgecolor', C(plt,:));
        
        st=[1 mean(xroc) 1]; L=[0 0 0]; U=[Inf 1 Inf];
        fo_ = fitoptions('method','NonlinearLeastSquares','Lower',L,'Upper',U,'Startpoint',st);
        ft_ = fittype('1-1/((1+(x/C)^B)^E)','dependent',{'y'},'independent',{'x'},'coefficients',{'B', 'C', 'E'});
        rocfit = fit(xroc,yroc,ft_,fo_);
        xfit=linspace(0,1,500);
        yfit=rocfit(xfit);
        %     t2 = table(s.fprPlotArray(:,plt), s.tprPlotArray(:,plt));
        %     t2.Properties.VariableNames = {'fpr', 'tpr'};
        %     [trainedModel, validationRMSE] = trainRegressionModel(t2);
        %     [fit, sd, int] = trainedModel.predictFcn(t2);
        h11 = plot(xfit, yfit);
        set(h11, 'linestyle','--', 'color', C(plt,:));
    end
    %plot(s.fprPlotArray', s.tprPlotArray', 'linestyle', '--', 'Color', [150/256, 150/256, 150/256], 'linewidth', 0.5);
    titleStr = sprintf("%s ROC curve", 'Monte Carlo');
    %title(titleStr)
    xlabel('False positive rate'); ylabel('True positive rate');
    % for kk = 1:length(s.avTimes)
    %     legendCellArr{kk} = sprintf("T = %d", s.avTimes(kk));
    % end
    % legend(legendCellArr)
    % t = annotation('textbox');
    % t.String = 'Mean of T observations per sample. Dashed lines indicate data points with same decision threshold.';
    % t.FontSize = 10;
    % %t.EdgeColor = 'none';
    % t.Position = [0.45 0.25 0.4 0.12];
    % x = [0.6 0.4]; y = [0.7 0.5];
    % ar = annotation('textarrow',x,y);
    % t = annotation('textbox');
    % t.String = 'Low threshold';
    % t.FontSize = 10;
    % t.EdgeColor = 'none';
    % t.Position = [0.55 0.65 0.4 0.1];
    % t = annotation('textbox');
    % t.String = 'High threshold';
    % t.FontSize = 10;
    % t.EdgeColor = 'none';
    % t.Position = [0.30 0.4 0.4 0.1];
    
    %SPRT
    %subplot(1,2,2)
    %axis([0, 1, 0, 1])
    %hold on
    for i = 1:length(xAvT)
        x = xAvT{i}; y = yAvT{i};
        fitTab = table(x', y');
        fitTab.Properties.VariableNames = {'threshNull', 'threshAlt'};
        tprFit = trainedModelTpr.predictFcn(fitTab);
        fprFit = trainedModelFpr.predictFcn(fitTab);
        h10 = plot(fprFit, tprFit);
        set(h10, 'linestyle','-', 'color', C(i,:));
    end
    titleStr = "Iso-nsteps in ROC space";
    title(titleStr)
    % ylabel('True positive rate'); xlabel('False positive rate')
    % for k = 1:length(xAvT)
    %     legStr{k} = sprintf('T = %d', zAvT(k));
    % end
    %legend(legStr)
    legList = arrayfun(@(x) num2str(x), ContourLevels, 'UniformOutput', false);
    legList = ['Averaged SDT', 'SPRT', legList];
    legend([h11 h10 h9], legList, 'location', 'SouthEast')
    % hDummy = plot([0 0; 0 0], [1 1; 2 2]);
    % set(hDummy, 'Visible', 'off', 'linestyle',
    iD = iD + 1;
end