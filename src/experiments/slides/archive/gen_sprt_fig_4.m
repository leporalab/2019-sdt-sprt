% This script runs implementations of the simulations suggested by Nathan's
% annotations on basal ganglia slide stack.

clear
close all

%% CHOOSE
nEpisodes = 500;
nContourLevels = 5;
ContourLevels = [6 10 14 18 22];
isFitToAv = 0;

threshAltArray = linspace(0.01, 3, 5);
threshNullArray = linspace(0.01, 3, 5);

nRows = nEpisodes * length(threshAltArray) * length(threshNullArray);

%% INITIALISE
tmpMu = zeros(2, 1);

maxT = 10000;

mu0Array = [2.5];
iterSubPlot = 1;

hSelectedWhenAltTrue = zeros(nEpisodes, 1);
hSelectedWhenNullTrue = zeros(nEpisodes, 1);
tprArray = zeros(length(threshNullArray), length(threshAltArray));
fprArray = zeros(length(threshNullArray), length(threshAltArray));
avDecTimeArray = zeros(length(threshNullArray), length(threshAltArray));
threshRatioArray = zeros(length(threshNullArray), length(threshAltArray));
decTimeArray = zeros(length(nEpisodes), 2);

tabNullThresh = zeros(nRows, 1);
tabAltThresh = zeros(nRows, 1);
tabDecTime = zeros(nRows, 2);
tabError = zeros(nRows, 2);

iRow = 1;

%% DO

% Hypotheses(1) is the null hypothesis and Hypotheses(2) is the
% alternative hypothesis
Hypotheses(1).mu = mu0Array;
Hypotheses(1).sd = 1;
Hypotheses(1).name = 'normal';
Hypotheses(2).mu = 3;
Hypotheses(2).sd = 1;
Hypotheses(2).name = 'normal';

for m = 1:length(threshNullArray)
    for j = 1:length(threshAltArray)
        fprintf('Performing SPRT over %d episodes for thresholds [%.2f %.2f] ...\n', nEpisodes, threshNullArray(m), threshAltArray(j));
        for k = 1:nEpisodes
            [hSelectedWhenAltTrue(k), ~, decTimeArray(k, 1)] = sprt(Hypotheses, 2, maxT, threshNullArray(m), threshAltArray(j));
            [hSelectedWhenNullTrue(k), ~, decTimeArray(k, 2)] = sprt(Hypotheses, 1, maxT, threshNullArray(m), threshAltArray(j));
            
            %%%%%%%%%%%%%%%%%%%%
            % Capture column vectors for long table of all data points
            %tabTrueHypothesis(2*k:2*k+1) = [2 1]';
            tabNullThresh(iRow) = threshNullArray(m);
            tabAltThresh(iRow) = threshAltArray(j);
            tabDecTime(iRow, :) = [decTimeArray(k, 1), decTimeArray(k, 2)];
            tabError(iRow, :) = [~(hSelectedWhenAltTrue(k) == 2), ~(hSelectedWhenNullTrue(k) == 1)];
            iRow = iRow + 1;
            %%%%%%%%%%%%%%%%%%
        end
        avDecTimeArray(m, j) = mean(decTimeArray(:));
        threshRatioArray(m, j) = threshAltArray(j) / threshNullArray(m);
        nHitsAlt = sum(hSelectedWhenAltTrue == 2);
        tprArray(m, j) = nHitsAlt / nEpisodes;
        nHitsNull = sum(hSelectedWhenNullTrue == 2);
        fprArray(m, j) = nHitsNull / nEpisodes;
        fprintf('TPR = %.2f, FPR = %.2f \n', tprArray(m,j), fprArray(m,j))
    end
end

%% PLOT
C = linspecer(length(threshAltArray)); % set attractive colour order
% Flatten arrays for plotting
avDecTimeArray = avDecTimeArray(:);
threshRatioArray = threshRatioArray(:);
tprArray = tprArray(:);
fprArray = fprArray(:);



if isFitToAv
    % Prepare table for regression analysis
    newThreshNullArray = repmat(threshNullArray, 1, length(threshNullArray));
    newThreshAltArray = repelem(threshAltArray, length(threshAltArray));
    t = table(newThreshNullArray', newThreshAltArray', avDecTimeArray, tprArray, fprArray);
    t.Properties.VariableNames = {'threshNull', 'threshAlt', 'avT', 'tpr', 'fpr'};
    %save('gp_data2','t')
    
    f1 = figure;
    set(f1, 'color','white', 'position', [100 100 1400 350])
    
    %% Fit GPs
    % Plot AvT
    subplot(1,4,1)
    [trainedModelAvT, validationRMSE] = trainRegressionModelAvT(t);
    [zfit, zsd, zint] = trainedModelAvT.predictFcn(t);
    scatter3(t.threshNull, t.threshAlt, t.avT, 20, 'MarkerEdgeColor','k',...
        'MarkerFaceColor','r')
    hold on
    plot3d_errorbars(t.threshNull, t.threshAlt, zfit,...
        zeros(length(t.threshNull)), zeros(length(t.threshAlt)), 2.*zsd, '.r')
    [Y,X] = meshgrid(threshNullArray, threshAltArray);
    Z = reshape(zfit, length(threshNullArray), length(threshAltArray));
    surf(X,Y,Z)
    [CavT, c1] = contour3(X,Y,Z,ContourLevels,'ShowText','off');
    c1.Visible = 'off';
    [xAvT, yAvT, zAvT] = C2xyz(CavT);
    xlabel('\theta_0'); ylabel('\theta_1'); zlabel('E(T)');
    title('E(T)')
    
    % Plot TPR
    subplot(1,4,2)
    [trainedModelTpr, validationRMSE] = trainRegressionModelTpr(t);
    [zfit, zsd, zint] = trainedModelTpr.predictFcn(t);
    scatter3(t.threshNull, t.threshAlt, t.tpr, 20, 'MarkerEdgeColor','k',...
        'MarkerFaceColor','r')
    hold on
    plot3d_errorbars(t.threshNull, t.threshAlt, zfit,...
        zeros(length(t.threshNull)), zeros(length(t.threshAlt)), 2.*zsd, '.r')
    
    [Y,X] = meshgrid(threshNullArray, threshAltArray);
    Z = reshape(zfit, length(threshNullArray), length(threshAltArray));
    surf(X,Y,Z)
    [Ctpr, c2] = contour3(X,Y,Z,'ShowText','off');
    c2.Visible = 'off';
    CoordTpr = C2xyz(Ctpr);
    xlabel('\theta_0'); ylabel('\theta_1'); zlabel('TPR');
    title('TPR')
    
    % Plot FPR
    subplot(1,4,3)
    [trainedModelFpr, validationRMSE] = trainRegressionModelFpr(t);
    [zfit, zsd, zint] = trainedModelFpr.predictFcn(t);
    scatter3(t.threshNull, t.threshAlt, t.fpr, 20, 'MarkerEdgeColor','k',...
        'MarkerFaceColor','r')
    hold on
    plot3d_errorbars(t.threshNull, t.threshAlt, zfit,...
        zeros(length(t.threshNull)), zeros(length(t.threshAlt)), 2.*zsd, '.r')
    [Y,X] = meshgrid(threshNullArray, threshAltArray);
    Z = reshape(zfit, length(threshNullArray), length(threshAltArray));
    surf(X,Y,Z)
    [Cfpr, c3] = contour3(X,Y,Z,'ShowText','off');
    c3.Visible = 'off';
    CoordFpr = C2xyz(Cfpr);
    xlabel('\theta_0'); ylabel('\theta_1'); zlabel('FPR');
    title('FPR')
    
    %%
    subplot(1,4,4)
    axis([0, 1, 0, 1])
    hold on
    for i = 1:length(xAvT)
        x = xAvT{i}; y = yAvT{i};
        fitTab = table(x', y');
        fitTab.Properties.VariableNames = {'threshNull', 'threshAlt'};
        tprFit = trainedModelTpr.predictFcn(fitTab);
        fprFit = trainedModelFpr.predictFcn(fitTab);
        plot(fprFit, tprFit)
    end
    titleStr = "SPRT: Iso-nsteps in ROC space";
    title(titleStr)
    
    ylabel('True positive rate'); xlabel('False positive rate')
    for k = 1:length(xAvT)
        legStr{k} = sprintf('T = %d', zAvT(k));
    end
    legend(legStr)
    
else
    %% Fit GPs
    % Plot AvT
    
    f1 = figure;
    set(f1, 'color','white', 'position', [100 100 1200 850])
    
    
    tabTrueHypothesis = ones(nRows, 1);
    t1 = table(tabTrueHypothesis, tabNullThresh, tabAltThresh, tabDecTime(:,1), tabError(:,1));
    t1.Properties.VariableNames = {'truHyp', 'threshNull', 'threshAlt', 'decTime', 'error'};
    tabTrueHypothesis = 2 * ones(nRows, 1);
    t2 = table(tabTrueHypothesis, tabNullThresh, tabAltThresh, tabDecTime(:,2), tabError(:,2));
    t2.Properties.VariableNames = {'truHyp', 'threshNull', 'threshAlt', 'decTime', 'error'};
    tBoth = [t1; t2];
    % Prediction table
    newThreshNullArray = repmat(threshNullArray, 1, length(threshNullArray));
    newThreshAltArray = repelem(threshAltArray, length(threshAltArray));
    dArr = zeros(length(newThreshNullArray), 1);
    tPredict = table(newThreshNullArray', newThreshAltArray');
    tPredict.Properties.VariableNames = {'threshNull', 'threshAlt'};
    
    subplot(1,4,1)
    [trainedModelDecTime, validationRMSE] = trainRegressionModelDecTime(tBoth);
    zfit = trainedModelDecTime.predictFcn(tPredict);
    scatter3(tBoth.threshNull, tBoth.threshAlt, tBoth.decTime, 10, 'MarkerEdgeColor','k',...
        'MarkerFaceColor','r')
    hold on
    %plot3d_errorbars(t.threshNull, t.threshAlt, zfit,...
    %    zeros(length(t.threshNull)), zeros(length(t.threshAlt)), 2.*zsd, '.r')
    [X,Y] = meshgrid(threshNullArray, threshAltArray);
    Z = reshape(zfit, length(threshNullArray), length(threshAltArray));
    surf(X,Y,Z)
    [CDecTime, c1] = contour3(X,Y,Z,ContourLevels,'ShowText','off');
    c1.Visible = 'off';
    [xDecTime, yDecTime, zDecTime] = C2xyz(CDecTime);
    xlabel('\theta_0'); ylabel('\theta_1'); zlabel('E(T)');
    title('E(T)')
    
    subplot(1,4,2)
    [trainedModelError1, validationRMSE] = trainRegressionModelError(t1);
    zfit = trainedModelError1.predictFcn(tPredict);
    scatter3(t1.threshNull, t1.threshAlt, t1.error, 10, 'MarkerEdgeColor','k',...
        'MarkerFaceColor','r')
    hold on
    %     plot3d_errorbars(t.threshNull, t.threshAlt, zfit,...
    %         zeros(length(t.threshNull)), zeros(length(t.threshAlt)), 2.*zsd, '.r')
    [X,Y] = meshgrid(threshNullArray, threshAltArray);
    Z = reshape(zfit, length(threshNullArray), length(threshAltArray));
    surf(X,Y,Z)
    [Cfpr, c3] = contour3(X,Y,Z,'ShowText','off');
    c3.Visible = 'off';
    CoordFpr = C2xyz(Cfpr);
    xlabel('\theta_0'); ylabel('\theta_1'); zlabel('E(e)');
    title('E(e): FPR')
    
    subplot(1,4,3)
    [trainedModelError2, validationRMSE] = trainRegressionModelError(t2);
    zfit = trainedModelError2.predictFcn(tPredict);
    newZ = 1 - zfit;
    scatter3(t2.threshNull, t2.threshAlt, (1-t2.error), 10, 'MarkerEdgeColor','k',...
        'MarkerFaceColor','r')
    hold on
    %     plot3d_errorbars(t.threshNull, t.threshAlt, zfit,...
    %         zeros(length(t.threshNull)), zeros(length(t.threshAlt)), 2.*zsd, '.r')
    [X,Y] = meshgrid(threshNullArray, threshAltArray);
    Z = reshape(newZ, length(threshNullArray), length(threshAltArray));
    surf(X,Y,Z)
    [Ctpr, c3] = contour3(X,Y,Z,'ShowText','off');
    c3.Visible = 'off';
    CoordFpr = C2xyz(Ctpr);
    xlabel('\theta_0'); ylabel('\theta_1'); zlabel('1 - E(e)');
    title('1 - E(e): TPR')
    
    %%
    subplot(1,4,4)
    axis([0, 1, 0, 1])
    hold on
    for i = 1:length(xDecTime)
        x1 = xDecTime{i}; y1 = yDecTime{i};
        fitTab = table(x1', y1');
        fitTab.Properties.VariableNames = {'threshNull', 'threshAlt'};
        fprFit = trainedModelError1.predictFcn(fitTab);
        tprFit = 1 - trainedModelError2.predictFcn(fitTab);
        plot(fprFit, tprFit)
    end
    titleStr = "SPRT: Iso-nsteps in ROC space";
    title(titleStr)
    ylabel('True positive rate'); xlabel('False positive rate')
    for k = 1:length(xDecTime)
        legStr{k} = sprintf('T = %d', zDecTime(k));
    end
    legend(legStr)
    
end
