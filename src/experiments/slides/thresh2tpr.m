function [tpr,fpr,avDecTime] = thresh2tpr(nullLogThresh, altLogThresh, Hypotheses)
%UNTITLED2 Uses Wald's approximations to arrive at tpr and fpr and
%avDecTime for equal priors
%   Detailed explanation goes here
A = exp(nullLogThresh);
B = exp(-altLogThresh);
alpha = (1 - B) / (A - B); %Approx Eq. 11 from your note
beta = (A^-1 - 1) / (A^-1 - B^-1); %Approx Eq. 13 from your note
tpr = 1 - beta;
fpr = alpha;
asnUnder0 = ((1 - alpha)*log(B) + alpha*log(A)) / expEvidenceNorm(Hypotheses(1).mu, Hypotheses(2).mu, Hypotheses(1).sd, Hypotheses(1).mu);
asnUnder1 = (beta*log(B) + (1 - beta)*log(A)) / expEvidenceNorm(Hypotheses(1).mu, Hypotheses(2).mu, Hypotheses(1).sd, Hypotheses(2).mu);
avDecTime = (asnUnder1 + asnUnder0)/2; % take mean
end

