clear
close all

%% CHOOSE

%Distribution options
discrim = 0.5;
sd = 1;
mu1 = 2;

calcMu2 = discrim * sd + mu1;
Hypotheses(1).mu = mu1;
Hypotheses(1).sd = sd;
Hypotheses(1).name = 'normal';
Hypotheses(2).mu = calcMu2;
Hypotheses(2).sd = sd;
Hypotheses(2).name = 'normal';

%SPRT options
nEpisodes = 500;
ContourLevels = [1, 2:3:50];
threshAltArray = linspace(0, 3, 5);
threshNullArray = linspace(0, 3, 5);

%SDT options
nSamples = 500;
nThreshold = 50;
T = ContourLevels;

%% DO

%SDT
%s = sdt(Hypotheses, nThreshold, nSamples, 'SequentialAnalysis', 1, 'avTimes', T);

%SPRT
roc = sprt4roc_thresh_isocurves(Hypotheses, threshNullArray, threshAltArray, T, 'nEpisodes', 500, 'FitToAv', 1);

%% PLOT

% C = linspecer(length(ContourLevels));
% markerList = {'+', 'o', '*', 'x', 's', 'd', '^'};
%   
% f2 = figure;
% set(f2, 'color','white', 'position', [150 150 600 500])
% %SDT
% axis([0, 1, 0, 1])
% h8 = plot([0 1], [0 1], 'k:');
% set(get(get(h8, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle', 'off');
% hold on
% for i = 1:length(ContourLevels)
%     h1(i) = plot(s.fprArrayExact(:,i), s.tprArrayExact(:,i));
%     set(h1(i), 'linestyle', '-', 'color', C(i,:));
%     h2 = plot(roc.fpr{i}, roc.tpr{i});
%     set(h2, 'marker', markerList{i}, 'linestyle', 'none', 'color', C(i,:));
% end
% legList = arrayfun(@(x) num2str(x), ContourLevels, 'UniformOutput', false);
% legList = ['Averaged SDT', 'SPRT', legList];
% d(1) = plot([100 100], [100 100], 'k-');
% d(2) = plot([100 100], [100 100], 'k+');
% ylim([0 1]); xlim([0 1]);
% legend([d(1) d(2)], legList, 'location', 'SouthEast')
% xlabel('False positive rate'); ylabel('True positive rate');
% strTitle = sprintf('d'' = %.1f', discrim);
% title(strTitle)
% t = annotation('textbox');
% t.String = sprintf('T = [%s]', num2str(ContourLevels));
% t.Position = [.6 .35 .2 .2];
% t.FitBoxToText = 'on';



