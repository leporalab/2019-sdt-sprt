clear
close all


p1 = [5, 1];
%p2 = linspace(5.1, 8, 50);

p2 = 7;

alpha = 0.01:0.05:0.99;
beta = 0.01:0.05:0.99;

ncalc = zeros(length(alpha), length(beta));

pwr = 1 - beta;

nout = sampsizepwr('z', p1, p2, pwr, [], 'Alpha', alpha, 'tail', 'right');
%
%% My calculations
for i = 1:length(alpha)
    for j = 1:length(beta)
        % Eq. (3.67) in Wald (1947)
        lambda = icdf('norm', [1-alpha(i), beta(j)], 0, 1);
        ncalc(i,j) = ((lambda(1) - lambda(2))^2) ./ (p1(1) - p2).^2;
    end
end

[fpr, tpr] = meshgrid(alpha, 1-beta);
figure
surface(fpr, tpr, ncalc)
view(3)
xlabel('FPR'); ylabel('TPR'); zlabel('sample size, n');
colorbar
title('Required sample size in ROC space for SDT')

%%
figure
plot(p2 - p1(1).*ones(1, length(p2)), nout, 'b')
hold on
plot(p2 - p1(1).*ones(1, length(p2)), ncalc, 'r')
xlabel('(\mu_1 - \mu_0)/\sigma');
ylabel('n');