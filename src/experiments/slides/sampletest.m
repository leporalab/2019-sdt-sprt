clear
close all


p1 = [5, 1];
p2 = linspace(5.1, 8, 50);

alpha = 0.01;
beta = 0.01;

pwr = 1 - beta;


nout = sampsizepwr('z', p1, p2, pwr, [], 'Alpha', alpha, 'tail', 'right');



%% My calculations
lambda = icdf('norm', [1-alpha, beta], 0, 1);

ncalc = ((lambda(1) - lambda(2))^2).*ones(1, length(p2)) ./ (p1(1).*ones(1, length(p2)) - p2).^2;

%%
plot(p2 - p1(1).*ones(1, length(p2)), nout, 'b')
hold on
plot(p2 - p1(1).*ones(1, length(p2)), ncalc, 'r')
xlabel('(\mu_1 - \mu_0)/\sigma');
ylabel('n');