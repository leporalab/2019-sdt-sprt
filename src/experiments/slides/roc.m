clear
close all

sd = 1;
mu0 = -0.1;
mu1 = 0.1;
Hypotheses(1).mu = mu0;
Hypotheses(1).sd = sd;
Hypotheses(1).name = 'normal';
Hypotheses(2).mu = mu1;
Hypotheses(2).sd = sd;
Hypotheses(2).name = 'normal';

alpha = 0.3;
beta = 0.3;

y = expEvidenceNorm(mu0, mu1, sd, mu0)^-1 * ((1-alpha)*log(beta/(1-alpha)) + alpha*log((1-beta)/alpha))