clear
close all

T = [2 3 4 5 6];
threshAltArray = linspace(0, 3, 10);
threshNullArray = linspace(0, 3, 10);

discrim = 0.5;
sd = 1;
mu1 = 2;
calcMu2 = discrim * sd + mu1;

Hypotheses(1).mu = mu1;
Hypotheses(1).sd = sd;
Hypotheses(1).name = 'normal';
Hypotheses(2).mu = calcMu2;
Hypotheses(2).sd = sd;
Hypotheses(2).name = 'normal';

roc = sprt4roc(Hypotheses, threshNullArray, threshAltArray, T, 'nEpisodes', 500, 'FitToAv', 1);