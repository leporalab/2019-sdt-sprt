 % Generate comparison figure of fixed sample size SDT vs. summed logLR with threshold
%
% Thom Griffith, July 2018

clear
close all
%% CHOOSE

%Distribution options.
discrim = 0.5;
sd = 1;
mu1 = 2;
calcMu2 = discrim * sd + mu1;

Hypotheses(1).mu = mu1;
Hypotheses(1).sd = sd;
Hypotheses(1).name = 'normal';
Hypotheses(2).mu = calcMu2;
Hypotheses(2).sd = sd;
Hypotheses(2).name = 'normal';

%SDT options
nSamples = 5000;
nThreshold = 50;
T = [1 2 3];
nLevels = length(T);

%% DO

% SDT
s = sdt(Hypotheses, nThreshold, nSamples, 'SequentialAnalysis', 1, 'avTimes', T);

%c = cumloglr(Hypotheses, nThreshold, nSamples, 'SequentialAnalysis', 1, 'avTimes', T);

% cumloglr
tpr = zeros(nLevels, 1);
fpr = zeros(nLevels, 1);
tprVar = zeros(nLevels, 1);
fprVar = zeros(nLevels, 1);

i = 1;
for t = T
    % Sample from probability distributions
    noiseSampleArr = icdf(Hypotheses(1).name, rand(nSamples, t), Hypotheses(1).mu, Hypotheses(1).sd);
    signalSampleArr = icdf(Hypotheses(2).name, rand(nSamples, t), Hypotheses(2).mu, Hypotheses(2).sd);
    
    % when H0 is true
    fOne = normpdf(noiseSampleArr, Hypotheses(2).mu, Hypotheses(2).sd);
    fZero = normpdf(noiseSampleArr, Hypotheses(1).mu, Hypotheses(1).sd);
    probRatio = fOne ./ fZero;
    logLR = log(probRatio);
    tmp = cumsum(logLR,2);
    cumlogLRnoiseTrue = tmp(:,end);
    
    % when H1 is true
    fOne = normpdf(signalSampleArr, Hypotheses(2).mu, Hypotheses(2).sd);
    fZero = normpdf(signalSampleArr, Hypotheses(1).mu, Hypotheses(1).sd);
    probRatio = fOne ./ fZero;
    logLR = log(probRatio);
    tmp = cumsum(logLR,2);
    cumlogLRsigTrue = tmp(:,end);
    
    % consider checking cumlogLR for zeros and replacing with pos/neg Bernoulli
    % variable
    
    tpr(i) = sum(cumlogLRsigTrue > 0) / nSamples;
    fnr = 1 - tpr(i);
    tprVar(i) = tpr(i) .* fnr;
    fpr(i) = sum(cumlogLRnoiseTrue > 0) / nSamples;
    tnr = 1 - fpr(i);
    fprVar(i) = fpr(i) .* tnr;
    
    i = i + 1;
end


%% PLOT

C = linspecer(length(T));
markerList = {'+', 'o', '*', 'x', 's', 'd', '^'};

f2 = figure;
set(f2, 'color','white', 'position', [150 150 600 500])
%SDT
axis([0, 1, 0, 1])
h8 = plot([0 1], [0 1], 'k:');
set(get(get(h8, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle', 'off');
hold on
for i = 1:length(T)
    h1(i) = plot(s.fprArrayExact(:,i), s.tprArrayExact(:,i));
    set(h1(i), 'linestyle', '-', 'color', C(i,:));
    h2 = plot(fpr(i), tpr(i));
    set(h2, 'marker', markerList{i}, 'linestyle', 'none', 'color', C(i,:));
    
    e1 = errorbar(fpr(i), tpr(i), tprVar(i), tprVar(i),...
        zeros(1, length(fpr(i))), zeros(1, length(fpr(i))), '.', 'capsize', 0);
    set(e1, 'color', C(i,:), 'linewidth', 1);
    e2 = errorbar(fpr(i), tpr(i), zeros(1, length(fpr(i))), ...
        zeros(1, length(fpr(i))), fprVar(i), fprVar(i), '.', 'capsize', 0);
    set(e2, 'color', C(i,:), 'linewidth', 1);
    
end
legList = arrayfun(@(x) num2str(x), T, 'UniformOutput', false);
legList = ['Averaged SDT', 'Cumulative logLR', legList];
d(1) = plot([100 100], [100 100], 'k-');
d(2) = plot([100 100], [100 100], 'k+');
ylim([0 1]); xlim([0 1]);
legend([d(1) d(2)], legList, 'location', 'SouthEast')
xlabel('False positive rate'); ylabel('True positive rate');
strTitle = sprintf('d'' = %.1f', discrim);
title(strTitle)
t = annotation('textbox');
t.String = sprintf('T = [%s]', num2str(T));
t.Position = [.6 .35 .2 .2];
t.FitBoxToText = 'on';
