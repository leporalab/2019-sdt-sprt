close all
clear

sd = 1;
mu0 = -1/3;
mu1 = 1/3;
Hypotheses(1).mu = mu0;
Hypotheses(1).sd = sd;
Hypotheses(1).name = 'normal';
Hypotheses(2).mu = mu1;
Hypotheses(2).sd = sd;
Hypotheses(2).name = 'normal';

expEv1 = expEvidenceNorm(mu0, mu1, sd, mu1);
expEv0 = expEvidenceNorm(mu0, mu1, sd, mu0);

x = 0:0.01:1;

y1 = x.*exp(expEv1);
y2 = 1 - (1 - x).*exp(expEv0);
y0 = x;

%%
f = figure
plot(x, y0, 'k:', 'linewidth', 1.5)
hold on
ylim([0 1]); xlim([0 1]);
plot(x, y1, 'r')
plot(x, y2, 'b')
a1 = area(x, y1); a1.FaceAlpha = 0.2;
a2 = area(x, y2); a2.FaceAlpha = 0.2;
ylabel('1 - \beta'); xlabel('\alpha')
title('Region where expected evidence is larger than threshold')

f.Units               = 'centimeters';
%f1.Position(3)         = 18.5; % two columns
f.Position(3)         = 11; % one column
f.Position(4)         = 8;
% set text properties
set(f.Children, ...
    'FontName',     'Arial', ...
    'FontSize',     8);

set(gcf, 'Renderer', 'painters')
pos = get(f,'Position');
set(f,'PaperPositionMode','Auto','PaperUnits','centimeters','PaperSize',[pos(3), pos(4)])
