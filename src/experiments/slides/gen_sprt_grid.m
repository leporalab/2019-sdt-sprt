clear
close all

T = 1:50;
threshAltArray = linspace(0, 3, 5);
threshNullArray = linspace(0, 3, 5);

nXvals = 50;

discrim = [1.2 0.8 0.4];
sd = 1;
mu1 = 2;

f1 = figure;
set(f1, 'color','white', 'position', [100 100 1200 550])
f2 = figure;
set(f2, 'color','white', 'position', [100 100 1200 550])
f3 = figure;
set(f3, 'color','white', 'position', [100 100 1200 550])

for iDis = 1:length(discrim)
    calcMu2 = discrim(iDis) * sd + mu1;
    
    Hypotheses(1).mu = mu1;
    Hypotheses(1).sd = sd;
    Hypotheses(1).name = 'normal';
    Hypotheses(2).mu = calcMu2;
    Hypotheses(2).sd = sd;
    Hypotheses(2).name = 'normal';
    
    [roc, trainedModel] = sprt4roc(Hypotheses, threshNullArray, threshAltArray, T, 'nEpisodes', 500, 'FitToAv', 1);
    
    %% Plot
    figure(f1)
    subplot(2,3,iDis)
    xValArray = linspace(Hypotheses(1).mu - Hypotheses(1).sd*3, Hypotheses(2).mu + Hypotheses(2).sd*3, nXvals);
    for i = [1, 2]
        pdfArray(i,:) = pdf(Hypotheses(i).name, xValArray, Hypotheses(i).mu, Hypotheses(i).sd);
    end
    h1 = plot(xValArray, pdfArray(1,:), 'linewidth', 1);
    hold on
    h2 = plot(xValArray, pdfArray(2,:), 'linewidth', 1);
    title('Probability distributions')
    ylabel('f(x)'); xlabel('x');
    legend([h1 h2], {'Noise', 'Signal + noise'})
    probDistYLim = ylim;
    
    % fpr = linspace(0.01, 0.4, 20);
    % tpr = linspace(0.6, 0.99, 20);
    alpha = linspace(0.01, 0.40, 15);
    beta = linspace(0.01, 0.40, 15);
    
    ncalc = zeros(length(alpha), length(beta));
    nout = zeros(length(alpha), length(beta));
    avN = zeros(length(alpha), length(beta));
    
    p1 = [Hypotheses(1).mu, Hypotheses(1).sd];
    p2 = Hypotheses(2).mu;
    
    for i = 1:length(alpha)
        for j = 1:length(beta)
            pwr = 1 - beta;
            %nout(i,j) = sampsizepwr('z', p1, p2, pwr, [], 'Alpha', alpha, 'tail', 'right');
            % Eq. (3.67) in Wald (1947)
            lambda = icdf('norm', [1-alpha(i), beta(j)], 0, 1);
            ncalc(i,j) = ((lambda(1) - lambda(2))^2) ./ (Hypotheses(1).mu - Hypotheses(2).mu).^2;
            
            t = table(alpha(i), 1-beta(j));
            t.Properties.VariableNames = {'fpr', 'tpr'};
            avN(i,j) = trainedModel.predictFcn(t);
        end
    end
    
    [fpr, tpr] = meshgrid(alpha, 1-beta);
    
    % subplot(1, 3, 1)
    % surface(fpr, tpr, ncalc)
    % view(2)
    % xlabel('FPR'); ylabel('TPR'); zlabel('sample size, n');
    % colorbar
    % title('Required sample size in ROC space for SDT')
    %
    % subplot(1, 3, 2)
    % surface(fpr, tpr, avN)
    % view(2)
    % xlabel('FPR'); ylabel('TPR'); zlabel('E(n)');
    % colorbar
    % title('E(n) in ROC space for SPRT')
    
    figure(f1)
    subplot(2,3,iDis + length(discrim))
    surface(fpr, tpr, 100*(ncalc - avN)./ncalc);
    view(2)
    zl(iDis,:) = zlim;
    xlabel('FPR'); ylabel('TPR'); zlabel('E[% saving in n] in ROC space for SPRT vs. SDT');
    c1(iDis) = colorbar;
    titString = sprintf('d'' = %.2f', discrim(iDis));
    title(titString)
    
    figure(f2)
    subplot(2,3,iDis)
    surface(fpr, tpr, ncalc);
    view(3)
    xlabel('FPR'); ylabel('TPR'); zlabel('Calculated sample size for SDT');
    %c1(iDis) = colorbar;
    titString = sprintf('d'' = %.2f', discrim(iDis));
    title(titString)
    
 
    subplot(2,3,iDis + length(discrim))
    surface(fpr, tpr, avN);
    view(3)
    xlabel('FPR'); ylabel('TPR'); zlabel('Predicted E(n) for SPRT');
    %c1(iDis) = colorbar;
    titString = sprintf('d'' = %.2f', discrim(iDis));
    title(titString)
    
end
figure(f1)
for i = 1:length(discrim)
    subplot(2,3,i + length(discrim))
    zlim([min(zl(:)), max(zl(:))]);
    caxis([min(zl(:)), max(zl(:))]);
    colorBarLimitAll(i,:) = c1(i).Limits;
end
colorBarLimits = [min(colorBarLimitAll(:)), max(colorBarLimitAll(:))];
for i = 1:length(discrim)
    
    rLim1 = floor(colorBarLimits(1)) - rem(10, floor(colorBarLimits(1)));
    if isnan(rLim1)
        rLim1 = 0;
    end
    rLim2 = ceil(colorBarLimits(2)) + rem(10, ceil(colorBarLimits(2)));
    c1(i).Limits = [rLim1, rLim2];
    %set(c1(i), 'Ticks', linspace(rLim1, rLim2, 5));
end
