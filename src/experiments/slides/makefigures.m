close all
clear
%% Equiwidth Gaussians
% CHOOSE
nSamples = 500;
nThreshold = 25;
nXvals = 100;
isSamplePerThreshold = 1;

discrim = 3;
sd = 1;
mu1 = 2;

% INITIALISE
tmpMu = zeros(2, 1);
calcMu2 = discrim * sd + mu1;

Hypotheses(1).mu = mu1;
Hypotheses(1).sd = sd;
Hypotheses(1).name = 'normal';
Hypotheses(2).mu = calcMu2;
Hypotheses(2).sd = sd;
Hypotheses(2).name = 'normal';

mp = (calcMu2 - mu1)/2 + mu1;


% DO
s = sdt(Hypotheses, nThreshold, nSamples, 'SamplePerThreshold', 'true');

% PLOT
f1 = figure;
set(f1, 'color','white', 'position', [100 100 600 400])
C = get(gca,'colororder'); % set attractive colour order

xValArray = linspace(Hypotheses(s.hNoise).mu - Hypotheses(s.hNoise).sd*3, Hypotheses(s.hSignal).mu + Hypotheses(s.hSignal).sd*3, nXvals);

X1 = linspace(mp, Hypotheses(s.hSignal).mu + Hypotheses(s.hSignal).sd*3, nXvals);
for i = [s.hNoise, s.hSignal]
    Y1(i,:) = pdf(Hypotheses(i).name, X1, Hypotheses(i).mu, Hypotheses(i).sd);
end

for i = [s.hNoise, s.hSignal]
    pdfArray(i,:) = pdf(Hypotheses(i).name, xValArray, Hypotheses(i).mu, Hypotheses(i).sd);
end
h0 = vline(mu1, 'k--', '\mu_0');
hold on
h11 = vline(calcMu2, 'k--', '\mu_1');
H1 = area(X1, Y1(2,:));
H1.FaceAlpha = 0.3;
H = area(X1, Y1(1,:));
H.FaceAlpha = 0.3;

h1 = plot(xValArray, pdfArray(1,:), 'linewidth', 1.5, 'color', C(2,:));
hold on
h2 = plot(xValArray, pdfArray(2,:), 'linewidth', 1.5, 'linestyle', '-', 'color', C(1,:));
%title('Probability distributions')
ylabel('f(x)'); xlabel('x')
legend([h1 h2], {'Noise', 'Signal + noise'})
probDistYLim = ylim;
ylim([0 0.5])
box off
h4 = vline((calcMu2 - mu1)/2 +mu1, 'k:', '\theta');



