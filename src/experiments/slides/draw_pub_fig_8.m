clear
close all

T = [2 3 4 5 6];
threshArray = linspace(0, 4, 10);

discrim = 0.5;
sd = 1;
mu1 = 2;
calcMu2 = discrim * sd + mu1;

Hypotheses(1).mu = mu1;
Hypotheses(1).sd = sd;
Hypotheses(1).name = 'normal';
Hypotheses(2).mu = calcMu2;
Hypotheses(2).sd = sd;
Hypotheses(2).name = 'normal';

[tprArray, fprArray, tabDecTime, t] = sprt4roc_equal_thresh(Hypotheses, threshArray, threshArray, T, 'nEpisodes', 500, 'FitToAv', 1);

%% PLOT
f1 = figure;
set(f1, 'color','white', 'position', [100 100 550 350])
C = get(gca,'colororder'); % set attractive colour order

% Fit logistic regression model to error and n data
B = glmfit(t.n, [t.error ones(length(t.error), 1)], 'binomial', 'link', 'logit');
X = linspace(0, max(t.n), 100);
Z = Logistic(B(1) + X*B(2));

jitterAmount = 0.01;
%jitterValuesX = 2*(rand(size(dataX))-0.5)*jitterAmount;   % +/-jitterAmount max
jitter = 2*(randn(size(t.error))-0.5)*jitterAmount;   % +/-jitterAmount max
%scatter(dataX+jitterValuesX, dataY+jitterValuesY);

%subplot(1,2,1)
scatter(t.n, t.error + jitter, 10, t.thresh, 'filled')
c = colorbar; ylabel(c, 'threshold, \theta'); c.FontSize = 8;
xlabel('n'); ylabel('error, e');
ylim([-0.1 1.1])
hold on
plot(X, Z, 'linewidth', 1.2)
box on; grid on
title('SPRT speed-accuracy curve (equal thresholds)')

%% Format figure
f1.Units               = 'centimeters';
f1.Position(3)         = 9;
f1.Position(4)         = 6.5;
% set text properties
set(f1.Children, ...
    'FontName',     'Arial', ...
    'FontSize',     8);

% l1.FontSize = 7;
% l2.FontSize = 7;

set(gcf, 'Renderer', 'painters')
pos = get(f1,'Position');
set(f1,'PaperPositionMode','Auto','PaperUnits','Centimeters','PaperSize',[pos(3), pos(4)])

% s3Pos = get(s3, 'Position');
% oldPos = get(s1, 'Position');
% s1.Position = [oldPos(1) 0.173 oldPos(3) 0.6];
% s2.Position = [oldPos(1) 0.81 oldPos(3) 0.1];


% subplot(1,3,1)
% b0 = boundedline(threshArray, fprArray, fprArray.*(1-fprArray), 'alpha', 'cmap', C(1,:));
% hold on
% b1 = boundedline(threshArray, tprArray, tprArray.*(1-tprArray), 'alpha', 'cmap', C(2,:));
% %title('SPRT true and false positive rates')
% xlabel('Decision threshold'); ylabel('Rate')
% t = annotation('textbox');
% t.String = 'Shaded regions show the variance';
% t.FontSize = 8;
% t.EdgeColor = 'none';
% t.Position = [0.25 0.50 0.07 0.15];
% legend([b0 b1], {'FPR', 'TPR'}, 'location', 'southwest')
% 
% subplot(1,3,2)
% for m = 1:length(tabDecTime)
%     avDecTime(m) = mean(mean(tabDecTime{m}));
%     varDecTime(m) = sqrt(var(tabDecTime{m}(:)));
% end
% h1 = plot(threshArray, avDecTime);
% %b2 = boundedline(threshArray, avDecTime, varDecTime, 'alpha');
% %title('Number of steps taken')
% xlabel('Decision threshold')
% ylabel('Number of steps')
% hold on
% 
% sel = [11 21 31 41];
% for i = 1:length(sel)
%     scatter(threshArray(sel(i))*ones(1000,1), tabDecTime{sel(i)}(:), '.')
% end
% yl = ylim;
% 
% subplot(1,3,3)
% for i = [4 3 2 1]
%     histogram(tabDecTime{sel(i)}, 'facecolor', C(i+1,:), 'linestyle', 'none', 'orientation', 'horizontal')
%     hold on
% end
% ylim(yl)
% xlabel('Count')




%%
% xValArray = linspace(Hypotheses(s.hNoise).mu - Hypotheses(s.hNoise).sd*3, Hypotheses(s.hSignal).mu + Hypotheses(s.hSignal).sd*3, nXvals);
%
% subplot(2,3,1)
% for i = [s.hNoise, s.hSignal]
%     pdfArray(i,:) = pdf(Hypotheses(i).name, xValArray, Hypotheses(i).mu, Hypotheses(i).sd);
% end
% h1 = plot(xValArray, pdfArray(1,:), 'linewidth', 1);
% hold on
% h2 = plot(xValArray, pdfArray(2,:), 'linewidth', 1);
% title('Probability distributions')
% ylabel('f(x)');
% legend([h1 h2], {'Noise', 'Signal + noise'})
% probDistYLim = ylim;
%
% subplot(2,3,4)
% edges = xValArray;
% if ~isSamplePerThreshold
%     histogram(sNullArray, edges)
%     hold on
%     histogram(sTrueArray, edges)
%     titleStr = sprintf("%d samples per distribution", nSamples);
% else
%     histogram(reshape(s.NoiseArrayPerThresholdCount, [], 1), edges)
%     hold on
%     histogram(reshape(s.SignalArrayPerThresholdCount, [], 1), edges);
%     titleStr = sprintf("%d samples per threshold", nSamples);
% end
% xlabel('x'); ylabel('Count');
% legend('Noise', 'Signal + noise')
% title(titleStr)
% histYLim = ylim;
%
% subplot(2,3,6)
% axis([0, 1, 0, 1])
% hold on
% h3 = plot([0 1], [0 1], 'k:');
% titleStr = sprintf("%s ROC curve", 'Monte Carlo');
% title(titleStr)
% xlabel('False positive rate'); ylabel('True positive rate');
% e1 = errorbar(s.fprArray, s.tprArray, s.tprVarArray, s.tprVarArray,...
%     zeros(1, length(s.fprArray)), zeros(1, length(s.fprArray)), '.', 'capsize', 0);
% set(e1, 'color', C(2,:), 'linewidth', 1);
% e2 = errorbar(s.fprArray, s.tprArray, zeros(1, length(s.fprArray)), ...
%     zeros(1, length(s.fprArray)), s.fprVarArray, s.fprVarArray, '.', 'capsize', 0);
% set(e2, 'color', C(1,:), 'linewidth', 1);
% plot(s.fprArray, s.tprArray, '--ok', 'linewidth', 1);
% ax1 = gca; ax1.YColor = C(2,:); ax1.XColor = C(1,:);
%
% subplot(2,3,3)
% axis([0, 1, 0, 1])
% hold on
% h5 = plot([0 1], [0 1], 'k:');
% titleStr = sprintf("%s ROC curve", 'Exact');
% title(titleStr)
% ylabel('True positive rate');
% box off
% e1 = errorbar(s.fprArrayExact, s.tprArrayExact, s.tprVarArrayExact, s.tprVarArrayExact,...
%     zeros(1, length(s.fprArrayExact)), zeros(1, length(s.fprArrayExact)), '.', 'capsize', 0);
% set(e1, 'color', C(2,:), 'linewidth', 1);
% e2 = errorbar(s.fprArrayExact, s.tprArrayExact, zeros(1, length(s.fprArrayExact)), ...
%     zeros(1, length(s.fprArrayExact)), s.fprVarArrayExact, s.fprVarArrayExact, '.', 'capsize', 0);
% set(e2, 'color', C(1,:), 'linewidth', 1);
% plot(s.fprArrayExact, s.tprArrayExact, '--ok', 'linewidth', 1);
% t1 = annotation('textbox');
% t1.String = 'Error bars show the variance';
% t1.FontSize = 8;
% t1.EdgeColor = 'none';
% t1.Position = [0.82 0.58 0.07 0.15];
% ax1 = gca; ax1.YColor = C(2,:); ax1.XColor = C(1,:);
%
% subplot(2,3,2)
% b0 = boundedline(s.threshArray, s.fprArrayExact, s.fprVarArrayExact, 'alpha', 'cmap', C(1,:));
% hold on
% b1 = boundedline(s.threshArray, s.tprArrayExact, s.tprVarArrayExact , 'alpha', 'cmap', C(2,:));
% title('Exact true and false positive rates')
% ylabel('Exact rate')
% legend([b0 b1], {'FPR', 'TPR'}, 'location', 'southwest')
%
% subplot(2,3,5)
% b0 = boundedline(s.threshArray, s.fprArray, s.fprVarArray, 'alpha', 'cmap', C(1,:));
% hold on
% b1 = boundedline(s.threshArray, s.tprArray, s.tprVarArray , 'alpha', 'cmap', C(2,:));
% title('MC true and false positive rates')
% xlabel('Decision threshold'); ylabel('Rate')
% t = annotation('textbox');
% t.String = 'Boundary lines show the variance';
% t.FontSize = 8;
% t.EdgeColor = 'none';
% t.Position = [0.55 0.75 0.07 0.15];
% legend([b0 b1], {'FPR', 'TPR'}, 'location', 'southwest')