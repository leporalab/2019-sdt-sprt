% This script runs implementations of the simulations suggested by Nathan's
% annotations on basal ganglia slide stack.

clear
close all

%% CHOOSE
nSamples = 500;
nThreshold = 100;
nXvals = 30;
isAnimated = 0;
isSequentialAnalysis = 1;
isSamplePerThreshold = 1;
T = [1 5 10 15 20 25];
isRandHyp = 0;
if ~isRandHyp
    Hypotheses(1).mu = 2.5;
    Hypotheses(1).sd = 1;
    Hypotheses(1).name = 'normal';
    Hypotheses(2).mu = 3;
    Hypotheses(2).sd = 1;
    Hypotheses(2).name = 'normal';
end

%% INITIALISE
tmpMu = zeros(2, 1);

%% DO
if isRandHyp
    [Hypotheses, ~] = make_hyp(2, 1);
end
for i = 1:2
    tmpMu(i) = Hypotheses(i).mu;
end
[maxMu, idxMu] = max(tmpMu);
hTrue = idxMu;
if hTrue == 1
    
    hNull = 2;
else
    hNull = 1;
end

threshArray = linspace(Hypotheses(hNull).mu - Hypotheses(hNull).sd*3, Hypotheses(hTrue).mu + Hypotheses(hTrue).sd*3, nThreshold);
xValArray = linspace(Hypotheses(hNull).mu - Hypotheses(hNull).sd*3, Hypotheses(hTrue).mu + Hypotheses(hTrue).sd*3, nXvals);

% H_1
tprArrayExact = 1 - cdf('Normal', threshArray, Hypotheses(hTrue).mu, Hypotheses(hTrue).sd);
fnrArrayExact = 1 - tprArrayExact;
tprVarArrayExact = (tprArrayExact .* fnrArrayExact);

% H_0
fprArrayExact = 1 - cdf('Normal', threshArray, Hypotheses(hNull).mu, Hypotheses(hNull).sd);
tnrArrayExact = 1 - fprArrayExact;
fprVarArrayExact = (fprArrayExact .* tnrArrayExact);

% MC sampling
sNullArray = zeros(nSamples, 1);
sTrueArray = zeros(nSamples, 1);
tprArray = zeros(length(threshArray), 1);
fnrArray = zeros(length(threshArray), 1);
fprArray = zeros(length(threshArray), 1);
tnrArray = zeros(length(threshArray), 1);
tprVarArray = zeros(length(threshArray), 1);
fprVarArray = zeros(length(threshArray), 1);
if isSamplePerThreshold
    sNullArrayPerThresholdCount = zeros(length(threshArray), nSamples);
    sTrueArrayPerThresholdCount = zeros(length(threshArray), nSamples);
end

if ~isSamplePerThreshold
    for k = 1:nSamples
        sNullArray(k) = icdf(Hypotheses(hNull).name, rand(1), Hypotheses(hNull).mu, Hypotheses(hNull).sd);
        sTrueArray(k) = icdf(Hypotheses(hTrue).name, rand(1), Hypotheses(hTrue).mu, Hypotheses(hTrue).sd);
    end
end

for i = 1:length(threshArray)
    if isSamplePerThreshold
        for k = 1:nSamples
            sNullArray(k) = icdf(Hypotheses(hNull).name, rand(1), Hypotheses(hNull).mu, Hypotheses(hNull).sd);
            sTrueArray(k) = icdf(Hypotheses(hTrue).name, rand(1), Hypotheses(hTrue).mu, Hypotheses(hTrue).sd);
            sNullArrayPerThresholdCount(i, k) = sNullArray(k);
            sTrueArrayPerThresholdCount(i, k) = sTrueArray(k);
        end
    end
    tprArray(i) = sum(sTrueArray >= threshArray(i)) / nSamples;
    fnrArray(i) = 1 - tprArray(i);
    tprVarArray(i) = tprArray(i) * fnrArray(i);
    fprArray(i) = sum(sNullArray >= threshArray(i)) / nSamples;
    tnrArray(i) = 1 - fprArray(i);
    fprVarArray(i) = fprArray(i) * tnrArray(i);
end





%% PLOT
f1 = figure;
set(f1, 'color','white', 'position', [100 100 900 500])
C = linspecer(length(T)); % set attractive colour order

subplot(2,3,1)
for i = [hNull, hTrue]
    pdfArray(i,:) = pdf(Hypotheses(i).name, xValArray, Hypotheses(i).mu, Hypotheses(i).sd);
end
h1 = plot(xValArray, pdfArray(1,:), 'linewidth', 1);
hold on
h2 = plot(xValArray, pdfArray(2,:), 'linewidth', 1);
title('Probability distributions')
ylabel('f(x)');
legend([h1 h2], {'Noise', 'Signal + noise'})
probDistYLim = ylim;

subplot(2,3,4)
edges = xValArray;
if ~isSamplePerThreshold
    histogram(sNullArray, edges)
    hold on
    histogram(sTrueArray, edges)
    titleStr = sprintf("%d samples per distribution", nSamples);
else
    histogram(reshape(sNullArrayPerThresholdCount, [], 1), edges)
    hold on
    histogram(reshape(sTrueArrayPerThresholdCount, [], 1), edges);
    titleStr = sprintf("%d samples per threshold / distribution", nSamples);
end
xlabel('x'); ylabel('Count');
legend('Noise', 'Signal + noise')
title(titleStr)
histYLim = ylim;

subplot(2,3,6)
axis([0, 1, 0, 1])
hold on
h3 = plot([0 1], [0 1], 'k:');
titleStr = sprintf("%s ROC curve", 'Monte Carlo');
title(titleStr)
xlabel('False positive rate'); ylabel('True positive rate');
e1 = errorbar(fprArray, tprArray, tprVarArray, tprVarArray,...
    zeros(1, length(fprArray)), zeros(1, length(fprArray)), '.', 'capsize', 0);
set(e1, 'color', C(2,:), 'linewidth', 1);
e2 = errorbar(fprArray, tprArray, zeros(1, length(fprArray)), ...
    zeros(1, length(fprArray)), fprVarArray, fprVarArray, '.', 'capsize', 0);
set(e2, 'color', C(1,:), 'linewidth', 1);
plot(fprArray, tprArray, '--ok', 'linewidth', 1);
ax1 = gca; ax1.YColor = C(2,:); ax1.XColor = C(1,:);

subplot(2,3,3)
axis([0, 1, 0, 1])
hold on
h5 = plot([0 1], [0 1], 'k:');
titleStr = sprintf("%s ROC curve", 'Exact');
title(titleStr)
ylabel('True positive rate');
box off
e1 = errorbar(fprArrayExact, tprArrayExact, tprVarArrayExact, tprVarArrayExact,...
    zeros(1, length(fprArrayExact)), zeros(1, length(fprArrayExact)), '.', 'capsize', 0);
set(e1, 'color', C(2,:), 'linewidth', 1);
e2 = errorbar(fprArrayExact, tprArrayExact, zeros(1, length(fprArrayExact)), ...
    zeros(1, length(fprArrayExact)), fprVarArrayExact, fprVarArrayExact, '.', 'capsize', 0);
set(e2, 'color', C(1,:), 'linewidth', 1);
plot(fprArrayExact, tprArrayExact, '--ok', 'linewidth', 1);
t1 = annotation('textbox');
t1.String = 'Error bars show the variance';
t1.FontSize = 8;
t1.EdgeColor = 'none';
t1.Position = [0.82 0.58 0.07 0.15];
ax1 = gca; ax1.YColor = C(2,:); ax1.XColor = C(1,:);

subplot(2,3,2)
b0 = boundedline(threshArray, fprArrayExact, fprVarArrayExact, 'alpha', 'cmap', C(1,:));
hold on
b1 = boundedline(threshArray, tprArrayExact, tprVarArrayExact , 'alpha', 'cmap', C(2,:));
title('Exact true and false positive rates')
ylabel('Exact rate')
legend([b0 b1], {'FPR', 'TPR'}, 'location', 'southwest')

subplot(2,3,5)
b0 = boundedline(threshArray, fprArray, fprVarArray, 'alpha', 'cmap', C(1,:));
hold on
b1 = boundedline(threshArray, tprArray, tprVarArray , 'alpha', 'cmap', C(2,:));
title('MC True and false positive rates')
xlabel('Decision threshold'); ylabel('Rate')
t = annotation('textbox');
t.String = 'Boundary lines show the variance';
t.FontSize = 8;
t.EdgeColor = 'none';
t.Position = [0.55 0.75 0.07 0.15];
legend([b0 b1], {'FPR', 'TPR'}, 'location', 'southwest')


%% ANIMATE
if isAnimated
    selectedThreshold = threshArray(1, floor(nThreshold/2));
    [~, selectedThresholdIndex] = ismember(selectedThreshold, threshArray);
    subplot(2,2,1)
    h8 = plot([selectedThreshold selectedThreshold], probDistYLim, 'k:');
    set(get(get(h8, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle', 'off');
    subplot(2,2,3)
    h9 = plot([selectedThreshold selectedThreshold], histYLim, 'k:');
    set(get(get(h9, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle', 'off');
    subplot(2,2,2)
    sc1 = scatter(fprArrayExact(selectedThresholdIndex), tprArrayExact(selectedThresholdIndex));
    sc1.LineWidth = 0.6; sc1.MarkerEdgeColor = 'b'; sc1.MarkerFaceColor = [0 0.5 0.5];
    subplot(2,2,4)
    sc2 = scatter(fprArray(selectedThresholdIndex), tprArray(selectedThresholdIndex));
    sc2.LineWidth = 0.6; sc2.MarkerEdgeColor = 'b'; sc2.MarkerFaceColor = [0 0.5 0.5];
end

%% SEQUENTIAL ANALYSIS

if isSequentialAnalysis
    f2 = figure;
    set(f2, 'color','white', 'position', [120 120 600 500])
    axis([0, 1, 0, 1])
    h8 = plot([0 1], [0 1], 'k:');
    set(get(get(h8, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle', 'off');
    hold on
    
    % MC sampling
    tprArray = zeros(length(threshArray), 1);
    fnrArray = zeros(length(threshArray), 1);
    fprArray = zeros(length(threshArray), 1);
    tnrArray = zeros(length(threshArray), 1);
    tprVarArray = zeros(length(threshArray), 1);
    fprVarArray = zeros(length(threshArray), 1);
    fprPlotArray = zeros(length(threshArray), length(T));
    tprPlotArray = zeros(length(threshArray), length(T));
    
    iPlt = 1;
    for t = T
        sNullArray = zeros(nSamples, t);
        sTrueArray = zeros(nSamples, t);
        if ~isSamplePerThreshold
            for k = 1:nSamples
                sNullArray(k, :) = icdf(Hypotheses(hNull).name, rand(1, t), Hypotheses(hNull).mu, Hypotheses(hNull).sd);
                sTrueArray(k, :) = icdf(Hypotheses(hTrue).name, rand(1, t), Hypotheses(hTrue).mu, Hypotheses(hTrue).sd);
            end
        end
        
        for i = 1:length(threshArray)
            if isSamplePerThreshold
                for k = 1:nSamples
                    sNullArray(k, :) = icdf(Hypotheses(hNull).name, rand(1, t), Hypotheses(hNull).mu, Hypotheses(hNull).sd);
                    sTrueArray(k, :) = icdf(Hypotheses(hTrue).name, rand(1, t), Hypotheses(hTrue).mu, Hypotheses(hTrue).sd);
                end
            end
            tprArray(i) = sum(mean(sTrueArray, 2) >= threshArray(i)) / nSamples;
            fnrArray(i) = 1 - tprArray(i);
            tprVarArray(i) = tprArray(i) * fnrArray(i);
            fprArray(i) = sum(mean(sNullArray, 2) >= threshArray(i)) / nSamples;
            tnrArray(i) = 1 - fprArray(i);
            fprVarArray(i) = fprArray(i) * tnrArray(i);
        end
        
        fprPlotArray(:,iPlt) = fprArray;
        tprPlotArray(:,iPlt) = tprArray;
        h9 = plot(fprArray, tprArray);
        set(h9, 'linestyle','-', 'color', C(iPlt,:));
        legendCellArr{iPlt} = sprintf("T = %d", t);
        iPlt = iPlt + 1;
    end
    plot(fprPlotArray', tprPlotArray', 'linestyle', '--', 'Color', [150/256, 150/256, 150/256], 'linewidth', 0.5);
    titleStr = sprintf("%s ROC curve", 'Monte Carlo');
    title(titleStr)
    xlabel('False positive rate'); ylabel('True positive rate');
    legend(legendCellArr)
    t = annotation('textbox');
    t.String = 'Mean of T observations per sample. Dashed lines indicate data points with same decision threshold.';
    t.FontSize = 10;
    %t.EdgeColor = 'none';
    t.Position = [0.45 0.25 0.4 0.12];
    
    x = [0.6 0.4]; y = [0.7 0.5];
    ar = annotation('textarrow',x,y);
    t = annotation('textbox');
    t.String = 'Low threshold';
    t.FontSize = 10;
    t.EdgeColor = 'none';
    t.Position = [0.55 0.65 0.4 0.1];
    t = annotation('textbox');
    t.String = 'High threshold';
    t.FontSize = 10;
    t.EdgeColor = 'none';
    t.Position = [0.30 0.4 0.4 0.1];
    
    
end

