% Plot ROC curves for average over multiple time steps in signal detection
% theory.

clear
close all

% CHOOSE
nSamples = 500;
nThreshold = 1000;
nXvals = 30;
maxT = 37;
nTimeSteps = 10;
T = round(linspace(1,maxT,nTimeSteps));
nCritSteps = 100;

discrim = [0.5];
sd = 1;
mu1 = 2;

%% INITIALISE

f1 = figure;
set(f1, 'color','white')

for i = 1:length(discrim)
    tmpMu = zeros(2, 1);
    calcMu2 = discrim(i) * sd + mu1;
    
    Hypotheses(1).mu = mu1;
    Hypotheses(1).sd = sd;
    Hypotheses(1).name = 'normal';
    Hypotheses(2).mu = calcMu2;
    Hypotheses(2).sd = sd;
    Hypotheses(2).name = 'normal';
    
    %% DO
    s = sdt(Hypotheses, nThreshold, nSamples, 'SequentialAnalysis', 1, 'SamplePerThreshold', 0, 'avTimes', T);
    
    %% PLOT
    C = linspecer(length(s.avTimes)); % set attractive colour order
    
    subplot(1,length(discrim), i)
    axis([0, 1, 0, 1])
    h8 = plot([0 1], [0 1], 'k:');
    set(get(get(h8, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle', 'off');
    hold on
    box off
        %% Draw threshold coords
    idx = round(linspace(1,nThreshold,nCritSteps)); %// get rounded equally spaced indices
    tmp = s.fprArrayExact';
    xplot = tmp(:,idx);
    tmp = s.tprArrayExact';
    yplot = tmp(:,idx);
    h9 = plot(xplot, yplot);
    set(h9, 'linestyle','--', 'linewidth', 0.5, 'Color', [150/256, 150/256, 150/256]);
%     if i == length(discrim)
%         legend(legendCellArr)
%     end
    set(gca, 'ColorOrder', C)
    for plt = 1:length(s.avTimes)
        h9 = plot(s.fprArrayExact(:,plt), s.tprArrayExact(:,plt));
        set(h9, 'linestyle','-', 'color', C(plt,:));
    end
    titleStr = sprintf("%s ROC curves: nSDT", 'Exact');
    title(titleStr)
    xlabel('False positive rate'); ylabel('True positive rate');
    for kk = 1:length(s.avTimes)
        legendCellArr{kk} = sprintf("T = %d", s.avTimes(kk));
    end
    
    colormap(gca, C)
    c1 = colorbar;
    labels = arrayfun(@(x) num2str(x), T, 'UniformOutput', false);
    labels_n = cellfun(@(c)['n = ' c],labels,'uni',false);
    c1.TickLabels = labels_n;
    tmp = c1.Ticks;
    jmp = diff(tmp);
    jump = jmp(1)/2;
    new_ticks = tmp(1:end-1) + jump;
    c1.Ticks = new_ticks;
    c1.TickLength = 0;
    
    
    % t = annotation('textbox');
    % t.String = 'Mean of T observations per sample. Dashed lines indicate data points with same decision threshold.';
    % t.FontSize = 10;
    % %t.EdgeColor = 'none';
    % t.Position = [0.45 0.25 0.4 0.12];
    % x = [0.6 0.4]; y = [0.7 0.5];
    % ar = annotation('textarrow',x,y);
    % t = annotation('textbox');
    % t.String = 'Low threshold';
    % t.FontSize = 10;
    % t.EdgeColor = 'none';
    % t.Position = [0.55 0.65 0.4 0.1];
    % t = annotation('textbox');
    % t.String = 'High threshold';
    % t.FontSize = 10;
    % t.EdgeColor = 'none';
    % t.Position = [0.30 0.4 0.4 0.1];
    

end

f1.Units               = 'centimeters';
%f1.Position(3)         = 18.5; % two columns
f1.Position(3)         = 9; % one column
f1.Position(4)         = 6.8;
% set text properties
set(f1.Children, ...
    'FontName',     'Arial', ...
    'FontSize',     8);

set(gcf, 'Renderer', 'painters')
pos = get(f1,'Position');
set(f1,'PaperPositionMode','Auto','PaperUnits','Centimeters','PaperSize',[pos(3), pos(4)])
