clear
close all

%% CHOOSE

%Distribution options
discrim = 0.8;
sd = 1;
mu1 = 2;

calcMu2 = discrim * sd + mu1;
Hypotheses(1).mu = mu1;
Hypotheses(1).sd = sd;
Hypotheses(1).name = 'normal';
Hypotheses(2).mu = calcMu2;
Hypotheses(2).sd = sd;
Hypotheses(2).name = 'normal';

%SPRT options
nEpisodes = 1000;
T = [1, 2:3:50];
threshAltArray = linspace(0, 4, 5);
threshNullArray = linspace(0, 4, 5);

%% Get data and train models

%SPRT
[roc, models] = trainthresh2models(Hypotheses, threshNullArray, threshAltArray, T, 'nEpisodes', 1000, 'FitToAv', 0);

%% Generate training data for threshold to cost-ratio vector-valued function
maxCostRatio = 100;
ptsPerFix = 5;
nullCost = linspace(1, maxCostRatio, ptsPerFix);
altCost = linspace(1, maxCostRatio, ptsPerFix);
nrows = length(nullCost) * length(altCost);
tabThreshPair = zeros(nrows, 2);
tabNullCost = zeros(nrows, 1);
tabAltCost = zeros(nrows, 1);

n = 1;
for i = 1:length(altCost)
    for j = 1:length(nullCost)
        threshPair = [nullCost(j), altCost(i)];
        [~, ~, tabThreshPair(n,:)] = cost2error(threshNullArray, threshAltArray, threshPair, models);
        tabNullCost(n) = threshPair(1);
        tabAltCost(n) = threshPair(2);
        n = n + 1
    end
end

t = table(tabNullCost, tabAltCost, tabThreshPair(:,1), tabThreshPair(:,2));
t.Properties.VariableNames = {'costNull', 'costAlt', 'threshNull', 'threshAlt'};

[trainedModelThresh2NullCost, validationRMSE0] = trainThresh2NullCost(t);
[trainedModelThresh2AltCost, validationRMSE1] = trainThresh2AltCost(t);


%% Get cost-ratio pairs for threshold iso-curves
ptsPerFix = 15;
isoNullThresh = linspace(1, max(threshNullArray), ptsPerFix);
isoAltThresh = linspace(1, max(threshAltArray), ptsPerFix);
results0cell = cell(1, length(isoNullThresh));
results1cell = cell(1, length(isoAltThresh));

f = waitbar(0,'Calculating iso cost curves', 'Name', 'Progress');
for i = 1:length(isoNullThresh)
    tabNull = isoNullThresh(i).*ones(1,ptsPerFix)';
    tabAlt = linspace(min(isoAltThresh), max(isoAltThresh), ptsPerFix)';
    t0 = table(tabNull, tabAlt);
    t0.Properties.VariableNames = {'threshNull', 'threshAlt'};
    for j = 1:length(tabNull)
        threshPair = [tabNull(j), tabAlt(j)];
        %[tpr(j), fpr(j), ~] = cost2error(threshNullArray, threshAltArray, cr, models);
        costRatioPair(j,:) = threshPair2CostPair(threshPair, trainedModelThresh2NullCost, trainedModelThresh2AltCost);
    end
    t0.nullCost = costRatioPair(:,1);
    t0.altCost = costRatioPair(:,2);
    results0cell{i} = t0;
    waitbar(i/(2*length(isoNullThresh)), f);
end

for i = 1:length(isoAltThresh)
    tabNull = linspace(min(isoNullThresh), max(isoNullThresh), ptsPerFix)';
    tabAlt = isoAltThresh(i).*ones(1,ptsPerFix)';
    t1 = table(tabNull, tabAlt);
    t1.Properties.VariableNames = {'threshNull', 'threshAlt'};
    for j = 1:length(tabNull)
        threshPair = [tabNull(j), tabAlt(j)];
        costRatioPair(j,:) = threshPair2CostPair(threshPair, trainedModelThresh2NullCost, trainedModelThresh2AltCost);
    end
    t1.nullCost = costRatioPair(:,1);
    t1.altCost = costRatioPair(:,2);
    results1cell{i} = t1;
    waitbar((length(isoNullThresh)+i)/(2*length(isoNullThresh)), f);
end
close (f)

%% Plot threshold isocurves
f2 = figure;
set(f2, 'color','white', 'position', [150 150 600 500])
% axis([0, 1, 0, 1])
% hold on
% leg = findobj(gcf,'Tag','legend');
% legStr = get(leg, 'String');
%     f2 = figure;
%     set(f2, 'color','white', 'position', [150 150 600 500])
%     %SDT
%     axis([0, 1, 0, 1])

% h8 = plot([0 1], [0 1], 'k:');
% set(get(get(h8, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle', 'off');
%     hold on
for i = 1:length(results0cell)
    plt_tab = results0cell{i};
    h1(i) = plot(plt_tab.nullCost, plt_tab.altCost);
    if i == 1
        hold on
    end
    set(h1(i), 'linestyle', ':', 'color', 1/256.*[128 128 128],...
        'linewidth', 0.8);
end
for i = 1:length(results1cell)
    plt_tab = results1cell{i};
    h2(i) = plot(plt_tab.nullCost, plt_tab.altCost);
    set(h2(i), 'linestyle', ':', 'color', 1/256.*[128 128 128],...
        'linewidth', 0.8);
end
%legend(legStr)
xlim([0 140]); ylim([0 140]);
xlabel('W_0/c'), ylabel('W_1/c');
