%%
f1.Units               = 'centimeters';
f1.Position(3)         = 18.5; % two columns
f1.Position(3)         = 9; % one column
f1.Position(4)         = 5;
% set text properties
set(f1.Children, ...
    'FontName',     'Arial', ...
    'FontSize',     8);

%% export to png
f1.PaperPositionMode   = 'auto';
print(['roc_curve_fig_1_v2'], '-dpng', '-r600')