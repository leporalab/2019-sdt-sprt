[X,Y] = meshgrid(1:0.1:10,1:0.1:20);
Z = sin(X) + cos(Y);
surf(X,Y,Z)
shading interp
hold on

data = [pi, pi/2, sin(pi) + cos(pi/2)];
scatter3(data(1), data(2), data(3), 'filled')