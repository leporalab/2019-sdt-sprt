clear
close all

T = 1:50;
threshAltArray = linspace(0, 4, 10);
threshNullArray = linspace(0, 4, 10);

nXvals = 50;

discrim = [0.5];
sd = 1;
mu1 = 2;

f1 = figure;
set(f1, 'color','white')

for iDis = 1:length(discrim)
    calcMu2 = discrim(iDis) * sd + mu1;
    
    Hypotheses(1).mu = mu1;
    Hypotheses(1).sd = sd;
    Hypotheses(1).name = 'normal';
    Hypotheses(2).mu = calcMu2;
    Hypotheses(2).sd = sd;
    Hypotheses(2).name = 'normal';
    
    [roc, trainedModel, tmn, tmtpr, tmfpr] = sprt4roc_pub_fig_6(Hypotheses, threshNullArray, threshAltArray, T, 'nEpisodes', 1000, 'FitToAv', 1);
    
    
    
    %%
    ps = 10;
    nullarrayplot = linspace(min(threshNullArray), max(threshNullArray), ps);
    altarrayplot = linspace(min(threshAltArray), max(threshAltArray), ps);
    newThreshNullArray = repmat(nullarrayplot, 1, length(nullarrayplot));
    newThreshAltArray = repelem(altarrayplot, length(altarrayplot));
    t = table(newThreshNullArray', newThreshAltArray');
    t.Properties.VariableNames = {'threshNull', 'threshAlt'};
    
    [Xfit, Xsd, ~] = tmfpr.predictFcn(t);
    [Yfit, Ysd, ~] = tmtpr.predictFcn(t);
    [Zfit, Zds, ~] = tmn.predictFcn(t);
    
    Xm = reshape(Xfit, [ps,ps]); Ym = reshape(Yfit, [ps,ps]); Zm = reshape(Zfit, [ps,ps]);
    
    Xfit(Xfit < 0) = 0.0001;
    Yfit(Yfit < 0) = 0.0001;
    Xfit(Xfit > 1) = 0.9999;
    Yfit(Yfit > 1) = 0.9999;
    
    % Calculate sample size
    p1 = [Hypotheses(1).mu, Hypotheses(1).sd];
    p2 = Hypotheses(2).mu;
    for j = 1:length(Xfit)
        ncalc(j) = sampsizepwr('z', p1, p2, Yfit(j), [], 'Alpha', Xfit(j), 'tail', 'right');
    end
    
    diffn = 100*(ncalc' - Zfit)./ncalc';
    diffnM = reshape(diffn, [ps,ps]);
    
    figure(f1)
    %subplot(1,3,iDis)
    axis([0, 1, 0, 1])
    hold on
    %surf(Xm, Ym, Zm)
    surf(Xm, Ym, diffnM)
    plot([0 1], [0 1], 'k:')
    zl(iDis,:) = zlim;
    c1(iDis) = colorbar;
    
    % fpr = linspace(0.01, 0.4, 20);
    % tpr = linspace(0.6, 0.99, 20);
    %     alpha = linspace(0.01, 0.40, 15);
    %     beta = linspace(0.01, 0.40, 15);
    %
    %     ncalc = zeros(length(alpha), length(beta));
    %     nout = zeros(length(alpha), length(beta));
    %     avN = zeros(length(alpha), length(beta));
    %
    %     p1 = [Hypotheses(1).mu, Hypotheses(1).sd];
    %     p2 = Hypotheses(2).mu;
    %
    %     for i = 1:length(alpha)
    %         for j = 1:length(beta)
    %             pwr = 1 - beta;
    %             nout(i,j) = sampsizepwr('z', p1, p2, pwr, [], 'Alpha', alpha, 'tail', 'right');
    %             Eq. (3.67) in Wald (1947)
    %             lambda = icdf('norm', [1-alpha(i), beta(j)], 0, 1);
    %             ncalc(i,j) = ((lambda(1) - lambda(2))^2) ./ (Hypotheses(1).mu - Hypotheses(2).mu).^2;
    %
    %             t = table(alpha(i), 1-beta(j));
    %             t.Properties.VariableNames = {'fpr', 'tpr'};
    %             avN(i,j) = trainedModel.predictFcn(t);
    %         end
    %     end
    %
    %     [fpr, tpr] = meshgrid(alpha, 1-beta);
    
    % subplot(1, 3, 1)
    % surface(fpr, tpr, ncalc)
    % view(2)
    % xlabel('FPR'); ylabel('TPR'); zlabel('sample size, n');
    % colorbar
    % title('Required sample size in ROC space for SDT')
    %
    % subplot(1, 3, 2)
    % surface(fpr, tpr, avN)
    % view(2)
    % xlabel('FPR'); ylabel('TPR'); zlabel('E(n)');
    % colorbar
    % title('E(n) in ROC space for SPRT')
    
    %     figure(f1)
    %     subplot(2,3,iDis + length(discrim))
    %     surface(fpr, tpr, 100*(ncalc - avN)./ncalc);
    %     view(2)
    %     zl(iDis,:) = zlim;
    %     xlabel('FPR'); ylabel('TPR'); zlabel('E[% saving in n] in ROC space for SPRT vs. SDT');
    %     c1(iDis) = colorbar;
    %     titString = sprintf('d'' = %.2f', discrim(iDis));
    %     title(titString)
    %
    %     figure(f2)
    %     subplot(2,3,iDis)
    %     surface(fpr, tpr, ncalc);
    %     view(3)
    %     xlabel('FPR'); ylabel('TPR'); zlabel('Calculated sample size for SDT');
    %     %c1(iDis) = colorbar;
    %     titString = sprintf('d'' = %.2f', discrim(iDis));
    %     title(titString)
    %
    %
    %     subplot(2,3,iDis + length(discrim))
    %     surface(fpr, tpr, avN);
    %     view(3)
    %     xlabel('FPR'); ylabel('TPR'); zlabel('Predicted E(n) for SPRT');
    %     %c1(iDis) = colorbar;
    %     titString = sprintf('d'' = %.2f', discrim(iDis));
    %     title(titString)
    
end



figure(f1)

zlim([min(zl(:)), max(zl(:))]);
caxis([min(zl(:)), max(zl(:))]);
colorBarLimitAll(1,:) = c1(1).Limits;

colorBarLimits = [min(colorBarLimitAll(:)), max(colorBarLimitAll(:))];


rLim1 = floor(colorBarLimits(1)) - rem(10, floor(colorBarLimits(1)));
if isnan(rLim1)
    rLim1 = 0;
end
rLim2 = ceil(colorBarLimits(2)) + rem(10, ceil(colorBarLimits(2)));
c1(1).Limits = [rLim1, rLim2];
%set(c1(i), 'Ticks', linspace(rLim1, rLim2, 5));
ylabel(c1, 'Relative efficiency')

xlabel('False positive rate'); ylabel('True positive rate');
box off
shading interp
title('Efficiency of SPRT over fixed sample');

%%
f1.Units               = 'centimeters';
%f1.Position(3)         = 18.5; % two columns
f1.Position(3)         = 9; % one column
f1.Position(4)         = 6.5;
% set text properties
set(f1.Children, ...
    'FontName',     'Arial', ...
    'FontSize',     8);

set(gcf, 'Renderer', 'painters')

pos = get(f1,'Position');
set(f1,'PaperPositionMode','Auto','PaperUnits','Centimeters','PaperSize',[pos(3), pos(4)])

