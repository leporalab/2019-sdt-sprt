clear
close all

%% CHOOSE

%Distribution options
discrimArr = [1.5 1.0 0.25];
sd = 1;
mu1 = 2;
nXvals = 30;

%SPRT options
nEpisodes = 500;
ContourLevels = [2 3];
threshAltArray = linspace(0, 3, 5);
threshNullArray = linspace(0, 3, 5);

%SDT options
nSamples = 500;
nThreshold = 50;
T = ContourLevels;

iD = 1;
f2 = figure;
set(f2, 'color','white', 'position', [150 150 600 500])
C = linspecer(length(ContourLevels));

for discrim = discrimArr
    calcMu2 = discrim * sd + mu1;
    Hypotheses(1).mu = mu1;
    Hypotheses(1).sd = sd;
    Hypotheses(1).name = 'normal';
    Hypotheses(2).mu = calcMu2;
    Hypotheses(2).sd = sd;
    Hypotheses(2).name = 'normal';
    
    %SDT
    s = sdt(Hypotheses, nThreshold, nSamples, 'SequentialAnalysis', 1, 'avTimes', T);
    
    %SPRT
    roc = sprt4roc(Hypotheses, threshNullArray, threshAltArray, T, 'nEpisodes', 500, 'FitToAv', 1);
    
    %% PLOT
    markerList = {'+', 'o', '*', 'x', 's', 'd', '^'};
    
    figure(f2)
    subplot(2,length(discrimArr),iD)
    xValArray = linspace(Hypotheses(s.hNoise).mu - Hypotheses(s.hNoise).sd*3, Hypotheses(s.hSignal).mu + Hypotheses(s.hSignal).sd*3, nXvals);
    for i = [s.hNoise, s.hSignal]
        pdfArray(i,:) = pdf(Hypotheses(i).name, xValArray, Hypotheses(i).mu, Hypotheses(i).sd);
    end
    h1 = plot(xValArray, pdfArray(1,:), 'linewidth', 1);
    hold on
    h2 = plot(xValArray, pdfArray(2,:), 'linewidth', 1);
    title(sprintf('d'' = %.1f', discrim))
    ylabel('f(x)');
    legend([h1 h2], {'Noise', 'Signal + noise'})
    probDistYLim = ylim;
    
    %SDT
    subplot(2,length(discrimArr), iD+length(discrimArr))
    axis([0, 1, 0, 1])
    h8 = plot([0 1], [0 1], 'k:');
    set(get(get(h8, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle', 'off');
    hold on
    for i = 1:length(ContourLevels)
        h1 = plot(s.fprArrayExact(:,i), s.tprArrayExact(:,i));
        set(h1, 'linestyle', '--', 'color', C(i,:));
        h2 = plot(roc.fpr{i}, roc.tpr{i});
        set(h2, 'marker', markerList{i}, 'linestyle', 'none', 'color', C(i,:));
    end
    legList = arrayfun(@(x) num2str(x), ContourLevels, 'UniformOutput', false);
    legList = ['Averaged SDT', 'SPRT', legList];
    d(1) = plot([100 100], [100 100], 'k--');
    d(2) = plot([100 100], [100 100], 'k+');
    ylim([0 1]); xlim([0 1]);
    legend([d(1) d(2)], legList, 'location', 'SouthEast')
    xlabel('False positive rate'); ylabel('True positive rate');
    %titleStr = sprintf("%s ROC curve", 'Monte Carlo');
    %title(titleStr)
    iD = iD + 1;
end
