clear
close all

t = 0:0.01:100;

sig = cos(2*pi*1000*t);
sig2 = 2.*sig;
noise = randn(1, length(t));
signoise = sig + noise;
subplot(3,2,1)
h1 = plot(t, sig);
subplot(3,2,3)
h2 = plot(t, noise, 'linewidth', 1);
subplot(3,2,5)
h3 = plot(t, sig + noise, 'linewidth', 1);

subplot(3,2,2)
h4 = plot(t, sig2);
subplot(3,2,4)
h5 = plot(t, noise, 'linewidth', 1);
subplot(3,2,6)
h6 = plot(t, sig2 + noise, 'linewidth', 1);

