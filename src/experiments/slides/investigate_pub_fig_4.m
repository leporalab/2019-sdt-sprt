clear
close all

%% Calculate Panel B
T = [1 5];
tlim = 4;

threshArray = linspace(0, tlim, 21);

discrim = 0.5;
sd = 1;
mu1 = 2;
calcMu2 = discrim * sd + mu1;

Hypotheses(1).mu = mu1;
Hypotheses(1).sd = sd;
Hypotheses(1).name = 'normal';
Hypotheses(2).mu = calcMu2;
Hypotheses(2).sd = sd;
Hypotheses(2).name = 'normal';

nEpisodes = 1000;

[tprArray, fprArray, tabDecTime] = sprt4roc_equal_thresh(Hypotheses, threshArray, threshArray, T, 'nEpisodes', nEpisodes, 'FitToAv', 1);

%%
for i = 1:length(tabDecTime)
    meanTime(i) = mean(mean(tabDecTime{i}));
    %varTime(i) = var(tabDecTime{i});
    stdTime(i) = std(tabDecTime{i}(:));
end

%% Calculate Panel C

%SPRT options
nEpisodes = 500;
ContourLevels = [1, 5];
threshAltArray = linspace(0, 3, 10);
threshNullArray = linspace(0, 3, 10);

%SDT options
nSamples = 500;
nThreshold = 50;
T = ContourLevels;

s = sdt(Hypotheses, nThreshold, nSamples, 'SequentialAnalysis', 1, 'SamplePerThreshold', 0, 'avTimes', T);

%SPRT
roc = sprt4roc_pub_fig_4(Hypotheses, threshNullArray, threshAltArray, T, 'nEpisodes', 500, 'FitToAv', 1);
tmp = length(roc.tpr{1});
if tmp > 1
    roc.tpr{1} = mean(roc.tpr{1});
    roc.fpr{1} = mean(roc.fpr{1});
end

%% PLOT
f1 = figure;
set(f1, 'color','white')
C = get(gca,'colororder'); % set attractive colour order

%% Panel A

%calculate
tNull = tlim;
tAlt = tlim;
hTrue = 2;
nTrajectories = 5;
maxT = 10000;

b = 0;
while b ~= 1
    Y = cell(nTrajectories, 1);
    sel = zeros(nTrajectories, 1);
    decTime = zeros(nTrajectories, 1);
    for i = 1:length(Y)
        [sel(i), Y{i}, decTime(i)] = sprt(Hypotheses, hTrue, maxT, tNull, tAlt);
    end
    b = sum(sel<2);
end

%plot
shrink = 0.7;
s1 = subplot(1,3,1);
s1Pos = get(s1, 'Position');
s1Height = s1Pos(4) - s1Pos(2);
s1NewHeight = s1Height * shrink;
s1.Position = [s1Pos(1) s1Pos(2) s1Pos(3) s1Pos(2) + s1NewHeight];
for i = 1:length(Y)
    plot(1:decTime(i)+1, Y{i}(1:decTime(i)+1));
    hold on
end
ylim([-tNull-1, tAlt+1]);
xlim([0 80])
hl3 = hline(-tNull, 'k:');
hl3.Color = C(1,:);
hl4 = hline(tAlt, 'k:');
hl4.Color = C(2,:);
xlabel('Number of observations'); ylabel('Evidence, Z');
box off

t0 = annotation('textbox');
t0.String = 'threshold, \theta_0';
t0.FontSize = 7;
t0.EdgeColor = 'none';
t0.Position = [ 0.1650    0.2079    0.0983    0.1200];
t0.Color = C(1,:);

t1 = annotation('textbox');
t1.String = 'threshold, \theta_1';
t1.FontSize = 7;
t1.EdgeColor = 'none';
t1.Position = [0.1650    0.8280    0.1026    0.1200];
t1.Color = C(2,:);



%% Hist A
s2 = subplot('Position', [s1Pos(1), s1Pos(2) + s1NewHeight+0.15, s1Pos(3), 0.17]); 
for i = 1:1000
    [sel(i), Y{i}, decTime(i)] = sprt(Hypotheses, hTrue, maxT, tNull, tAlt);
end
bb = get(s1, 'Xlim');
hist(decTime,1:bb(2)+1)
xlim(bb)
a1 = gca; a1.XTickLabel = '';
box off
ylabel('Count')

%% Panel B
s3 = subplot(1,3,2);
yyaxis left
b0 = boundedline(threshArray, fprArray, 1.96.*sqrt((1/nEpisodes)*fprArray.*(1-fprArray)), 'alpha', 'cmap', C(1,:));
hold on
b1 = boundedline(threshArray, tprArray, 1.96.*sqrt((1/nEpisodes)*tprArray.*(1-tprArray)), 'alpha', 'cmap', C(2,:));
ylim([0 1]);
xlabel('Decision thresholds, \theta_0 = \theta_1'); ylabel('Rate')
yyaxis right
b2 = boundedline(threshArray, meanTime, 1.96*stdTime/sqrt(2*nEpisodes), 'alpha', 'cmap', C(5,:));
%title('SPRT true and false positive rates')
%xlabel('Decision thresholds, \theta_0 = \theta_1');
ylabel('<n>')

% t = annotation('textbox');
% t.String = 'Shaded regions show the 95% CI';
% t.FontSize = 7;
% t.EdgeColor = 'none';
% t.Position = [0.5355    0.3859    0.0858    0.1500];
l1 = legend([b0 b1 b2], {'FPR', 'TPR', '<n>'}, 'location', 'southwest');
l1.Box = 'off'; l1.Position = [0.5497    0.7017    0.0779    0.1380];

%% PLOT Panel C
figure(f1)
s4 = subplot(1, 3, 3);
axis([0, 1, 0, 1])
h8 = plot([0 1], [0 1], 'k:');
set(get(get(h8, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle', 'off');
hold on
linOrd = {'--','-'}; linWid = [0.3 0.3];
linOrdSprt = {'none', '-'}; MarkerOrd = {'+', 'none'};
for plt = 1:length(s.avTimes)
    h9(plt) = plot(s.fprArrayExact(:,plt), s.tprArrayExact(:,plt));
    set(h9(plt), 'linestyle', linOrd{plt}, 'lineWidth', linWid(plt), 'Color', [150/256, 150/256, 150/256]);
    h8(plt) = plot(roc.fpr{plt}, roc.tpr{plt}, 'Color', C(4,:),...
        'linewidth', 1,...
        'Marker', MarkerOrd{plt},...
        'linestyle', linOrdSprt{plt});
end
titleStr = "Exact ROC curves";
%title(titleStr)
xlabel('False positive rate'); ylabel('True positive rate');
box off
% t = annotation('textbox');
% t.String = 'n=1';
% t.FontSize = 7;
% t.EdgeColor = 'none';
% %t.EdgeColor = 'none';
% t.Position = [0.7557 0.6188 0.4 0.12];
l2 = legend([h9(1) h9(2) h8(1) h8(2)], {'SDT, n=1', 'SDT, n=5', 'SPRT, n=1', 'SPRT, n=5'});
l2.Box = 'off';
% t1 = annotation('textbox');
% t1.String = 'n=5';
% t1.FontSize = 7;
% t1.EdgeColor = 'none';
% %t.EdgeColor = 'none';
% t1.Position = [0.7272 0.6894 0.4 0.12];



%% Format figure
f1.Units               = 'centimeters';
f1.Position(3)         = 18.5;
f1.Position(4)         = 5;
% set text properties
set(f1.Children, ...
    'FontName',     'Arial', ...
    'FontSize',     8);

l1.FontSize = 7;
l2.FontSize = 7;

set(gcf, 'Renderer', 'painters')

s3Pos = get(s3, 'Position');
oldPos = get(s1, 'Position');
s1.Position = [oldPos(1) 0.173 oldPos(3) 0.6];
s2.Position = [oldPos(1) 0.81 oldPos(3) 0.1];
