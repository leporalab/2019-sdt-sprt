clear
close all

%% Set parameters.
% Define hypotheses distributions.
Hyp.mean = [-1/3, 1/3]; % 1 index is 'noise', 2 index is 'signal'
Hyp.sd = 1;
Hyp.nHyp = 2;
Hyp.nTrials = 1e5;
Hyp.maxT = 200;

% Generate evidence and select true hypotheses (targets).
[Hyp.evidence, Hyp.target] = generateSprtEvidence_rap(Hyp);

% Initialise sprt bounds. To force equal magnitudes just enter one value.
ini_sprtBounds = [0 0];

costRatios = [10, 10]; % (W_0/c, W_1/c)
threshRange = [0, 1];


%% Optimise thresholds using exhaustive search
precision = 0.001;
SamplingOut = optExhaustive_rap(costRatios, threshRange, Hyp, precision);

%% Plot sampling optimiser
plotExhaustiveData_rap(SamplingOut);

%% Indicate finish of single learning episode
beep
fprintf('REINFORCE optimised threshold = %f \n', optThreshReinforce')
fprintf('Sampling optimised threshold = %f \n', SamplingOut.optThresh)

%% Do cost-ratio parameter sensitivity study
numLearningEpisodes = 200;
lowerBoundCR = 10;
inverseCostRatio = (1/lowerBoundCR)*rand(1, numLearningEpisodes);
costRatioArr = 1./inverseCostRatio;

% Initialise plot data arrays
[Saved.thresholdAv, ...
    Saved.decisionTimeAv,...
    Saved.rewardAv,...
    Saved.errorAv,...
    Saved.thresholdExhaustive] = deal(zeros(1, numLearningEpisodes));

% Number of trials to average over (counting back from final trial)
numTrialsAv = 100;

for iEpisode = 1:numLearningEpisodes
    % Generate new evidence trajectories
    [Hyp.evidence, Hyp.target] = generateSprtEvidence(Hyp);
    
    % Set cost ratio and optimise thresholds using REINFORCE (Gaussian Unit)
    costRatios = [costRatioArr(iEpisode), costRatioArr(iEpisode)];
    [PlotData, optThreshReinforce, ~, ~] = optReinforceGaussian(costRatios, ...
        ini_sprtBounds, Hyp, ReinParam, GaussParam);
    
    % Average over last numTrialsAv
    Saved.thresholdAv(iEpisode) = mean(PlotData.thresh(1, end - numTrialsAv:end));
    Saved.decisionTimeAv(iEpisode) = mean(PlotData.decTime(end - numTrialsAv:end));
    Saved.rewardAv(iEpisode) = mean(PlotData.reward(end - numTrialsAv:end));
    Saved.errorAv(iEpisode) = mean(PlotData.error(end - numTrialsAv:end));
    
    % Optimise thresholds using exhaustive search
    SamplingOut = optExhaustive(costRatios, threshRange, Hyp);
    Saved.thresholdExhaustive(iEpisode) = SamplingOut.optThresh;
    Saved.rewardExhaustive(iEpisode) = SamplingOut.optReward;
    disp(iEpisode)
    
end

% Plot cost ratio sensitivity data
f1 = figure; numPlots = 4;
set(f1, 'color', 'white', 'units', 'centimeters', 'position', [1 1 9 18])

subplot(numPlots, 1, 1)
plot(1./costRatioArr, Saved.errorAv, 'k.')
title('Decision error'); ylabel('Decision error, e');

subplot(numPlots, 1, 2)
plot(1./costRatioArr, Saved.decisionTimeAv, 'k.' )
title('Decision time'); ylabel('Decision time, T');

subplot(numPlots, 1, 3)
plot(1./costRatioArr, Saved.rewardAv, 'k.')
title('Reward'); ylabel('Reward, r');

subplot(numPlots, 1, 4)
plot(1./costRatioArr, Saved.thresholdAv, 'k.')
hold on
% Exhaustive threshold plot
[sortedCostRatio, indices] = sort(costRatioArr);
sortedThresholdExhaustive = Saved.thresholdExhaustive(indices);
plot(1./sortedCostRatio, sortedThresholdExhaustive)
title('Decision threshold'); xlabel('cost parameter, c_0/W=c_1/W'); ylabel('Decision threshold, \theta_0 = \theta_1');


%% Indicate finish
beep

%% Save output variables for cost ratio sensitivity study
filePath = sprintf('../results/costSensitivityOutput_%s.mat', datestr(now, 'mm-dd-yyyy HH-MM'));
save(filePath, 'Saved')