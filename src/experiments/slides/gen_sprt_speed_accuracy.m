clear
close all

T = [2 3 4 5 6];
threshAltArray = linspace(0, 3, 5);
threshNullArray = linspace(0, 3, 5);

discrim = 0.5;
sd = 1;
mu1 = 2;
calcMu2 = discrim * sd + mu1;

Hypotheses(1).mu = mu1;
Hypotheses(1).sd = sd;
Hypotheses(1).name = 'normal';
Hypotheses(2).mu = calcMu2;
Hypotheses(2).sd = sd;
Hypotheses(2).name = 'normal';

roc = sprt4roc_speed_accuracy(Hypotheses, threshNullArray, threshAltArray, T, 'nEpisodes', 500, 'FitToAv', 1);

%% PLOT

% Fit logistic regression model to error and n data
B = glmfit(roc.n, [roc.error ones(length(roc.error), 1)], 'binomial', 'link', 'logit');
X = linspace(0, max(roc.n), 100);
Z = Logistic(B(1) + X*B(2));

f1 = figure;
set(f1, 'color','white', 'position', [100 100 900 350])

subplot(1,2,1)
scatter(roc.n, roc.error, '.')
xlabel('n'); ylabel('error');
ylim([-0.1 1.1])
hold on
plot(X, Z)