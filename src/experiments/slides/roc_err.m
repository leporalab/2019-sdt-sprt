function rocError = roc_err(threshNull, threshAlt, cr, models)
%UNTITLED5 Find tpr and fpr error for findmaxcost
%   Detailed explanation goes here

t_fpr = table(min(threshNull), max(threshAlt));
t_fpr.Properties.VariableNames = {'threshNull', 'threshAlt'};
t_tpr = table(max(threshNull), min(threshAlt));
t_tpr.Properties.VariableNames = {'threshNull', 'threshAlt'};

[costtpr, costfpr] = cost2error(threshNull, threshAlt, cr, models);

rocError = (costfpr - models.thresh2Fpr.predictFcn(t_fpr))^2 + (costtpr - models.thresh2Tpr.predictFcn(t_tpr))^2;
end

