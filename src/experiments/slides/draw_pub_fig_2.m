% Plot ROC curves for average over multiple time steps in signal detection
% theory.

clear
close all

% CHOOSE
nSamples = 500;
nThreshold = 1000;
nXvals = 100;
maxT = 50;
nTimeSteps = 10;
%T = round(linspace(1,maxT,nTimeSteps));
T = [1 5];
nCritSteps = 100;

discrim = [0.5];
sd = 1;
mu1 = 2;

%% INITIALISE

f1 = figure;
set(f1, 'color','white')



for i = 1:length(discrim)
    tmpMu = zeros(2, 1);
    calcMu2 = discrim(i) * sd + mu1;
    
    Hypotheses(1).mu = mu1;
    Hypotheses(1).sd = sd;
    Hypotheses(1).name = 'normal';
    Hypotheses(2).mu = calcMu2;
    Hypotheses(2).sd = sd;
    Hypotheses(2).name = 'normal';
    
    %% DO
    s = sdt(Hypotheses, nThreshold, nSamples, 'SequentialAnalysis', 1, 'SamplePerThreshold', 0, 'avTimes', T);
    
    %% PLOT
    C = linspecer(length(s.avTimes)); % set attractive colour order
    xValArray = linspace(Hypotheses(s.hNoise).mu - Hypotheses(s.hNoise).sd*3, Hypotheses(s.hSignal).mu + Hypotheses(s.hSignal).sd*3, nXvals);
    
    subplot(1,3,1)
    for i = [s.hNoise, s.hSignal]
        pdfArray(i,:) = pdf(Hypotheses(i).name, xValArray, Hypotheses(i).mu, Hypotheses(i).sd);
        pdf2Array(i,:) = pdf(Hypotheses(i).name, xValArray, Hypotheses(i).mu, Hypotheses(i).sd/sqrt(T(2)));
    end
    h1 = plot(xValArray, pdfArray(1,:), 'linewidth', 0.3, 'linestyle', '--');
    hold on
    h2 = plot(xValArray, pdfArray(2,:), 'linewidth', 0.3, 'linestyle', '--');
    clrOrd = get(gca, 'colororder'); 
    h3 = plot(xValArray, pdf2Array(1,:), 'linewidth', 1, 'color', clrOrd(1,:));
    h4 = plot(xValArray, pdf2Array(2,:), 'linewidth', 1, 'color', clrOrd(2,:));
    title('Sample mean distributions')
    ylabel('f(x)'); xlabel('x')
    %legend([h1 h2], {'Noise', 'Signal + noise'})
    probDistYLim = ylim;
    vline(mu1 + (calcMu2 - mu1)/2, 'k:')
    box off
    l4 = legend([h3 h4 h1 h2], {'n=5', 'n=5', 'n=1', 'n=1'});
    l4.Box = 'off';
    
    subplot(1,3,2)
    linOrd = {'--','-'};
    linWid = [0.3 1];
    %clrOrd = get(gca, 'colororder');
%     [grad1,im]=colorGradient(clrOrd(1,:),1/256*[201 222 236],nTimeSteps);
%     [grad2,im]=colorGradient(clrOrd(2,:),1/256*[243 221 212],nTimeSteps);
    %b0 = boundedline(s.threshArray, s.fprArrayExact, s.fprVarArrayExact, 'alpha', 'cmap', C(1,:));
    for plt = 1:2
        b0(plt) = plot(s.threshArray, s.fprArrayExact(:,plt), ...
            'linewidth', linWid(plt),...
            'linestyle', linOrd{plt},...
            'color', clrOrd(1,:));
        hold on
        %b1 = boundedline(s.threshArray, s.tprArrayExact, c.*s.tprVarArrayExact , 'alpha', 'cmap', C(2,:));
        b1(plt) = plot(s.threshArray, s.tprArrayExact(:,plt),...
            'linewidth', linWid(plt),...
            'linestyle', linOrd{plt},...
            'color', clrOrd(2,:));
    end
    title('P(choose H_1): nSDT')
    ylabel('P(choose H_1)'); xlabel('Decision threshold, \theta')
    l1 = legend([b0(2) b1(2) b0(1) b1(1)], {'h_0, n=5', 'h_1, n=5', 'h_0, n=1', 'h_1, n=1'}, 'Box', 'off');
    
    box off
    
    subplot(1, 3, 3)
    axis([0, 1, 0, 1])
    h8 = plot([0 1], [0 1], 'k:');
    set(get(get(h8, 'Annotation'), 'LegendInformation'), 'IconDisplayStyle', 'off');
    hold on
    for plt = 1:length(s.avTimes)
        h9 = plot(s.fprArrayExact(:,plt), s.tprArrayExact(:,plt), 'k');
        set(h9, 'linestyle', linOrd{plt}, 'lineWidth', linWid(plt));
    end
    titleStr = "Exact ROC curves: nSDT";
    title(titleStr)
    xlabel('False positive rate'); ylabel('True positive rate');
    for kk = 1:length(s.avTimes)
        legendCellArr{kk} = sprintf("T = %d", s.avTimes(kk));
    end
    box off
    ax1 = gca; ax1.YColor = C(2,:); ax1.XColor = C(1,:);
    t = annotation('textbox');
    t.String = 'n=1';
    t.FontSize = 7;
    t.EdgeColor = 'none';
    %t.EdgeColor = 'none';
    t.Position = [0.7557 0.6188 0.4 0.12];
    
    t1 = annotation('textbox');
    t1.String = 'n=5';
    t1.FontSize = 7;
    t1.EdgeColor = 'none';
    %t.EdgeColor = 'none';
    t1.Position = [0.7272 0.6894 0.4 0.12];
    % x = [0.6 0.4]; y = [0.7 0.5];
    % ar = annotation('textarrow',x,y);
    % t = annotation('textbox');
    % t.String = 'Low threshold';
    % t.FontSize = 10;
    % t.EdgeColor = 'none';
    % t.Position = [0.55 0.65 0.4 0.1];
    % t = annotation('textbox');
    % t.String = 'High threshold';
    % t.FontSize = 10;
    % t.EdgeColor = 'none';
    % t.Position = [0.30 0.4 0.4 0.1];
    
    %% Draw threshold coords
%     idx = round(linspace(1,nThreshold,nCritSteps)); %// get rounded equally spaced indices
%     tmp = s.fprArrayExact';
%     xplot = tmp(:,idx);
%     tmp = s.tprArrayExact';
%     yplot = tmp(:,idx);
%     h9 = plot(xplot, yplot);
%     set(h9, 'linestyle','--', 'linewidth', 0.5, 'Color', [150/256, 150/256, 150/256]);
    
    %legend(legendCellArr)
    
end

%%
f1.Units               = 'centimeters';
f1.Position(3)         = 18.5;
f1.Position(4)         = 5;
% set text properties
set(f1.Children, ...
    'FontName',     'Arial', ...
    'FontSize',     8);

l1.FontSize = 7;
l4.FontSize = 7;

set(gcf, 'Renderer', 'painters')
pos = get(f1,'Position');
set(f1,'PaperPositionMode','Auto','PaperUnits','Centimeters','PaperSize',[pos(3), pos(4)])

