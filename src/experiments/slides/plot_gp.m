clear
close all

load('gp_data2.mat')

% What were predictors for gp_data.mat?
threshAltArray = linspace(0.01, 3, 10);
threshNullArray = linspace(0.01, 3, 10);

nContourLevels = 5;

f1 = figure;
set(f1, 'color','white', 'position', [100 100 1400 350])

%% Fit GPs

% Plot AvT
subplot(1,4,1)
[trainedModelAvT, validationRMSE] = trainRegressionModelAvT(t);
[zfit, zsd, zint] = trainedModelAvT.predictFcn(t);
scatter3(t.threshNull, t.threshAlt, t.avT, 20, 'MarkerEdgeColor','k',...
        'MarkerFaceColor','r')
hold on
plot3d_errorbars(t.threshNull, t.threshAlt, zfit,...
    zeros(length(t.threshNull)), zeros(length(t.threshAlt)), 2.*zsd, '.r')
[X,Y] = meshgrid(threshNullArray, threshAltArray);
Z = reshape(zfit, 10, 10);
surf(X,Y,Z)
[CavT, c1] = contour3(X,Y,Z,nContourLevels,'ShowText','off');
c1.Visible = 'off';
[xAvT, yAvT, zAvT] = C2xyz(CavT);
xlabel('\theta_0'); ylabel('\theta_1'); zlabel('E(T)');
title('E(T)')

% Plot TPR
subplot(1,4,2)
[trainedModelTpr, validationRMSE] = trainRegressionModelTpr(t);
[zfit, zsd, zint] = trainedModelTpr.predictFcn(t);
scatter3(t.threshNull, t.threshAlt, t.tpr, 20, 'MarkerEdgeColor','k',...
        'MarkerFaceColor','r')
hold on
plot3d_errorbars(t.threshNull, t.threshAlt, zfit,...
    zeros(length(t.threshNull)), zeros(length(t.threshAlt)), 2.*zsd, '.r')

[X,Y] = meshgrid(threshNullArray, threshAltArray);
Z = reshape(zfit, 10, 10);
surf(X,Y,Z)
[Ctpr, c2] = contour3(X,Y,Z,'ShowText','off');
c2.Visible = 'off';
CoordTpr = C2xyz(Ctpr);
xlabel('\theta_0'); ylabel('\theta_1'); zlabel('TPR');
title('TPR')

% Plot FPR
subplot(1,4,3)
[trainedModelFpr, validationRMSE] = trainRegressionModelFpr(t);
[zfit, zsd, zint] = trainedModelFpr.predictFcn(t);
scatter3(t.threshNull, t.threshAlt, t.fpr, 20, 'MarkerEdgeColor','k',...
        'MarkerFaceColor','r')
hold on
plot3d_errorbars(t.threshNull, t.threshAlt, zfit,...
    zeros(length(t.threshNull)), zeros(length(t.threshAlt)), 2.*zsd, '.r')
[X,Y] = meshgrid(threshNullArray, threshAltArray);
Z = reshape(zfit, 10, 10);
surf(X,Y,Z)
[Cfpr, c3] = contour3(X,Y,Z,'ShowText','off');
c3.Visible = 'off';
CoordFpr = C2xyz(Cfpr);
xlabel('\theta_0'); ylabel('\theta_1'); zlabel('FPR');
title('FPR')

%%
subplot(1,4,4)
axis([0, 1, 0, 1])
hold on
for i = 1:nContourLevels
    x = xAvT{i}; y = yAvT{i};
    fitTab = table(x', y');
    fitTab.Properties.VariableNames = {'threshNull', 'threshAlt'};
    tprFit = trainedModelTpr.predictFcn(fitTab);
    fprFit = trainedModelFpr.predictFcn(fitTab);
    plot(fprFit, tprFit)
end
titleStr = "Iso-nsteps in ROC space";
title(titleStr)
ylabel('True positive rate'); xlabel('False positive rate')
for k = 1:nContourLevels
   legStr{k} = sprintf('T = %.1f', zAvT(k)); 
end
legend(legStr)




