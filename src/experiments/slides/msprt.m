clear
close all

%%
num_channels = 3;

on_mask = zeros(3,1);
on_mask(randi(num_channels,1)) = 1;
on_mask = logical(on_mask);
onChannel = find(on_mask);

discrim = 0.5;
sd = 1;
mu1 = 2;
calcMu2 = discrim * sd + mu1;

Hypotheses(1).mu = mu1;
Hypotheses(1).sd = sd;
Hypotheses(1).name = 'normal';
Hypotheses(2).mu = calcMu2;
Hypotheses(2).sd = sd;
Hypotheses(2).name = 'normal';

priors_arr = (1/num_channels) .* ones(num_channels, 1);
probthresh_arr = 0.95 * ones(num_channels, 1);
Theta_arr = -log(probthresh_arr);

%% DO
simtime = 1000;

% Preallocate storage arrays
s_arr = zeros(num_channels, simtime);
O_arr = zeros(num_channels, simtime);
Y_arr = zeros(num_channels, simtime);
C_arr = zeros(1, simtime);

% Preallocate delta y
x_t = zeros(num_channels, 1);

% Initialise array values at time t = 0
Y_arr(:,1) = log(priors_arr);
C_arr(1) = logsumexp(Y_arr(:,1));
O_arr(:,1) = -Y_arr(:,1) + C_arr(1, 1);

for t = 2:simtime
    
    % Sample from true hypothesis channel distributions
    s_arr(on_mask, t) = icdf(Hypotheses(2).name, rand(1), Hypotheses(2).mu, Hypotheses(2).sd);
    s_arr(~on_mask, t) = icdf(Hypotheses(1).name, rand(num_channels-1, 1), Hypotheses(1).mu, Hypotheses(1).sd);
    
    % Calculate likelihoods over hypotheses
    p0 = pdf(Hypotheses(1).name, s_arr(:, t), Hypotheses(1).mu, Hypotheses(1).sd);
    p1 = pdf(Hypotheses(2).name, s_arr(:, t), Hypotheses(2).mu, Hypotheses(2).sd);
    
    % EVIDENCE at time t per channel
    x_t = log(p1 ./ p0);
    
    % Iterate evidence for each channel
    Y_arr(:,t) = Y_arr(:,t-1) + x_t;
    
    % Calculate competition term
    C_arr(1, t) = logsumexp(Y_arr(:,t));
    
    % Calculate the negative log posterior, O_vec
    O_arr(:,t) = -Y_arr(:,t) + C_arr(1, t);
    
    % Test if threshold is met
    if any(O_arr(:,t) < Theta_arr)
        break
    end
    
end

% Set decision time and delta
dec_time = t-1;
[M, delta] = min(O_arr(:,t));

% Set textbox string
if onChannel == delta && t < simtime
    iscorrect_str = 'Correct channel selection';
elseif ~(onChannel == delta) && t < simtime
    iscorrect_str = 'Incorrect channel selection';
else
    iscorrect_str = 'No decision in simtime';
end

%% PLOT

f1 = figure;
set(f1, 'color','white', 'position', [100 100 1300 450])
C = get(0,'DefaultAxesColorOrder'); % set attractive colour order
for i = 1:num_channels
    legStr{i} = sprintf('Channel %d', i);
end

subplot(1,3,1)
nXvals = 30;
xValArray = linspace(Hypotheses(1).mu - Hypotheses(1).sd*3, Hypotheses(2).mu + Hypotheses(2).sd*3, nXvals);
for i = [1, 2]
    pdfArray(i,:) = pdf(Hypotheses(i).name, xValArray, Hypotheses(i).mu, Hypotheses(i).sd);
end
h1 = plot(xValArray, pdfArray(1,:), 'k:');
hold on
h2 = plot(xValArray, pdfArray(2,:), 'k-');
%title('Channel probability distributions')
ylabel('f(x)'); xlabel('x');
legend([h1 h2], {'f_0 Channel off (noise)', 'f_1 Channel on (signal + noise)'})
probDistYLim = ylim;
hold on

% Plot all observations of s_true as hist up to dec_time
xl = xlim;
binEdges = linspace(xl(1), xl(2), 20);
tmp = s_arr(:,2:t);
for p = 1:num_channels
    hist1(p) = histogram(tmp(p,:),10, 'binEdges', binEdges);
    hist1(p).Normalization = 'probability';
    set(hist1(p), 'FaceColor', C(p,:))
end
set(hist1(~on_mask), 'FaceAlpha', 0.25, 'EdgeAlpha', 0);
set(hist1(on_mask), 'FaceAlpha', 0.9);
legend([h1 h2, hist1(1), hist1(2), hist1(3)], {'f_0 -> Channel off (noise)', 'f_1 -> Channel on (signal + noise)', legStr{:}})
%colorList = get(gca,'ColorOrder');

% subplot(1,2,2)
subplot(1,3,2)
h1 = plot(1:t, O_arr(:,1:t));
set(h1(on_mask), 'linewidth', 1.5, 'linestyle','-');
set(h1(~on_mask), 'linestyle', '--');
xlabel('n'), ylabel('Negative log posterior, O_k');
l = legend(legStr);
l.Location = 'northwest'

tb = annotation('textbox', [0.42 0.70 0.07 0.07]);
tb.String = iscorrect_str;
tb.FitBoxToText = 'on';


subplot(1,3,3)
h2 = plot(1:t, Y_arr(:,1:t));
set(h2(on_mask), 'linewidth', 1.5, 'linestyle','-');
set(h2(~on_mask), 'linestyle', '--');
hold on
xlabel('n'), ylabel('Evidence, Y_k');
h3 = plot(1:t, C_arr(1:t), ':');
l2 = legend(legStr, 'Competition term');
l2.Location = 'northwest';

% subplot(1,2,2)
% h3 = plot(1:t, C_arr(1:t));
% set(h3, 'linewidth', 1.5, 'linestyle','-');
% %set(h3(~on_mask), 'linestyle', '--');
% xlabel('n'), ylabel('Competition (logsumexp(Y_k)');
% %legend(legStr)
