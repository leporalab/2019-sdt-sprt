clear
close all

%% CHOOSE

%Distribution options
discrim = 0.5;
sd = 1;
mu1 = 2;

calcMu2 = discrim * sd + mu1;
Hypotheses(1).mu = mu1;
Hypotheses(1).sd = sd;
Hypotheses(1).name = 'normal';
Hypotheses(2).mu = calcMu2;
Hypotheses(2).sd = sd;
Hypotheses(2).name = 'normal';

%SPRT options
nEpisodes = 500;
T = [1, 2:3:50];
threshAltArray = linspace(0, 5, 10);
threshNullArray = linspace(0, 5, 10);

%% Train models

[roc, models] = trainthresh2models(Hypotheses, threshNullArray, threshAltArray, T, 'nEpisodes', 500, 'FitToAv', 1);

%% Sample cost-ratio pairs and find corresponding threshold pairs
numRatios = 10;
theta_arr = zeros(numRatios, 2);
cr_arr = zeros(numRatios, 2);
%f = waitbar(0,'Minimizing BR', 'Name', 'Progress');

% Sample cost-ratio pair over range W/c > WcRatio
% WcRatio = 100;
% inv_cr = 1/WcRatio .* rand([1,2]);
% cr = 1./inv_cr;

% cr_arr = {[0.1 0.1], [0.5 0.5], [1 1], [2 2], [5 5], [10 10];...
%      [50 50], [45 55], [40 60], [30 50], [20 80], [10 90]};

cr_arr = {[5 5], [100 100], [30, 70], [10 90]};
%[50 50], [45 55], [40 60], [30 50], [20 80], [10 90]};

for k = 1
    
    cr = cr_arr(k,:);
    f1 = figure;
    set(f1, 'color','white')
    C = get(gca,'colororder'); % set attractive colour order
    
    for plt = 1:length(cr)
        % Draw bayes risk over threshold ranges
        BR = zeros(length(threshNullArray), length(threshAltArray));
        for j = 1:length(threshNullArray)
            for i = 1:length(threshAltArray)
                BR(i,j) = bayesrisk([threshNullArray(j) threshAltArray(i)], cr{plt}, models);
            end
        end
        
        caxis manual
        caxis([0 80])
        subplot(1, 1, plt)
        h1(plt) = surf(threshNullArray, threshAltArray, BR)
        az = 70;
        el = 30;
        %view(az, el);
        view(2)
        hold on
        xlabel('\theta_0'); ylabel('\theta_1'); zlabel('Bayes risk');
        xlim([0 5]), ylim([0, 5]); zlim([0 80])
        pbaspect([1 1 1])
        
        
        
        % Minimize bayes risk
        theta_ini = [1, 1];
        A = []; b = []; Aeq = []; beq = []; lb = [0, 0]; ub = [];
        [x, fval] = fmincon(@(theta) bayesrisk(theta, cr{plt}, models), theta_ini, A,b,Aeq,beq,lb,ub);
        
        % theta_arr(i,:) = x;
        % cr_arr(i,:) = cr{plt};
        
        scatter3(x(1), x(2), fval, 20, 'MarkerEdgeColor','k',...
            'MarkerFaceColor','r')
        title(sprintf('Cost ratios: (%d, %d)', cr{plt}(1), cr{plt}(2)));
        
        
    end
end



%% Format figure
f1.Units               = 'centimeters';
f1.Position(3)         = 18.5;
f1.Position(4)         = 5;
% set text properties
set(f1.Children, ...
    'FontName',     'Arial', ...
    'FontSize',     8);

l1.FontSize = 7;
l2.FontSize = 7;

set(gcf, 'Renderer', 'painters')

% s3Pos = get(s3, 'Position');
% oldPos = get(s1, 'Position');
% s1.Position = [oldPos(1) 0.173 oldPos(3) 0.6];
% s2.Position = [oldPos(1) 0.81 oldPos(3) 0.1];

%waitbar(i/numRatios, f, 'Minimizing BR');
%close(f)
% crTable = table(theta_arr(:,1), theta_arr(:,2), cr_arr(:,1), cr_arr(:,2));
% crTable.Properties.VariableNames = {'thetaNull', 'thetaAlt', 'W0_c', 'W1_c'};
% head(crTable)
pos = get(f1,'Position');
set(f1,'PaperPositionMode','Auto','PaperUnits','Centimeters','PaperSize',[pos(3), pos(4)])

